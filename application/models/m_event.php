<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_event extends CI_Model {
	
	function simpan() 
	{	$path = "./assets/event/";//.date("Y")."/";  
		 if ($this->input->post("mode") == "edit") { 
				$id_event = $this->input->post("id_event");
				$data = array('nama_event' =>  $this->input->post("nama_event", true), 
						  'tanggal_awal' => $this->input->post("tanggal_awal", true), 
						  'tanggal_akhir' => $this->input->post("tanggal_akhir", true), 
						  'tempat' => $this->input->post("tempat", true),  
						  'status' => $this->input->post("status", true), 
						  'create_by' => $this->session->userdata('username')
						  );
				$this->db->where('id_event', $id_event);  
				$this->db->update("event", $data ); 
				
			} else {
				$data = array('nama_event' =>  $this->input->post("nama_event", true), 
						  'tanggal_awal' => $this->input->post("tanggal_awal", true), 
						  'tanggal_akhir' => $this->input->post("tanggal_akhir", true), 
						  'tempat' => $this->input->post("tempat", true),  
						  'status' => $this->input->post("status", true), 
						  'create_by' => $this->session->userdata('username')
						  );
				$this->db->insert("event", $data );
				$id_event = $this->db->insert_id();  
			} 
			
			
			
			if ($this->input->post("materi") != null) {
					foreach($this->input->post("materi") as $row) {  
						$arr_name = str_replace(" ", "_", $row["arr_name"]);
						$data2 = array('id_event' =>  $id_event , 
							  'nama_materi' => $row["arr_materi"], 
							  'path_file' => $path, 
							  'nama_file' => $arr_name, 
							  'size_file' => $row["arr_size"], 
							  'tipe_file' => $row["arr_type"], 
							  ); 
				 		$dt = $this->db->insert("materi", $data2 );
					}		  
					
			} 
			
			$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;
	}
	
	
	function dele($table, $key, $id, $file=NULL) {  
			$dt = $this->m_global->get_list_by_id('materi', 'id_event', $id); 
			if ($dt !== null) {
				foreach($dt as $row) {
					$path = $row->path_file;
					$nama = $row->nama_file; 
					if(file_exists($path.$nama)) {
					 unlink($path.$nama);  
					}   
				}
				$this->db->delete('materi', array('id_event' => $id));
			}
			 
		 
		$this->db->delete($table, array($key => $id));
		$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;	
	} 
}


/* End of file m_master.php */
/* Location: ./application/model/m_master.php */
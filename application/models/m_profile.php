<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_profile extends CI_Model
{	
	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
   	} 
	
	function simpan() 
	{ 
			$data = array ( 
				'user_name' => $this->input->post('nama', true),
				'email' => $this->input->post('email'),
				'nip' => $this->input->post('nip', true),
				'nik' => $this->input->post('nik', true),
				'no_telp' => $this->input->post('no_telp', true),
				'asal_instansi' => $this->input->post('asal_instansi', true),
				'jabatan' => $this->input->post('jabatan', true),
				'path_file' => $this->input->post('path_file'),
				'nama_file' => $this->input->post('nama_file'),
				'size_file' => $this->input->post('size_file'),
				'tipe_file' => $this->input->post('tipe_file'),
				'update_by' => $this->session->userdata("username"),
				'update_dt' => date("Y-m-d H:i:s")
	          );
			$this->db->where('user_username', $this->session->userdata("username") ); 
			$this->db->update("user",$data); 
			 
			
			$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;
	}
	 
	 
}
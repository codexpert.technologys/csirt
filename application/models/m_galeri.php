<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_galeri extends CI_Model {
	
	function simpan($mode) 
	{
		if ($mode == "edit") { 
				if ($this->input->post("nama_file", true)=== "") {
					$data = array( 'nama' =>  $this->input->post("nama_galeri", true),
						  'status' => $this->input->post("status", true), 
						  'update_by' => $this->session->userdata('username'),
						  'update_dt' => date("Y-m-d H:i:s"),
						  );
				} else {
					$data = array( 'nama' =>  $this->input->post("nama_galeri", true),
						  'nama_file' => $this->input->post("nama_file", true),  
						  'tipe_file' => $this->input->post("tipe_file", true),  
						  'size_file' => $this->input->post("size_file", true),  
						  'path_file' => $this->input->post("path_file", true), 
						  'status' => $this->input->post("status", true), 
						  'update_by' => $this->session->userdata('username'),
						  'update_dt' => date("Y-m-d H:i:s"),
						  );
				}		  
				$this->db->where('id_galeri', $this->input->post("id_galeri"));  
				$this->db->update("galeri", $data );
			} else {
				$data = array( 'nama' =>  $this->input->post("nama_galeri", true),
						  'path_file' => $this->input->post("path_file"), 
						  'nama_file' => $this->input->post("nama_file"), 
						  'size_file' => $this->input->post("size_file"),  
						  'tipe_file' => $this->input->post("tipe_file"),  
						  'status' => $this->input->post("status"), 
						  'create_by' => $this->session->userdata('username') 
						  );
				$this->db->insert("galeri", $data );
			}
		
		$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;	
	}
	 
}

/* End of file m_master.php */
/* Location: ./application/model/m_master.php */
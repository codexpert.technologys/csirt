<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_forum extends CI_Model {
	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
   	}
	
	function simpanf ($mode) {
		if ($mode == "add") {
			$data = array (   
					'nama_forum' => $this->input->post('nama_forum', true) ,
					'deskripsi' => nl2br($this->input->post('deskripsi', true)) ,
					'create_by' => $this->session->userdata('username')  
		          );
			$this->db->insert("forum",$data);
		} else {
			$data = array ( 
		            'nama_forum' => $this->input->post('nama_forum', true) ,
					'deskripsi' => $this->input->post('deskripsi', true) ,
					'create_by' => $this->session->userdata('username')  
		          );
			$this->db->where('id_forum', $this->input->post('id_forum') ); 
			$this->db->update("forum", $data);
		}
		
		$dbRet = array (
			'errMsg' => $this->db->_error_message(),
			'errNum' => $this->db->_error_number()
		); 
		return $dbRet;
	}
	 
	 
	function get_forum () {
		$sql = "select a.id_forum, a.nama_forum, a.deskripsi, a.create_dt, b.user_name,
				(select count(*) from topik c where c.id_forum=a.id_forum) jml_topik, 
				(select count(*) from reply  d where d.id_forum=a.id_forum) jml_posts ,
				COALESCE((select f.user_name from reply e, user f where e.id_forum=a.id_forum and e.create_by=f.user_username order by e.create_dt desc limit 1), '-')  last_post ,
				COALESCE((select g.create_dt from reply g where g.id_forum=a.id_forum order by g.create_dt desc limit 1 ), '') last_date
				from 
				forum a, user b 
				where a.create_by=b.user_username";
		return $this->m_global->do_query($sql);
		
	} 
	
	
	function get_topik () {
		$sql = "select a.id_forum, a.id_topik, a.nama_topik, a.deskripsi, a.create_dt, b.user_name,
				(select count(*) from reply  c where c.id_topik=a.id_topik) jml_reply,  
				COALESCE((select f.user_name from reply e, user f where e.id_topik=a.id_topik and e.create_by=f.user_username order by e.create_dt desc limit 1), '-')  last_post ,
				COALESCE((select g.create_dt from reply g where g.id_topik=a.id_topik order by g.create_dt desc limit 1 ), '') last_date
				from 
				topik a, user b 
				where a.create_by=b.user_username and a.id_forum=".$this->input->post("id_topik");
		return $this->m_global->do_query($sql);
	}	
	
	function simpant ($mode) {
		if ($mode == "add") {
			$data = array (   
					'id_forum' => $this->input->post('id_forumt', true) ,
					'nama_topik' => $this->input->post('nama_topik', true) ,
					'deskripsi' => $this->input->post('deskripsi_topik', true) ,
					'create_by' => $this->session->userdata('username')  
		          );
			$this->db->insert("topik",$data);
		} else {
			$data = array ( 
		            'nama_topik' => $this->input->post('nama_topik', true) ,
					'deskripsi' => $this->input->post('deskripsi', true) ,
					'create_by' => $this->session->userdata('username')  
		          );
			$this->db->where('id_topik', $this->input->post('id_topik') ); 
			$this->db->update("topik", $data);
		}
		
		$dbRet = array (
			'errMsg' => $this->db->_error_message(),
			'errNum' => $this->db->_error_number()
		); 
		return $dbRet;
	}
	
	function simpanr ($mode) { 
			$data = array (   
					'id_forum' => $this->input->post('id_forum_r', true) ,
					'id_topik' => $this->input->post('id_topik_r', true) , 
					'deskripsi' => nl2br($this->input->post('deskripsi_reply', true)) ,
					'create_by' => $this->session->userdata('username')  
		          );
			$this->db->insert("reply",$data);
		 
		$dbRet = array (
			'errMsg' => $this->db->_error_message(),
			'errNum' => $this->db->_error_number()
		); 
		return $dbRet;
	}
	
	/*function get_reply() {
		$sql = "select * from reply where id_topik = " . $this->input->post("id") . " order by create_dt";
		//$this->data["dt"] = $this->m_global->do_query($sql);
		return $this->m_global->do_query($sql);
	}*/
	
	public function get_current_page($limit, $start) {
        $this->db->where('id_topik', $this->uri->segment(3)); 
		$this->db->limit($limit, $start);
        $query = $this->db->get('reply');
        $rows = $query->result();
 
        if ($query->num_rows() > 0) {
            foreach ($rows as $row) {
                $data[] = $row;
            }
             
            return $data;
        }
 
        return false;
    }
     
    public function get_total() {
		$this->db->where('id_topik', $this->uri->segment(3)); 
        return $this->db->count_all('reply');
    }
	
	function get_topik_by_id($id) {
		$sql = "select a.id_forum, a.id_topik, a.nama_topik, a.deskripsi, a.create_dt, b.user_name from topik a, user b  
				where a.create_by = b.user_username and a.id_topik=".$id;
		return $this->db->query($sql)->row(); 
	}
	
} 
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_email extends CI_Model {
	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
   	}
	
	function simpan ($mode) {
		if ($mode == "add") {
			$dt = $this->m_global->get_data("setting_email");
			if ($dt == null) { 
				$data = array (   
						'from_email' => $this->input->post('from_email', true) ,
						'from_name' => $this->input->post('from_name', true) ,
						'to_email' => $this->input->post('to_email', true)  
			          );
				$this->db->insert("setting_email",$data);
			}
		} else {
			$data = array (  
					'from_email' => $this->input->post('from_email', true) ,
					'from_name' => $this->input->post('from_name', true) ,
					'to_email' => $this->input->post('to_email', true)  
		          );
			$this->db->where('id', $this->input->post('id') ); 
			$this->db->update("setting_email", $data);
		}
		
		$dbRet = array (
			'errMsg' => $this->db->_error_message(),
			'errNum' => $this->db->_error_number()
		); 
		return $dbRet;
	}
	 
} 
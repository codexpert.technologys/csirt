<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_kategori extends CI_Model {
	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
   	}
	
	function insert_kategori() 
	{	$data = array (   
					'kategori' => $this->input->post('kategori', true), 
					'is_aktif' => "1"  
		          );
		$this->db->insert("kategori",$data);
		$dbRet = array (
			'errMsg' => $this->db->_error_message(),
			'errNum' => $this->db->_error_number()
		); 
		return $dbRet;
	} 
	 
	function update_kategori() 
	{	$data = array ( 
		            'kategori' => $this->input->post('kategori', true) 
		          );
		$this->db->where('kategori_id', $this->input->post('kategori_id') ); 
		$this->db->update("kategori", $data);
		$dbRet = array (
			'errMsg' => $this->db->_error_message(),
			'errNum' => $this->db->_error_number()
		); 
		return $dbRet;
	}
	
	function dele_kategori() 
	{	$data = array ( 
		            'is_aktif' => "0"
		          );
		$this->db->where('kategori_id', $this->input->post('id') ); 
		$this->db->update("kategori", $data);
		$dbRet = array (
			'errMsg' => $this->db->_error_message(),
			'errNum' => $this->db->_error_number()
		); 
		return $dbRet;
	}
	
	
} 
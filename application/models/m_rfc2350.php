<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_rfc2350 extends CI_Model {
	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
   	}
	
	function simpan() 
	{
		if ($this->input->post('mode') == "edit") {  
				$data = array (  
		            'long_desc' => $this->input->post('editordata', true), 
					'update_by' => $this->session->userdata("username"),
					'update_dt' => date("Y-m-d H:i:s")
		          );
				$this->db->where('portal_id', 'rfc2350'); 
				$this->db->update('portal',$data);
			} else {
				$data = array (  
				    'portal_id' => 'rfc2350', 
		            'long_desc' => $this->input->post('editordata', true), 
					'create_by' => $this->session->userdata("username")
		          );
					$this->db->insert('portal',$data);
					$profil_id = $this->db->insert_id();  
			} 
			 
		$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;	
	}
	  
	function simpan_file() 
	{ 	for ($i=1; $i <=2; $i++) {
			$data = array (   
					'nama_file' => $this->input->post("nama_file_" . $i, true),  
					'tipe_file' => $this->input->post("tipe_file_" . $i, true),  
					'size_file' => $this->input->post("size_file_" . $i, true),   
					'path_file' => $this->input->post("path_file_" . $i, true),   
		          );
				$this->db->where('id_rfc', $i); 
				$this->db->update('rfc_file',$data);
			
		}	 
		$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;	
	}
}


/* End of file m_master.php */
/* Location: ./application/model/m_master.php */
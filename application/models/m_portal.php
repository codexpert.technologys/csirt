<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_portal extends CI_Model {
	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
   	}
	
	function get_banner()  {
		 return $this->m_global->get_record_by_id("banner", "status", "1"); 
	}
	
	function get_logo()  {
		 return $this->m_global->get_record_by_id("logo", "status", "1"); 
	}
	 
	
	function list_berita_banner() 
	{	 $sql = "SELECT berita_id, title, nama_file, path_file FROM `berita` WHERE status = '1' 
				and kategori_id = 1 and publish_dt < now() ";
		 return $this->m_global->do_query($sql); 
	}
	
	function list_peringatan_keamanan() 
	{	 $sql = "SELECT berita_id, title, nama_file, path_file FROM `berita` WHERE status = '1' 
				 and kategori_id = 2 and publish_dt < now() order by publish_dt desc limit 5";
		 return $this->m_global->do_query($sql); 
	}
	
	function list_berita_1() 
	{	 $sql = "SELECT berita_id, title, nama_file, path_file,publish_dt FROM `berita` WHERE status = '1' 
				 and publish_dt < now() order by publish_dt desc limit 5";
		 return $this->m_global->do_query($sql); 
	}
	
	function list_berita_5() 
	{	 $sql = "SELECT a.berita_id, a.title, a.short_desc, a.nama_file, a.path_file, b.kategori FROM berita a, kategori b WHERE a.status = '1' 
				 and a.publish_dt <= now() and a.kategori_id=b.kategori_id
				
				order by publish_dt desc limit 4";
		 return $this->m_global->do_query($sql); 
	}
	//and a.berita_id not in (select max(berita_id) from berita WHERE status = '1' and publish_dt <= now())
	
	function list_arsip() 
	{	 $sql = "SELECT distinct year(publish_dt) tahun from berita where status = '1' ";
		 return $this->m_global->do_query($sql); 
	}
	
	function list_event() 
	{	 $sql = "SELECT * from event a, materi b  
				 where a.id_event = b.id_event
					and a.status = '1'
				order by a.create_dt, b.id_materi";
		 return $this->m_global->do_query($sql); 
	}
	
	public function get_current_page_galeri($limit, $start) { 
		$this->db->limit($limit, $start);
		$this->db->where("status", "1");
        $query = $this->db->get('galeri');
        $rows = $query->result();
 
        if ($query->num_rows() > 0) {
            foreach ($rows as $row) {
                $data[] = $row;
            }
             
            return $data;
        }
 
        return false;
    }
	
	public function get_total_galeri() { 
        return $this->db->count_all('galeri');
    }
	
	
	function hit() {
		 
		$this->db->set('hit', 'hit+1', FALSE);
					$this->db->where('id_panduan', $this->input->post("id")); 
					$this->db->update('panduan');
		$this->db->query($sql); 
		$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;	
	}
	
	function hit_stat(){
		$ip      = $_SERVER['REMOTE_ADDR']; 
		$tanggal = date("Y-m-d"); // Mendapatkan tanggal sekarang 
		$page = $this->uri->segment(2);
		$page = ($page == '' ? 'main' : $page) ;
		
		$c = $this->db->get_where("stat_ip", array('tanggal' => $tanggal))->row(); 
		if ($c == null) { 
			$this->db->empty_table('stat_ip');  
		} 
	
		$d = $this->db->get_where("stat_ip", array('ip' => $ip, 'tanggal' => $tanggal, 'page' => $page))->row(); 
		if ($d == null) { 
		
			//$timestamp = $d->getTimestamp(); // Unix timestamp
			$data = array('ip' => $ip, 
						  'tanggal' => $tanggal , 
						  'page' => $page , 
						  );
			$this->db->insert("stat_ip", $data );
			
			$stat = $this->db->get_where("stat_page", array('page' => $page, 'tanggal' => $tanggal))->row(); 
				if ($stat == null) {
					
					$data = array('page' =>  $page, 
							  'tanggal' => $tanggal, 
							  'hits' => 1 
							  );
					$this->db->insert("stat_page", $data ); 
				} else { 
					$this->db->set('hits', 'hits+1', FALSE);
					$this->db->where('page', $page);
					$this->db->where('tanggal', $tanggal); 
					$this->db->update('stat_page');
				}  
		}
		 
	}
	
	
	public function get_total_berita($year) { 
		//$year = "2018";
		return $this->db->where('year(publish_dt)', $year)->from("berita")->count_all_results(); 
    }
	
	public function get_current_page_berita($limit, $start, $year) { 
		$this->db->limit($limit, $start);
		$this->db->where("status", "1");
		$this->db->where("year(publish_dt)", $year);
        $query = $this->db->get("berita");
        $rows = $query->result();
 
        if ($query->num_rows() > 0) {
            foreach ($rows as $row) {
                $data[] = $row;
            }
             
            return $data;
        }
 
        return false;
    }
}


/* End of file m_portal.php */
/* Location: ./application/model/m_portal.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_berita extends CI_Model {
	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
   	}
	
	function simpan($mode, $filename, $url) 
	{
		if ($mode == "edit") { 
				$berita_id = $this->input->post('berita_id');
				$data = array ( 
		            'title' => $this->input->post('title', true),
					'kategori_id' => $this->input->post('kategori_id', true),
					'short_desc' => $this->input->post('short_desc', true),
		            'long_desc' => htmlspecialchars_decode($this->input->post('editordata')),
					'status' => $this->input->post('status', true),
					'publish_dt' => date('Y-m-d', strtotime($this->input->post('publish_dt'))), 
					'update_by' => $this->session->userdata("username"),
					'update_dt' => date("Y-m-d H:i:s")
		          ); 
				$this->db->where('berita_id', $berita_id); 
				$this->db->update('berita',$data);
			} else {
				$data = array ( 
		            'title' => $this->input->post('title', true),
					'kategori_id' => $this->input->post('kategori_id', true),
					'short_desc' => $this->input->post('short_desc', true),
		            'long_desc' => htmlspecialchars_decode($this->input->post('editordata')),
					'status' => $this->input->post('status', true),
					'publish_dt' => date('Y-m-d', strtotime($this->input->post('publish_dt'))), 
					'create_by' => $this->session->userdata("username")
		          );
					$this->db->insert('berita',$data);
					$berita_id = $this->db->insert_id();  
			} 
			
			if ($filename != '') {
				$data = array ( 
					'judul_video' => $this->input->post("judul_video", true),
		            'nama_file' => str_replace(" ", "_", $filename),
					'path_file' => $url
					);
				$this->db->where('berita_id', $berita_id); 
				$this->db->update('berita',$data);
			} 
			
			if ($this->input->post("judul_video") == '') {
				if(file_exists($this->input->post("path_file").$filename)) {
					 unlink($this->input->post("path_file").$filename);  
				}
				
				$data = array ( 
					'judul_video' => '',
		            'nama_file' => '',
					'path_file' => ''
					);
				$this->db->where('berita_id', $berita_id); 
				$this->db->update('berita',$data);
			} 
			
		$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;	
	}
	  
	 
}


/* End of file m_master.php */
/* Location: ./application/model/m_master.php */
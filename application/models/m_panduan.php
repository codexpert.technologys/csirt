<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_panduan extends CI_Model {
	
	function simpan($data) 
	{
		if ($data["mode"] == "edit") { 
				if ($data["nama_file"] === "") {
					$data = array( 'nama_panduan' =>  $data["nama_panduan"],
						  'status' => $data["status"],
						  'update_by' => $this->session->userdata('username'),
						  'update_dt' => date("Y-m-d H:i:s")
						  );
				} else {
				$data = array( 'nama_panduan' =>  $data["nama_panduan"],
						  'nama_file' => $data["nama_file"], 
						  'tipe_file' => $data["tipe_file"], 
						  'size_file' => $data["size_file"],
						  'path_file' => $data["path_file"],
						  'status' => $data["status"],
						  'update_by' => $this->session->userdata('username'),
						  'update_dt' => date("Y-m-d H:i:s")
						  );
				}		  
				$this->db->where('id_panduan', $this->input->post("id_panduan"));  
				$this->db->update("panduan", $data );
			} else {
				$data = array('nama_panduan' =>  $data["nama_panduan"],
						  'nama_file' => $data["nama_file"], 
						  'tipe_file' => $data["tipe_file"], 
						  'size_file' => $data["size_file"],
						  'path_file' => $data["path_file"],
						  'status' => $data["status"],
						  'create_by' => $this->session->userdata('username'),
						  'hit' => 0
						  );
				$this->db->insert("panduan", $data );
			}
			
			$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;	
	}
	  
}

/* End of file m_master.php */
/* Location: ./application/model/m_master.php */
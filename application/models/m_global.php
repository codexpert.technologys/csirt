<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_global extends CI_Model {
	
	function get_data($modul) {
		$this->db->from($modul);
		$query = $this->db->get();
		return $query->result();
	} 
	
	function get_data_order($modul, $order) {
		$this->db->from($modul);
		$this->db->order_by($order, "desc");
		$query = $this->db->get();
		return $query->result();
	} 
	
	function get_list_by_id($modul, $key, $id) {
		$this->db->from($modul);
		$this->db->where($key, $id);
		$query = $this->db->get();
		return $query->result();
	} 
	
	function get_list_not_by_id($modul, $key, $id) {
		$this->db->from($modul);
		$this->db->where_not_in($key, $id);
		$query = $this->db->get();
		return $query->result();
	} 
	
	function get_record_join($table1, $table2, $joinkey, $key, $id) {
		$this->db->select('*');
		$this->db->from($table1);
		$this->db->join($table2, $table1.'.'.$joinkey .'='. $table2.'.'.$joinkey); 
		$this->db->where($key, $id); 
		$query = $this->db->get();
		return $query->row(); //only one row
	} 
	
	function do_query($sql) {
		$hasil = [];
		$ambil = $this->db->query($sql); 
		if ($ambil->num_rows() > 0) {
			foreach ($ambil->result() as $data) 
			{ $hasil[] = $data; }
		}
		return $hasil;
	}
	
	function get_record_by_id($table, $key, $id) {
		$d = $this->db->get_where($table, array($key => $id))->row(); 
		return $d;
	} 
	
	function get_record_by_2id($table, $key, $id, $key2, $id2) {
		$d = $this->db->get_where($table, array($key => $id, $key2 => $id2))->row(); 
		return $d;
	} 
	
	function get_column_by_id($table, $column, $key, $id) {
		$this->db->select($column);
		$d = $this->db->get_where($table, array($key => $id))->row(); 
		return $d;
	} 
	
	function get_column_by_2id($table, $column, $key1, $id1, $key2, $id2) {
		$this->db->select($column);
		$d = $this->db->get_where($table, array($key1 => $id1, $key2 => $id2))->row(); 
		return $d;
	} 
	
	function dele($table, $key, $id, $file=NULL) { 
		if ($file != NULL) {   
			$dt = $this->m_global->get_record_by_id($table, $key, $id);
			if (isset($dt->nama_file)	!= '') { 
				$path = $dt->path_file;
				$nama = $dt->nama_file; 
				
				$arr = explode(".", $nama);
				if(file_exists($path.$nama)) {
					 unlink($path.$nama);  
				} 
			} else {
				if ($table = "materi") {
					$path = "./assets/event/".date("Y-m-d")."/";
					$nama = $this->input->post("nama_file"); 
					if(file_exists($path.$nama)) {
					 unlink($path.$nama);  
					}
				}
			}
		}
		$this->db->delete($table, array($key => $id));
		$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;	
	}
	
	function get_logo()  {
		 return $this->get_record_by_id("logo", "status", "1"); 
	}
}


/* End of file m_global.php */
/* Location: ./application/model/m_global.php */
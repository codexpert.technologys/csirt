<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_hubungi extends CI_Model {
	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
   	}
	
	function insert_hubungi() 
	{	$data = array (   
					"nama_lokasi" => $this->input->post("nama_lokasi", true) ,
					"alamat_lokasi" => $this->input->post("alamat_lokasi", true) ,
					"telepon" => $this->input->post("telepon", true) ,
					"email" => $this->input->post("email", true) ,
					"url_map" => $this->input->post("url_map", true) ,
					"keterangan" => $this->input->post("keterangan", true)   
		          );
		$this->db->insert("hubungi",$data);
		$dbRet = array (
			'errMsg' => $this->db->_error_message(),
			'errNum' => $this->db->_error_number()
		); 
		return $dbRet;
	} 
	 
	function update_hubungi() 
	{	$data = array ( 
		            "nama_lokasi" => $this->input->post("nama_lokasi", true) ,
					"alamat_lokasi" => $this->input->post("alamat_lokasi", true) ,
					"telepon" => $this->input->post("telepon", true) ,
					"email" => $this->input->post("email", true) ,
					"url_map" => $this->input->post("url_map", true) ,
					"keterangan" => $this->input->post("keterangan", true)   
		          );
		$this->db->where('id_hubungi', $this->input->post('id_hubungi') ); 
		$this->db->update("hubungi", $data);
		$dbRet = array (
			'errMsg' => $this->db->_error_message(),
			'errNum' => $this->db->_error_number()
		); 
		return $dbRet;
	}
	
	
} 
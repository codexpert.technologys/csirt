<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class m_insiden extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model('m_global');
    }

    function simpan($mode) {
        $path = "./assets/insiden/" . date("Y-m-d") . "/";
		
		$tipe_ins = ($this->input->post('tipe_insiden', true) != "" ? implode(',', $this->input->post('tipe_insiden', true)) : "") ;
		$tipe_ser = ($this->input->post('tipe_serangan', true) != "" ? implode(',', $this->input->post('tipe_serangan', true)) : "") ; 
        if ($mode == "add") {
            $data = array(
                'nama_lengkap' => $this->input->post('nama_lengkap', true),
                'telp_organisasi' => $this->input->post('telp_organisasi', true),
                'organisasi' => $this->input->post('organisasi', true),
                'email' => $this->input->post('email', true),
                'handphone' => $this->input->post('handphone', true),
                'tipe_laporan' => $this->input->post('tipe_laporan', true),
                'tgl_insiden' => substr($this->input->post('tgl_insiden'), 6, 4) . '/' . substr($this->input->post('tgl_insiden'), 3, 2) . '/' . substr($this->input->post('tgl_insiden'), 0, 2),
                'jam_insiden' => $this->input->post('jam_insiden', true),
                'tipe_insiden' => $tipe_ins,
                'deskripsi_insiden' => $this->input->post('deskripsi_insiden', true),
                'path_file' => $path,
                'nama_file' => $this->input->post('file_name', true),
                'dampak_insiden' => $this->input->post('dampak_insiden', true),
                'respon_pdk' => $this->input->post('respon_pdk', true),
                'respon_pjg' => $this->input->post('respon_pjg', true),
                'respon_bak' => $this->input->post('respon_bak', true),
                'laporan_lain' => $this->input->post('laporan_lain', true),
                'aset_kritis' => $this->input->post('aset_kritis', true),
                'dampak_aset' => $this->input->post('dampak_aset', true),
                'jumlah_dampak' => $this->input->post('jumlah_dampak', true),
                'dampak_ict' => $this->input->post('dampak_ict', true),
                'ip_penyerang' => $this->input->post('ip_penyerang', true),
                'port_diserang' => $this->input->post('port_diserang', true),
                'tipe_serangan' => $tipe_ser,
                'lap_analisis' => $this->input->post('lap_analisis', true),
                'lap_forensik' => $this->input->post('lap_forensik', true),
                'lap_audit' => $this->input->post('lap_audit', true),
                'lap_traffic' => $this->input->post('lap_traffic', true),
                'nama_perangkat' => $this->input->post('nama_perangkat', true),
                'lokasi_perangkat' => $this->input->post('lokasi_perangkat', true),
                'sistem_operasi' => $this->input->post('sistem_operasi', true),
                'last_update_os' => $this->input->post('last_update_os', true),
                'ip_address' => $this->input->post('ip_address', true),
                'mac_address' => $this->input->post('mac_address', true),
                'dns_entry' => $this->input->post('dns_entry', true),
                'domain' => $this->input->post('domain', true),
                'konek_jaringan' => $this->input->post('konek_jaringan', true),
                'konek_modem' => $this->input->post('konek_modem', true),
                'pengamanan_fisik' => $this->input->post('pengamanan_fisik', true),
                'pengamanan_logik' => $this->input->post('pengamanan_logik', true),
                'perangkat_putus' => $this->input->post('perangkat_putus', true),
                'status_insiden' => $this->input->post('status_insiden', true),
                'manajemen_krisis' => $this->input->post('manajemen_krisis', true),
                'status' => 'D',
                'create_by' => $this->session->userdata("username")
            );
            $this->db->insert("insiden", $data);
        } else {
            $data = array(
                'nama_lengkap' => $this->input->post('nama_lengkap', true),
                'telp_organisasi' => $this->input->post('telp_organisasi', true),
                'organisasi' => $this->input->post('organisasi', true),
                'email' => $this->input->post('email', true),
                'handphone' => $this->input->post('handphone', true),
                'tipe_laporan' => $this->input->post('tipe_laporan', true),
                'tgl_insiden' => $this->input->post('tgl_insiden', true), //substr($this->input->post('tgl_insiden'), 6, 4) .'/'.substr($this->input->post('tgl_insiden'), 3, 2).'/'.substr($this->input->post('tgl_insiden'), 0, 2) ,
                'jam_insiden' => $this->input->post('jam_insiden', true),
                'tipe_insiden' => $tipe_ins,
                'deskripsi_insiden' => $this->input->post('deskripsi_insiden', true),
                'path_file' => $path,
                'nama_file' => $this->input->post('file_name', true),
                'dampak_insiden' => $this->input->post('dampak_insiden', true),
                'respon_pdk' => $this->input->post('respon_pdk', true),
                'respon_pjg' => $this->input->post('respon_pjg', true),
                'respon_bak' => $this->input->post('respon_bak', true),
                'laporan_lain' => $this->input->post('laporan_lain', true),
                'aset_kritis' => $this->input->post('aset_kritis', true),
                'dampak_aset' => $this->input->post('dampak_aset', true),
                'jumlah_dampak' => $this->input->post('jumlah_dampak', true),
                'dampak_ict' => $this->input->post('dampak_ict', true),
                'ip_penyerang' => $this->input->post('ip_penyerang', true),
                'port_diserang' => $this->input->post('port_diserang', true),
                'tipe_serangan' => $tipe_ser,
                'lap_analisis' => $this->input->post('lap_analisis', true),
                'lap_forensik' => $this->input->post('lap_forensik', true),
                'lap_audit' => $this->input->post('lap_audit', true),
                'lap_traffic' => $this->input->post('lap_traffic', true),
                'nama_perangkat' => $this->input->post('nama_perangkat', true),
                'lokasi_perangkat' => $this->input->post('lokasi_perangkat', true),
                'sistem_operasi' => $this->input->post('sistem_operasi', true),
                'last_update_os' => $this->input->post('last_update_os', true),
                'ip_address' => $this->input->post('ip_address', true),
                'mac_address' => $this->input->post('mac_address', true),
                'dns_entry' => $this->input->post('dns_entry', true),
                'domain' => $this->input->post('domain', true),
                'konek_jaringan' => $this->input->post('konek_jaringan', true),
                'konek_modem' => $this->input->post('konek_modem', true),
                'pengamanan_fisik' => $this->input->post('pengamanan_fisik', true),
                'pengamanan_logik' => $this->input->post('pengamanan_logik', true),
                'perangkat_putus' => $this->input->post('perangkat_putus', true),
                'status_insiden' => $this->input->post('status_insiden', true),
                'manajemen_krisis' => $this->input->post('manajemen_krisis', true),
                'status' => 'D',
                'update_by' => $this->session->userdata("username"),
                'update_dt' => date("Y-m-d h:i:s")
            );
            $this->db->where('id_insiden', $this->input->post('id_insiden'));
            $this->db->update("insiden", $data);
        }

        $dbRet = array(
            'errMsg' => $this->db->_error_message(),
            'errNum' => $this->db->_error_number()
        );
        return $dbRet;
    }

    function get_tipe_insiden() {
        $sql = "select * from ref_insiden where id in (
                select  
                  substring_index(
                    substring_index(tipe_insiden, ',', id), 
                    ',', 
                    -1
                  ) as id_split
                from insiden a
                join ref_insiden b
                  on char_length(tipe_insiden) 
                    - char_length(replace(tipe_insiden, ',', '')) 
                    >= b.id - 1
                where id_insiden = ".$this->input->post('id').")";
        $dt = $this->m_global->do_query($sql);
        return $dt;
    }
    
    function get_tipe_serangan() {
        $sql = "select * from ref_insiden where id in (
                select  
                  substring_index(
                    substring_index(tipe_serangan, ',', id), 
                    ',', 
                    -1
                  ) as id_split
                from insiden a
                join ref_insiden b
                  on char_length(tipe_serangan) 
                    - char_length(replace(tipe_serangan, ',', '')) 
                    >= b.id - 1
                where id_insiden = ".$this->input->post('id').")";
        $dt = $this->m_global->do_query($sql);
        return $dt;
    }

}

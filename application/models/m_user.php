<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_user extends CI_Model
{	
	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
   	}
	public function load_menu() {
		$this->auth->restrict();
		$role = $this->session->userdata('role'); 
		return $this->get_menu_for_role($role); 
	} 
	
	function get_menu_for_role($user_role)
	{   
		$this->db->from('menu');
		$this->db->like('menu_allowed','+'.$user_role.'+');
		$this->db->order_by('id');
		$query = $this->db->get();
		
		return $query->result();  
	}
	 
	function get_menu_not_role($user_role)
	{
		$this->db->from('menu');
		$this->db->not_like('menu_allowed','+'.$user_role.'+');
		$this->db->order_by('id');
		$query = $this->db->get();
		return $query->result();
	}
	 
	
	function get_array_menu($id)
	{
		$this->db->select('menu_allowed');
		$this->db->from('menu');
		$this->db->where('id',$id);
		$data = $this->db->get();
		if($data->num_rows() > 0)
		{
			$row = $data->row();
			$role = $row->menu_allowed;
			$arr = explode('+',$role);
			return $arr;
		} else
		{
			die();
		}
	}

	function get_array_menu_uri($id)
	{	
		$this->db->select('menu_uri');
		$this->db->from('menu');
		$this->db->where('id',$id);
		$this->db->like('menu_allowed', $this->session->userdata('role'));
		$data = $this->db->get();
		
		if($data->num_rows() > 0)
		{   $row = $data->row();
			$arr = "list_".$row->menu_uri;
		} else
		{
			$arr = "xxxx";  
		}		 
		return $arr;
	}
	
	public function get_menu_for_level($user_level)
	{
		$this->db->from('menu');
		$this->db->like('menu_allowed','+'.$user_level.'+');
		$this->db->order_by('id');
		$result = $this->db->get();
		return $result;
	}
	
	function simpan($data) 
	{
		if ($data == "edit") { 
				$data = array ( 
		            'user_name' => $this->input->post('nama', true),
		            'user_role' => $this->input->post('role'),
					'email' => $this->input->post('email'),
					'nip' => $this->input->post('nip', true),
					'nik' => $this->input->post('nik', true),
					'no_telp' => $this->input->post('no_telp', true),
					'asal_instansi' => $this->input->post('asal_instansi', true),
					'jabatan' => $this->input->post('jabatan', true),
					'aktif' => $this->input->post('status'), 
					'update_by' => $this->session->userdata("username"),
					'update_dt' => date("Y-m-d H:i:s")
		          );
				$this->db->where('user_username', $this->input->post('username') ); 
				$this->db->update("user",$data); 
			} else {
				$data = array ( 
		            'user_username' => $this->input->post('username', true),
					'user_password' => md5($this->input->post('password', true)),
					'user_name' => $this->input->post('nama', true),
		            'user_role' => $this->input->post('role'),
					'email' => $this->input->post('email'),
					'nip' => $this->input->post('nip', true),
					'nik' => $this->input->post('nik', true),
					'no_telp' => $this->input->post('no_telp', true),
					'asal_instansi' => $this->input->post('asal_instansi', true),
					'jabatan' => $this->input->post('jabatan', true),
					'path_file' => $this->input->post('path_file'),
					'nama_file' => $this->input->post('nama_file'),
					'size_file' => $this->input->post('size_file'),
					'tipe_file' => $this->input->post('tipe_file'),
					'aktif' => $this->input->post('status'), 
					'create_by' => $this->session->userdata("username")
		          );
				$this->db->insert("user",$data);
			}
			
			$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;
	}
	 
	
	function save_change() 
	{	$data = array ( 
		            'user_password' => md5($this->input->post("password", true)),
					'is_change' => "1", 
					'update_by' => $this->session->userdata("username"),
					'update_dt' => date("Y-m-d H:i:s")
		          );
		$this->db->where('user_username', $this->session->userdata("username")); 
		return $this->db->update("user",$data); 
	}
	
	function simpan_ubah() 
	{	$data = array ( 
		            'user_password' => md5($this->input->post("password", true)), 
					'update_by' => $this->session->userdata("username"),
					'update_dt' => date("Y-m-d H:i:s")
		          );
		$this->db->where('user_username', $this->session->userdata("username")); 
		$this->db->update("user",$data); 
		$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;
	}
	 
	//-----------------------ROLE-------------------------------------------------------------
	function info_role($a) {
		$d = $this->db->get_where('role', array('id' => $a))->row(); 
		return $d;
	}
	 
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_dashboard extends CI_Model {
	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
   	}
	
	function get_tanggal7() {
		$sql = "SELECT distinct tanggal FROM `stat_page` order by 1 desc limit 7";  
		return $this->m_global->do_query($sql); 
	} 
	
	function get_hit_page() {
		$sql = "SELECT id page FROM `ref_page`";  
		return $this->m_global->do_query($sql); 
	}
	
	
	function get_hit_value() { 				
		$sql = "SELECT id page, ifnull((select hits from stat_page b where a.id=b.page and tanggal = '".date("Y-m-d")."'),0)  hits FROM  ref_page a"		;
		return $this->m_global->do_query($sql); 
	}
	
	
	function get_hit_month() {  
		$sql = "SELECT sum(hits) as value , page as name FROM `stat_page` WHERE month(tanggal) = month(now())
				group by page"; 
		return $this->m_global->do_query($sql);
	}
	
	function get_hit_panduan() {  
		$sql = "SELECT hit as value , nama_panduan as name FROM panduan"; 
		return $this->m_global->do_query($sql);
	}
}


/* End of file m_master.php */
/* Location: ./application/model/m_master.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_kontak extends CI_Model {
	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
   	}
	
	function simpan ($mode) {
		if ($mode == "add") {
			$data = array (   
					'nama_kontak' => $this->input->post('nama_kontak', true) ,
					'nama_instansi' => $this->input->post('nama_instansi', true) ,
					'alamat' => $this->input->post('alamat', true) ,
					'no_telp' => $this->input->post('no_telp', true) ,
					'fax' => $this->input->post('fax', true) ,
					'email' => $this->input->post('email', true)
		          );
		$this->db->insert("kontak",$data);
		} else {
			$data = array ( 
		            'nama_kontak' => $this->input->post('nama_kontak', true) ,
					'nama_instansi' => $this->input->post('nama_instansi', true) ,
					'alamat' => $this->input->post('alamat', true) ,
					'no_telp' => $this->input->post('no_telp', true) ,
					'fax' => $this->input->post('fax', true) ,
					'email' => $this->input->post('email', true)
		          );
			$this->db->where('id_kontak', $this->input->post('id_kontak') ); 
			$this->db->update("kontak", $data);
		}
		
		$dbRet = array (
			'errMsg' => $this->db->_error_message(),
			'errNum' => $this->db->_error_number()
		); 
		return $dbRet;
	}
	
	function loaddata($dataarray) { 
		$count=0;$count_gagal=0;
		$jml_data = count($dataarray) + 2;
        for ($i = 2; $i < $jml_data; $i++) { //mulai baris ke 2, 1=header
			$A  = ($dataarray[$i]["A"] != "" ? $dataarray[$i]["A"] : "-");
			$B  = (isset($dataarray[$i]["B"]) ? $dataarray[$i]["B"] : "-");
			$C  = ($dataarray[$i]["C"] != "" ? $dataarray[$i]["C"] : "-");
			$D  = ($dataarray[$i]["D"] != "" ? $dataarray[$i]["D"] : "-");
			$E  = ($dataarray[$i]["E"] != "" ? $dataarray[$i]["E"] : "-");
			$F  = (isset($dataarray[$i]["F"]) ? $dataarray[$i]["F"] : "-");
            $data = array(
                'nama_kontak' => $A,
                'nama_instansi' => $B,
                'alamat' => $C,
				'no_telp' => $D,
				'fax' => $E,
				'email' => $F,
				'create_by' => $this->session->userdata("username") 
            ); 
			
            if ($A !== "-" && $B !== "-" && $D !== "-" && $F !== "-") { //cek ''
           		$this->db->insert('kontak', $data);
				$count++;
				$jml_upload["good"] = $count; 
            } else {
				$count_gagal++;
				$jml_upload["bad"] = $count_gagal;
			}
        } 
		$dbRet = array (
			'errMsg' => "Berhasil Upload : " . $B . " <br>Gagal Upload : " . $count_gagal,
			'errNum' => 0
		); 
		
		return $dbRet ;
	}	
} 
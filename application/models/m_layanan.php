<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_layanan extends CI_Model {
	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
   	}
	
	function simpan() 
	{
		if ($this->input->post('mode') == "edit") {  
				$data = array (  
		            'long_desc' => $this->input->post('editordata', true), 
					'update_by' => $this->session->userdata("username"),
					'update_dt' => date("Y-m-d H:i:s")
		          );
				$this->db->where('portal_id', 'layanan'); 
				$this->db->update('portal',$data);
			} else {
				$data = array (  
				    'portal_id' => 'layanan', 
		            'long_desc' => $this->input->post('editordata', true), 
					'create_by' => $this->session->userdata("username")
		          );
					$this->db->insert('portal',$data);
					$profil_id = $this->db->insert_id();  
			} 
			 
		$dbRet = array (
				'errMsg' => $this->db->_error_message(),
				'errNum' => $this->db->_error_number()
			); 
			return $dbRet;	
	}
	  
	 
}


/* End of file m_master.php */
/* Location: ./application/model/m_master.php */
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div> 
        <!-- <div class="heading-elements add_heading-elements">
            <div class="heading-btn-group">
                <a href="#" class="btn btn-link btn-float has-text" id="btn_add_user"><i class="icon-googleplus5 text-primary"></i><span>Tambah</span></a>  
            </div>
        </div> -->
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i><?php echo $title; ?></a></li> 
        </ul>

    </div>
</div>
<!-- Content area -->
<div class="content">

    <!-- Framed panel body table -->
    <div class="panel panel-flat">  
        <div class="panel-body form_user"> 
            <div class="table-responsive">
                <div class="col-md-12"><!-- class="form-validate-jquery"   -->
                    <form action="#" id="myForm" enctype="multipart/form-data" >
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <fieldset>
                                            <legend class="text-semibold"><i class="icon-reading position-left"></i> Personal details</legend>
                                            <div class="form-group">
                                                <label>Username :<span class="text-danger">*</span></label>
                                                <input type="text" name="username" class="form-control" id="username" readonly  placeholder="Masukkan username" required="required">
                                            </div>

                                            <div class="form-group">
                                                <label>Nama :<span class="text-danger">*</span></label>
                                                <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan nama" required="required">
                                            </div>
                                            <div class="form-group">
                                                <label>Email :</label>
                                                <input type="email" name="email" id="email" class="form-control" required="required"> 
                                            </div>
                                            <div class="form-group">
                                                <label>Dokumen SK Penetapan :</label><br>
												<input type="file" id="fileToUpload" name="fileToUpload" class="file-styled" >
                                                <a href="" class="sk" target="_blank"><span id="lbl_sk"></span> </a>
												<input name="nama_file" type="hidden" id="nama_file" class="form-control">
		                                        <input name="path_file" type="hidden" id="path_file" class="form-control">
		                                        <input name="size_file" type="hidden" id="size_file" class="form-control"> 
		                                        <input name="tipe_file" type="hidden" id="tipe_file" class="form-control">

                                            </div> 
                                        </fieldset>
                                    </div>

                                    <div class="col-md-6">
                                        <fieldset>
                                            <legend class="text-semibold"> .</legend>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>NIP:</label>
                                                        <input type="text" name="nip" id="nip" placeholder="NIP" class="form-control" required="required">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>NIK:</label>
                                                        <input type="text" name="nik" id="nik" placeholder="NIK" class="form-control" required="required">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>No Telepon:</label>
                                                        <input type="text" name="no_telp" id="no_telp" class="form-control" required="required">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Asal Instansi:</label>
                                                        <input type="text" name="asal_instansi" id="asal_instansi" class="form-control" required="required">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row"> 
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Jabatan:</label>
                                                        <input type="text" name="jabatan" id="jabatan" class="form-control" required="required">
                                                    </div>
                                                </div>  
                                            </div> 
                                            <div class="row"> 
                                                <div class="col-md-6">

                                                </div> 
                                            </div> 
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <input type="hidden" id="mode" name="mode" value="edit"> 
                                    <button type="submit" class="btn btn-primary btn-labeled btn_save_user"><b><i class="icon-floppy-disk"></i></b> Simpan </button>  
                                </div>
                            </div>
                        </div>
                    </form> 
                </div>
            </div>

        </div>
    </div> 
    <!-- /framed panel body table -->
<?php $this->load->view($footer); ?>
</div> 
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/profile.js"></script>    

<?php  

class FPDF_AutoWrapTable extends FPDF {
  	private $data = array();
  	private $options = array(
  		'filename' => '',
  		'destinationfile' => '',
  		'paper_size'=>'A4',
  		'orientation'=>'P'
  	);
  	
  	function __construct($data = array(), $options = array()) {
    	parent::__construct(); 
    	$this->data = $data;
    	$this->options = $options;
		$this->CI =& get_instance();
	}
	
	public function rptDetailData () {
		//
		$border = 0;
		$this->AddPage();
		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;
		
		//header
		/*$this->SetFont("", "B", 15);
		$this->MultiCell(0, 12, 'Laporan ');
		$this->Cell(0, 1, " ", "B");
		$this->Ln(10);
		$this->SetFont("", "B", 12);
		$this->SetX($left); $this->Cell(0, 10, $title, 0, 1,'C');
		$this->Ln(3);
		$this->SetFont("", "B", 8);
		$this->SetX($left); $this->Cell(0, 10, $range, 0, 1,'C');*/
		$this->Ln(40);
		
		$h = 18;
		$left = 40;
		$top = 200;	
		#tableheader
		$this->SetFillColor(240,230,140);	
		//$left = $this->GetX();
		$this->SetX($left);
		$this->Cell(520,20,'A. INFORMASI UMUM',1,0,'L',true);
		$this->Ln(20);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,20,'1. IDENTITAS PELAPOR',1,0,'L',true);
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		
		$this->Ln(20);$this->SetX($left);
		$this->Cell(100,$h,'Nama Lengkap',1,0,'L',true);
		$this->Cell(160,$h,$this->data->nama_lengkap,1,0,'L',true);
		$this->Cell(100,$h,'Email',1,0,'L',true);
		$this->Cell(160,$h,$this->data->email,1,0,'L',true);
			
		$this->Ln(18);$this->SetX($left);
		$this->Cell(100, $h, 'Telp Organisasi' ,1 ,0 ,'L' ,true );
		$this->Cell(160, $h, $this->data->telp_organisasi ,1 ,0 ,'L' ,true );
		$this->Cell(100, $h, 'Handphone', 1, 0, 'L', true);
		$this->Cell(160, $h, $this->data->handphone ,1 ,0 ,'L' ,true );
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(100, $h, 'Organisasi' ,1 ,0 ,'L' ,true );
		$this->Cell(420, $h, $this->data->organisasi ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'2. TIPE LAPORAN ',1,0,'L',true);
		
		if ($this->data->tipe_laporan == "AW") {
			$tl = "Awal";
		} elseif ($this->data->tipe_laporan == "TL") {
			$tl = "Tindak Lanjut";
		} else {
			$tl = "Akhir";
		}
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->Cell(100, $h, 'Tipe' ,1 ,0 ,'L' ,true );
		$this->Cell(420, $h, $tl ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'3. WAKTU TERJADINYA INSIDEN',1,0,'L',true);
		
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->Cell(100, $h, 'Tanggal' ,1 ,0 ,'L' ,true );
		$this->Cell(420, $h, $this->data->tgl_insiden ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(100, $h, 'Jam' ,1 ,0 ,'L' ,true );
		$this->Cell(420, $h, $this->data->jam_insiden ,1 ,0 ,'L' ,true ); 
		
		
		$this->Ln(18);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'4. TIPE INSIDEN',1,0,'L',true);
		
		$this->Ln(16);
		 
		$this->SetFont('Arial','',8);
		$this->SetWidths(array(30,50,50,60,50,90,110,110));
		$this->SetAligns(array('C','L','L','L','L','L','L','L'));
		$no = 1; $this->SetFillColor(255);
		/*if ($this->data <> NULL){
			foreach ($this->data as $baris) {
				$this->Row(
					array($no++, 
					date('d-m-Y', strtotime($baris->tgl_input)), 
					date('d-m-Y', strtotime($baris->tgl_surat)), 
					$this->CI->encrypt->decode($baris->no_surat),
					$baris->no_agenda,
					$this->CI->encrypt->decode($baris->asal_surat),
					$this->CI->encrypt->decode($baris->tujuan_surat),
					$this->CI->encrypt->decode($baris->perihal)
				));
			}
		} else {
			$this->Row(array('-', 'No Data', '','','','','',''));
		}*/

	}

	public function printPDF () {
				
		if ($this->options['paper_size'] == "A4") {
			$a = 8.3 * 72; //1 inch = 72 pt
			$b = 13.0 * 72;
			$this->FPDF($this->options['orientation'], "pt", array($a,$b));
		} else {
			$this->FPDF($this->options['orientation'], "pt", $this->options['paper_size']);
		}
		
	    $this->SetAutoPageBreak(false);
	    $this->AliasNbPages();
	    $this->SetFont("helvetica", "B", 10);
	    //$this->AddPage();
	
	    $this->rptDetailData();
		
		$this->SetY(-70);
	    $this->SetFont('arial','',8);
    	$this->Cell(95, 5, 'Printed on : '.date('d/m/Y H:i').' | by : '.$this->CI->session->userdata('username'),0,'LR','L');

	    $this->Output($this->options['filename'],$this->options['destinationfile']);
  	}
  	
  	
  	
  	private $widths;
	private $aligns;

	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}

	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}

	function Row($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=10*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,10,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}

	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
} //end of class
 
$data  = $hasil;
$title = 'ins';
//pilihan
$options = array(
	'filename' => $title.'_'.date("Ymd-Hs").'.pdf', //nama file penyimpanan, kosongkan jika output ke browser
	'destinationfile' => 'I', //I=inline browser (default), F=local file, D=download
	'paper_size'=>'A4',	//paper size: F4, A3, A4, A5, Letter, Legal
	'orientation'=>'P' //orientation: P=portrait, L=landscape
);

$tabel = new FPDF_AutoWrapTable($data, $options);
//$tabel->printPDF($title, $range);
$tabel->printPDF();
?>

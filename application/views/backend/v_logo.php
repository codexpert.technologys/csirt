<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div> 
        <div class="heading-elements add_heading-elements">
            <div class="heading-btn-group">
                <a href="#" class="btn btn-link btn-float has-text" id="btn_add_logo"><i class="icon-googleplus5 text-primary"></i><span>Tambah</span></a>  
            </div>
        </div> 
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i><?php echo $title; ?></a></li> 
        </ul>

    </div>
</div>
<!-- Content area -->
<div class="content">

    <!-- Framed panel body table -->
    <div class="panel panel-flat"> 
        <div class="panel-body list_logo"> 
            <div class="table-responsive">
                <table id="tbl_logo"  class="table table-xxs datatable-basic table-bordered table-striped table-hover" >
                    <thead>
                        <tr class="bg-primary">  
                            <th width="15%">Title</th> 
                            <th width="20%">Ukuran</th> 
                            <th width="10%">Status</th>   
                            <th width="10%">Preview</th>   
                            <th width="15%">Aksi</th> 
                        </tr>
                    </thead>
                    <tbody>  
                    </tbody>
                </table>    
            </div>
        </div>

        <div class="panel-body form_logo" style="display:none"> 
            <div class="table-responsive">
                <div class="col-md-12">

                    <!-- Basic layout--> 
                    <form action="#" id="myForm" enctype="multipart/form-data"  class="form-horizontal">
                        <div class="panel panel-flat"> 
                            <div class="panel-body">
                                <input type="hidden" class="form-control" name="id_logo" id="id_logo" > 
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Title</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="title" id="title" value="">
                                    </div>
                                </div>  
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Unggah File</label>
                                    <div class="col-lg-10">
                                        <input name="fileToUpload" type="file" id="fileToUpload"  class="file-styled">
                                        <input name="nama_file" type="hidden" id="nama_file" class="form-control">
                                        <input name="path_file" type="hidden" id="path_file" class="form-control">
                                        <input name="size_file" type="hidden" id="size_file" class="form-control"> 
                                        <input name="tipe_file" type="hidden" id="tipe_file" class="form-control">
                                        <a href="" class="uf" target="_blank"><span id="lbl_uf"></span> </a>
                                    </div>
                                </div> 
                                <div class="form-group divMsg">
                                    <label class="col-lg-2 control-label">.</label>
                                    <div class="col-lg-10"> <span id="message">jpg|jpeg|png only - dimensions : 64 x 64 </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Status</label>
                                    <div class="col-lg-10">
                                        <input type="checkbox" name="status" id="status" value="1"> <span id="txtStatus">UnPublish</span>
                                    </div>
                                </div> 
                                <div class="text-right"> 
                                    <input type="hidden" id="mode" name="mode" value="add"> 
                                    <button type="submit" class="btn btn-primary btn-labeled btn_save_logo"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
                                    <button type="button" class="btn btn-danger btn-labeled btn_cancel"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
                                </div>
                            </div> 
                        </div>
                    </form>
                </div>

                <!-- /basic layout -->

            </div>
        </div>
    </div>
</div>
<!-- /framed panel body table -->
<?php $this->load->view($footer); ?>
</div>
<div id="modal_default" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Konfirmasi Hapus</h5>
            </div>
            <div class="modal-body">
                <h6 class="text-semibold"></h6>
                <p id="txtHapus"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-labeled btnHapus"><b><i class="icon-trash-alt"></i></b>Hapus</button> 
                <button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>

            </div>
        </div>
    </div>
</div>
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/logo.js"></script>    

<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div> 
		<div class="heading-elements add_heading-elements">
			<div class="heading-btn-group">
				<a href="<?php echo base_url();?>assets/template.xlsx" class="btn btn-link btn-float has-text" id="btn_template" download><i class="icon-file-download text-primary"></i><span>Download Template</span></a>  
				<a href="#" class="btn btn-link btn-float has-text" id="btn_upload_xls"><i class="icon-file-excel text-primary"></i><span>Upload Excel</span></a>  
				<a href="#" class="btn btn-link btn-float has-text" id="btn_add_kontak"><i class="icon-googleplus5 text-primary"></i><span>Tambah</span></a>  
			</div>
		</div> 
    </div>
	<div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i><?php echo $title; ?></a></li>  
        </ul> 
    </div> 
</div>
<!-- Content area -->
<div class="content">
    
    <!-- Framed panel body table -->
    <div class="panel panel-flat"> 
        <div class="panel-body list_kontak"> 
            <div class="table-responsive">
                <table id="tbl_kontak" class="table table-xxs datatable-basic table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-primary"> 
                            <th width="30%">Nama Instansi</th>
							<th width="20%">Nama Kontak</th>
							<th width="20%">No Kontak</th>
							<th width="20%">Email</th>
							<th width="10%">Aksi</th> 
                        </tr>
                    </thead>
                    <tbody>  
                    </tbody>
                </table>   
            </div> 
        </div>
		<div class="panel-body form_kontak" style="display:none"> 
            <div class="table-responsive">
                 <div class="col-md-12">
				 <form class="form-horizontal" id="myForm" action="#">
						<div class="panel panel-flat">
							 
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6">
										<fieldset>
											<legend class="text-semibold"><i class="icon-reading position-left"></i> Kontak Detil </legend>
											<input type="hidden" class="form-control" name="id_kontak" id="id_kontak" maxlength="3"> 
											<div class="form-group">
												<label class="col-lg-3 control-label">Nama Instansi:<span class="text-danger">*</span></label>
												<div class="col-lg-9">
													<input type="text" name="nama_instansi" id="nama_instansi" class="form-control" required="required" placeholder="Masukkan nama instansi">
												</div>
											</div>

											<div class="form-group">
												<label class="col-lg-3 control-label">Nama Kontak:<span class="text-danger">*</span></label>
												<div class="col-lg-9">
													<input type="text" name="nama_kontak" id="nama_kontak" class="form-control" required="required" placeholder="Masukkan nama kontak">
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">Alamat:</label>
												<div class="col-lg-9">
													<textarea rows="5" cols="5" class="form-control" id="alamat" name="alamat" placeholder="Masukkan alamat"></textarea>
												</div>
											</div>
										</fieldset>
									</div>

									<div class="col-md-6">
										<fieldset>
						                	<legend class="text-semibold">.</legend>
 
											<div class="form-group">
												<label class="col-lg-3 control-label">Email:<span class="text-danger">*</span></label>
												<div class="col-lg-9">
													<input type="email" name="email" id="email" class="form-control" required="required">
												</div>
											</div>

											<div class="form-group">
												<label class="col-lg-3 control-label">Phone #:<span class="text-danger">*</span></label>
												<div class="col-lg-9">
													<input type="text" id="no_telp" name="no_telp" class="form-control" required="required">
												</div>
											</div>
 
											<div class="form-group">
												<label class="col-lg-3 control-label">Fax:</label>
												<div class="col-lg-9">
													<input type="text" id="fax" name="fax" class="form-control">
												</div>
											</div> 
										</fieldset>
									</div>
								</div>

								<div class="text-right">
									<input type="hidden" id="mode" name="mode" value="add"> 
                                    <button type="submit" class="btn btn-primary btn-labeled"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
                                    <button type="button" class="btn btn-danger btn-labeled btn_cancel"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
								</div>
							</div>
						</div>
					</form> 

				 </div>
            </div>
		</div>
		
		<div class="panel-body upload_kontak" style="display:none"> 
            <div class="table-responsive">
                 <div class="col-md-12">
				 <form class="form-horizontal" id="myFormX" action="#">
						<div class="panel panel-flat">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label col-sm-3">File to Upload (.xlsx)</label>
											<div class="col-sm-9"> 
												<input name="fileToUpload" type="file" id="fileToUpload"  class="file-styled">
												<input type="hidden" name="path" id="path">
											</div>
										</div> 
										<div class="form-group divMsg" style="display :''">
		                                    <label class="col-sm-3 control-label">.</label>
		                                    <div class="col-sm-9"> <span id="message"></span>
		                                    </div>
		                                </div> 
								</div>
								<div class="text-right"> 
                                    <button type="submit" class="btn btn-primary btn-labeled"><b><i class="icon-floppy-disk"></i></b> Unggah </button> 
                                    <button type="button" class="btn btn-danger btn-labeled btn_cancel"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
								</div>
							</div>
						</div>
					</form> 

				 </div>
            </div>
		</div>
		
    </div> 
    <!-- /framed panel body table -->
     <?php $this->load->view($footer); ?>
</div>
<div id="modal_default" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Konfirmasi Hapus</h5>
			</div>
			<div class="modal-body">
				<h6 class="text-semibold"></h6>
				<p id="txtHapus"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-labeled btnHapus"><b><i class="icon-trash-alt"></i></b>Hapus</button> 
				<button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>
				
			</div>
		</div>
	</div>
</div>
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/kontak.js"></script>    

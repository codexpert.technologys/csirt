<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ul> 
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <!-- Dashboard content -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title cTanggal">Tanggal : <?php echo date("d-m-Y") ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li> 
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div> 
                <div class="panel-body">
                    <div class="chart-container">
                        <div class="chart has-fixed-height" id="columns_stacked"></div>
                    </div>
                </div>
            </div>
        </div>
	</div>
	<div class="row">			
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo date('M-Y'); ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li> 
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="chart-container has-scroll">
                        <div class="chart has-fixed-height has-minimum-width" id="pie_basic"></div>
                    </div>
                </div>
            </div>
        </div>		
    </div>				
	<div class="row">			
       
		<div class="col-lg-12">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title">Unduhan Panduan</h5>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li> 
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>

				<div class="panel-body">
					<div class="chart-container has-scroll">
						<div class="chart has-fixed-height has-minimum-width" id="pie_rose"></div>
					</div>
				</div>
			</div>
        </div>		
    </div>
    <!-- /dashboard content -->


    <!-- Footer -->
    <div class="footer text-muted">
        &copy; 2019. <?php echo $logo->title; ?>
    </div>
    <!-- /footer -->

</div>
<!-- /content area -->
<script src="<?php echo base_url() ?>global_assets/js/plugins/visualization/echarts/echarts.min.js"></script>
<script>
    $(document).ready(
            function () {
                initStat();
                var page = [];
                var tanggal = [];
                var series = [];
				var lblRose = [];
                $.ajax({
                    url: ci.baseurl + 'dashboard/get_hit_stat',
                    type: "post",
                    dataType: 'json', //data format      
                    success: function (data)          //on recieve of reply
                    {
                        // console.log("data:", data)
                        $.each(data[0], function (index, value) {
                            tanggal.push(value.tanggal);
                        });

                        $.each(data[1], function (index, value) {
                            page.push(value.page);
                        });

                        $.each(data[2], function (index, value) {
                            series.push(value.hits);
                        }); 
						
						$.each(data[4], function (index, value) {
                            lblRose.push(value.name);
                        }); 

                        initStat(tanggal, page, series, data[3], lblRose, data[4]);
                    }
                });
            }
    );

</script>
<script src="<?php echo base_url();?>global_assets/js/backend/dashboard.js"></script>  
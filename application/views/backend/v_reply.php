<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div>  
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo base_url().'forum';?>"><i class="icon-home position-left"></i>Forum</a> / <?php echo $title;?></li> 
        </ul>

    </div>
</div>
<!-- Content area -->
<div class="content">

    <!-- Framed panel body table -->
    <div class="panel panel-flat">  
		 
		
		<div class="panel-body" id="div_reply" > 
			 <div class="panel-heading">
			<button type="button" class="btn btn-xs bg-teal-400 btn-labeled btn_balas"><b><i class="icon-reply"></i></b> Balas</button> 
			
			</div>
			<div class="panel panel-white border-top-info border-bottom-info" id="div_header"> 	
				<div class="panel-heading">
					<span class="badge bg-blue-700 pull-right">#1</span> 
					<span class="text-semibold" id="div_header_title"><?php echo $topik->nama_topik ?></span>
	                <span class="display-block text-primary text-size-mini" id="div_header_by">oleh : <?php echo $topik->user_name .', '.$topik->create_dt ;?> </span> 
				</div>
				
				<div class="panel-body" id="div_header_descr"> 
					<?php echo nl2br($topik->deskripsi) ;?> 
				</div>
			</div>
			<div id="div_detil">
				<?php $no = ($page_ke == 0 ? 2 : ($page_ke + 1) * $limit_page);
				if ($results != null) {  
			      foreach($results as $row){ 
				  	
				  	echo '<div class="panel panel-white"> 
						 	<div class="panel-heading"> 
						 		<span class="badge bg-blue-700 pull-right"> #'.$no.'</span>   
						 		<span class="text-semibold">Re : '.$topik->nama_topik.'</span> 
				                 <span class="display-block text-teal text-size-mini">oleh : Administrator , ' . $row->create_dt . '</span>  
						 	</div>
						 	<div class="panel-body">' 
						 		  . $row->deskripsi .
						 	'</div> 
						 </div>';$no++;
					
				  }
				  }
			    ?>
			</div> 
			<div id="pagination">
				<div class="col-md-12"> 
								<div class="text-right">
									<?php
									    if(isset($links)){
									     echo $links;
									    } 
									    ?>
								</div> 
						</div>
			</div>
		</div>
         
		<div class="panel-body" id="form_reply" style="display:none"> 
            <div class="table-responsive">
                <div class="col-md-12">
					<form name="myReply" id="myReply" class="form-horizontal"> 
                    <div class="panel panel-flat"> 
                        <div class="panel-body">
                             
							<div class="form-group">
                                <label class="col-lg-2 control-label">Deskripsi</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" cols="100" class="form-control" id="deskripsi_reply" name="deskripsi_reply"></textarea>
									
                                </div>
                            </div> 
                            <div class="text-right"> 
                                <input type="hidden" id="mode" name="mode" value="add"> 
								<input type="hidden" class="form-control" name="id_forum_r" id="id_forum_r" value="<?php echo $topik->id_forum;?>">
								<input type="hidden" class="form-control" name="id_topik_r" id="id_topik_r" value="<?php echo $topik->id_topik;?>">
                                <button type="submit" class="btn btn-primary btn-labeled btn_save_reply"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
                                <button type="button" class="btn btn-danger btn-labeled" id="btn_cancel_reply"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
                            </div>
                        </div> 
                    </div>
                    </form>
				
                </div> 
            </div>
        </div> 
    </div>
</div>
<!-- /framed panel body table -->
<?php $this->load->view($footer); ?>
</div>
 
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/forum.js"></script>    

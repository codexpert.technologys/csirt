<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div> 
		<div class="heading-elements add_heading-elements">
			<div class="heading-btn-group">
				<a href="#" class="btn btn-link btn-float has-text" id="btn_add_kategori"><i class="icon-googleplus5 text-primary"></i><span>Tambah</span></a>  
			</div>
		</div> 
    </div>
	<div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i><?php echo $title; ?></a></li> 
        </ul>
 
    </div>
</div>
<!-- Content area -->
<div class="content">
    
    <!-- Framed panel body table -->
    <div class="panel panel-flat"> 
        <div class="panel-body list_kategori"> 
            <div class="table-responsive">
                <table id="tbl_kategori" class="table table-xxs datatable-basic table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-primary"> 
                            <th width="30%">Kategori</th>       
							<th width="10%">Aksi</th> 
                        </tr>
                    </thead>
                    <tbody>  
                    </tbody>
                </table>   
            </div>
        </div>
		<div class="panel-body form_kategori" style="display:none"> 
            <div class="table-responsive">
                 <div class="col-md-12">
					<form class="form-horizontal" id="myForm">  
								<fieldset class="content-group">
									<legend class="text-bold">Form kategori</legend>
									<div class="form-group">
										<label class="control-label col-lg-2">Kategori <span class="text-danger">*</span></label>
										<div class="col-lg-10">
											<input type="text" name="kategori" class="form-control" id="kategori" required="required" placeholder="Masukkan Kategori">
										</div>
									</div>  
									<div class="text-right"> 
											<input type="hidden" id="mode" name="mode" value="add">
							                <input type="hidden" class="form-control" name="kategori_id" id="kategori_id" maxlength="3"> 
											<button type="submit" class="btn btn-primary btn-labeled btn_save_kategori"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
											<button type="button" class="btn btn-danger btn-labeled btn_cancel"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
									</div>
</form>

				 </div>
            </div>
		
         </div>
    </div> 
    <!-- /framed panel body table -->
     <?php $this->load->view($footer); ?>
</div>
<div id="modal_default" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Konfirmasi Hapus</h5>
			</div>
			<div class="modal-body">
				<h6 class="text-semibold"></h6>
				<p id="txtHapus"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-labeled btnHapus"><b><i class="icon-trash-alt"></i></b>Hapus</button> 
				<button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>
				
			</div>
		</div>
	</div>
</div>
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/kategori.js"></script>    

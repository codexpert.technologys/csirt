<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
header('X-Frame-Options: DENY');
?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>:: <?php echo $logo->title; ?> ::</title>

        <!-- Global stylesheets -->
		<link rel="icon" type="image/png" href="<?php echo base_url(); ?><?php echo (isset($logo->nama_file) ? $logo->path_file .$logo->nama_file : ""); ?>"/>
        <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"> -->
		<link href="<?php echo base_url() ?>global_assets/css/font.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/core.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>global_assets/css/validationEngine.jquery.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        <script>var ci = {
                baseurl: "<?php echo base_url(); ?>",
				sr:"<?php echo $this->session->userdata("role");?>"
            }
        </script>
        <!-- Core JS files -->

        <script src="<?php echo base_url() ?>global_assets/js/plugins/loaders/pace.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/core/libraries/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/core/libraries/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/loaders/blockui.min.js"></script>

        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="<?php echo base_url() ?>global_assets/js/plugins/forms/validation/validate.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/forms/inputs/touchspin.min.js"></script> 
        <script src="<?php echo base_url() ?>global_assets/js/plugins/notifications/noty.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <!-- Theme JS files -->
        <script src="<?php echo base_url() ?>global_assets/js/plugins/media/fancybox.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/demo_pages/datatables_basic.js"></script>  
        <script src="<?php echo base_url() ?>global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/pickers/pickadate/picker.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/pickers/pickadate/picker.date.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/demo_pages/picker_date.js"></script> 
        <script src="<?php echo base_url() ?>global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/pickers/pickadate/picker.time.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/editors/summernote/summernote.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/forms/styling/switch.min.js"></script><!-- 1 -->
        <script src="<?php echo base_url() ?>global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script src="<?php echo base_url();?>global_assets/js/app.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/demo_pages/form_validation.js"></script><!-- 1 -->
        <script src="<?php echo base_url() ?>global_assets/js/demo_pages/gallery_library.js"></script>
		<script src="<?php echo base_url() ?>global_assets/js/demo_pages/editor_summernote.js"></script>
    </head>

    <body>

        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"><img width="24" height="24"  src="<?php echo base_url(); ?><?php echo (isset($logo->nama_file) ? $logo->path_file .$logo->nama_file : ""); ?>" alt="LOGO">
				</a><h6><b><?php echo $logo->title  ?></b></h6>

                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">


                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="">
                            <span><?php echo $this->session->userdata('name') ?></span>
                            <i class="caret"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right"> 
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url()."profile" ?>"><i class="icon-accessibility"></i>Profile</a></li>
							<li><a href="<?php echo base_url()."ubah" ?>"><i class="icon-cog5"></i>Ubah Password</a></li>
                            <li><a href="<?php echo site_url('login/logout');?>"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="category-content">
                                <div class="media">
                                    <a href="#" class="media-left"><img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold"><?php echo $this->session->userdata('name') ?></span>

                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list">
                                            <li>
                                                <a target="_blank" href="<?php echo base_url()?>"><i class="icon-home"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->


                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">
                                    <!-- <!-- <li>
                                        <a target="_blank" href=" // echo base_url();"><i class="icon-home" title="Main pages"> </i><span>Beranda Portal</span></a></li> -->
                                    <?php $lastlevel = "";$submenu0="";	
                                    foreach($menu as $row)
                                    { 
                                    if ($row->menu_level=="0") 
                                    {
                                    if ($lastlevel == "0") 
                                    {
                                    echo "   </li>" ;
                                    } elseif  ($lastlevel == "1") 
                                    {
                                    echo "    </ul>
                                    </li>" ;
                                    $submenu0="";	 
                                    } 
                                    echo "<li><a href='".site_url($row->menu_uri)."' title='".$row->menu_nama."'><i class='".$row->menu_class."'></i><span>".$row->menu_nama."</span></a>";
                                    $menu0="1";
                                    }

                                    if ($row->menu_level=="1") 
                                    {
                                    if ($submenu0=="") 
                                    {
                                    $submenu0 = "<ul class=\"sub\">";
                                    echo $submenu0 ;
                                    }
                                    echo "<li><a href='".site_url($row->menu_uri)."' title='".isset($row->menu_nama)."'>".$row->menu_nama." </a></li>" ;

                                    }

                                    $lastlevel = $row->menu_level ;  
                                    }		
                                    if ($lastlevel == "0") {
                                    echo "   </li>" ;
                                    } else {
                                    echo "    </ul>
                                    </li>" ;
                                    }

                                    ?>
                                    <!-- Main -->

                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->

                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <?php $this->load->view($page) ?> 

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
        <div id="modal_change" class="modal fade" data-backdrop="static"  tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                        <h5 class="modal-title">Konfirmasi Ubah Password</h5>
                    </div> 
                    <form name="myFormC" id="myFormC" class="form-horizontal">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Password Baru</label>
                                <div class="col-sm-9">
                                    <input type="password" id="password" name="password" required="required" class="form-control">
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary btn-labeled btnChange"><b><i class="icon-trash-alt"></i></b>Simpan</button> 

                            </div>
                        </div>
                    </form>
                </div>
            </div>
		</div>	
		<script src="<?php echo base_url();?>global_assets/js/backend/upload_file.js"></script>  
            <script>
            document.addEventListener('DOMContentLoaded', function () {
                Noty.overrideDefaults({
                    theme: 'limitless',
                    layout: 'topRight',
                    type: 'alert',
                    timeout: 1000
                });
            });

            function message(num, msg) {
                if (num == 0) {
                    new Noty({
                        layout: 'center',
                        text: (msg != '' ? msg : 'Data berhasil disimpan.'),
                        type: 'success'
                    }).show();
                } else {
                    new Noty({
                        layout: 'center',
                        text: 'Data gagal disimpan. <br> \n' + msg,
                        type: 'error'
                    }).show();
                }
            }

            function message_hapus(num, msg) {
                if (num == 0) {
                    new Noty({
                        layout: 'center',
                        text: 'Data berhasil dihapus.',
                        type: 'success'
                    }).show();
                } else {
					//console.log("-->", num);
                    new Noty({
                        layout: 'center',
                        text: 'Data gagal dihapus.',
                        type: 'error'
                    }).show();
                }
            }

            function msg_error(txt) {
                new Noty({
                    layout: 'center',
                    text: txt,
                    type: 'error',
					timeout: 5000
                }).show();
				control = $("#fileToUpload");
				control.replaceWith( control.val('').clone( true ) );
            }
            </script>
    </body>
</html>

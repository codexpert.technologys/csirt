<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div> 
        <!-- <div class="heading-elements add_heading-elements">
            <div class="heading-btn-group">
                <a href="#" class="btn btn-link btn-float has-text" id="btn_add_user"><i class="icon-googleplus5 text-primary"></i><span>Tambah</span></a>  
            </div>
        </div> -->
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i><?php echo $title; ?></a></li> 
        </ul>

    </div>
</div>
<!-- Content area -->
<div class="content">

    <!-- Framed panel body table -->
    <div class="panel panel-flat"> 
        <div class="panel-body list_user"> 
            <div class="table-responsive">
                <table id="tbl_user" class="table table-xxs datatable-basic table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-primary"> 
                            <th width="20%">Username</th>   
                            <th width="20%">Nama</th>   
                            <th width="20%">Role</th>    
                            <th width="10%">Aktif?</th>   
                            <th width="10%">Aksi</th> 
                        </tr>
                    </thead>
                    <tbody>  
                    </tbody>
                </table>   
            </div>
        </div>
        <div class="panel-body form_user" style="display:none"> 
            <div class="table-responsive">
                <div class="col-md-12"><!-- class="form-validate-jquery"   -->
                    <form action="#" id="myForm" enctype="multipart/form-data" >
                        <div class="panel panel-flat">

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <fieldset>
                                            <legend class="text-semibold"><i class="icon-reading position-left"></i> Personal details</legend>

                                            <div class="form-group">
                                                <label>Username :<span class="text-danger">*</span></label>
                                                <input type="text" name="username" class="form-control" id="username"  placeholder="Masukkan username" required="required" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label>Nama :<span class="text-danger">*</span></label>
                                                <input type="text" name="nama" id="nama" class="form-control" readonly placeholder="Masukkan nama" required="required">
                                            </div>

                                            <div class="form-group">
                                                <label>Email :</label>
                                                <input type="email" name="email" id="email" class="form-control" readonly required="required"> 
                                            </div>
                                            <div class="form-group">
                                                <label>Role :<span class="text-danger">*</span></label>
                                                <select  name="role" id="role" class="form-control" required="required"> 
                                                    <option value="">-- Pilih Role --</option>
                                                    <?php
                                                    foreach ($role as $row) {
                                                        echo '<option value="' . $row->id . '">' . $row->role . '</option>';
                                                    }
                                                    ?> 
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Dokumen SK Penetapan :</label><br>
                                                <a href="" class="sk" target="_blank"><span id="lbl_sk"></span> </a>

                                            </div> 
                                            <div class="form-group">
                                                <label>Status Aktif:</label>
                                                <label class="radio-inline">
                                                    <input type="checkbox" name="status" id="status" value="1"> <span id="txtStatus">Tidak</span>
                                                </label>
                                            </div> 
                                        </fieldset>
                                    </div>

                                    <div class="col-md-6">
                                        <fieldset>
                                            <legend class="text-semibold"> .</legend>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>NIP:</label>
                                                        <input type="text" name="nip" id="nip" placeholder="NIP" class="form-control" readonly required="required">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>NIK:</label>
                                                        <input type="text" name="nik" id="nik" placeholder="NIK" class="form-control" readonly required="required">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>No Telepon:</label>
                                                        <input type="text" name="no_telp" id="no_telp" class="form-control" readonly required="required">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Asal Instansi:</label>
                                                        <input type="text" name="asal_instansi" id="asal_instansi" class="form-control" readonly required="required">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row"> 
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Jabatan:</label>
                                                        <input type="text" name="jabatan" id="jabatan" class="form-control" readonly required="required">
                                                    </div>
                                                </div>  
                                            </div> 
                                            <div class="row"> 
                                                <div class="col-md-6">

                                                </div> 
                                            </div> 
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <input type="hidden" id="mode" name="mode" value="add"> 
                                    <button type="submit" class="btn btn-primary btn-labeled btn_save_user"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
                                    <button type="button" class="btn btn-danger btn-labeled btn_cancel"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
                                </div>
                            </div>
                        </div>
                    </form> 
                </div>
            </div>

        </div>
    </div> 
    <!-- /framed panel body table -->
<?php $this->load->view($footer); ?>
</div>
<div id="modal_default" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Konfirmasi Hapus</h5>
            </div>
            <div class="modal-body">
                <h6 class="text-semibold"></h6>
                <p id="txtHapus"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-labeled btnHapus"><b><i class="icon-trash-alt"></i></b>Hapus</button> 
                <button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>

            </div>
        </div>
    </div>
</div>
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/user.js"></script>    

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>

	<!-- Global stylesheets -->
	<link href="<?php echo base_url() ?>global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/core.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?php echo base_url() ?>global_assets/js/plugins/loaders/pace.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/core/libraries/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/core/libraries/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->


	<!-- Theme JS files -->
	<script src="<?php echo base_url();?>global_assets/js/app.js"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	 
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Error title -->
					<div class="text-center content-group">
						<h1 class="error-title">404</h1>
						<h5>Oops, an error has occurred. Page not found!</h5>
					</div>
					<!-- /error title -->


					<!-- Error content -->
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3"> 
								<div class="row">
									<div class="col-sm-12">
										<a href="<?php echo base_url(); ?>" class="btn btn-primary btn-block content-group"><i class="icon-circle-left2 position-left"></i> Go to home</a>
									</div> 
								</div> 
						</div>
					</div> 
					<div class="footer text-muted text-center">
						 
					</div> 
				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>

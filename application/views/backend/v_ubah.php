<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div> 
        <div class="heading-elements add_heading-elements">
            <div class="heading-btn-group">
                
            </div>
        </div> 
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i><?php echo $title; ?></a></li> 
        </ul>

    </div>
</div>
<!-- Content area -->
<div class="content">

    <!-- Framed panel body table -->
    <div class="panel panel-flat"> 
        <div class="panel-body form_ubah" > 
            <div class="table-responsive">
                <div class="col-md-12">

                    <!-- Basic layout--> 
                    <form action="#" id="myForm"  class="form-horizontal">
                        <div class="panel panel-flat"> 
                            <div class="panel-body">
                                <input type="hidden" class="form-control" name="id_ubah" id="id_ubah" maxlength="3"> 
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Password Baru</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="password" id="password" required="required">
                                    </div>
                                </div>   
                                <div class="text-right">  
                                    <button type="submit" class="btn btn-primary btn-labeled btn_save_ubah"><b><i class="icon-floppy-disk"></i></b> Simpan </button>  
                                </div>
                            </div> 
                        </div>
                    </form>
                </div>

                <!-- /basic layout -->

            </div>
        </div>
    </div>
</div>
<!-- /framed panel body table -->
<?php $this->load->view($footer); ?>
</div> 
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/ubah.js"></script>    

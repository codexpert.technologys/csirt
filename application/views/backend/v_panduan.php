<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div> 
        <div class="heading-elements add_heading-elements">
            <div class="heading-btn-group">
                <a href="#" class="btn btn-link btn-float has-text" id="btn_add_panduan"><i class="icon-googleplus5 text-primary"></i><span>Tambah</span></a>  
            </div>
        </div> 
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i><?php echo $title; ?></a></li> 
        </ul>

    </div>
</div>
<!-- Content area -->
<div class="content">

    <!-- Framed panel body table -->
    <div class="panel panel-flat"> 
        <div class="panel-body list_panduan"> 
            <div class="table-responsive">
                <table id="tbl_panduan" class="table table-xxs datatable-basic table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-primary"> 
                            <th width="30%">Judul</th>   
                            <th width="20%">Tgl Unggah</th> 
                            <th width="20%">Ukuran</th> 
							<th width="10%">Jml Hit</th> 
                            <th width="10%">Status</th>   
                            <th width="10%">Aksi</th> 
                        </tr>
                    </thead>
                    <tbody>  
                    </tbody>
                </table>   
            </div>
        </div>

        <div class="panel-body form_panduan" style="display:none"> 
            <div class="table-responsive">
                <div class="col-md-12">

                    <!-- Basic layout-->
                    <form name="myForm" id="myForm" class="form-horizontal">
                    <?php //echo form_open_multipart('panduan/simpan', 'name="myForm" id="myForm" class="form-horizontal" '); ?>
                    <div class="panel panel-flat"> 
                        <div class="panel-body">
                            <input type="hidden" class="form-control" name="id_panduan" id="id_panduan" maxlength="3"> 
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Nama Panduan </label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="nama_panduan" id="nama_panduan">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Unggah File</label>
                                <div class="col-lg-8">
                                    <input name="fileToUpload" type="file" id="fileToUpload"  class="file-styled">
                                    <input name="nama_file" type="hidden" id="nama_file" class="form-control">
                                    <input name="path_file" type="hidden" id="path_file" class="form-control">
                                    <input name="size_file" type="hidden" id="size_file" class="form-control"> 
                                    <input name="tipe_file" type="hidden" id="tipe_file" class="form-control">
									<a href="" class="uf" target="_blank" download><span id="lbl_uf"></span> </a>
                                </div> 
                            </div> 
							<div class="form-group divMsg" style="display :none">
                                    <label class="col-lg-2 control-label">.</label>
                                    <div class="col-lg-10"> <span id="message">pdf Only</span>
                                    </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Status</label>
                                <div class="col-lg-10">
                                    <input type="checkbox" name="status" id="status" value="1" checked="checked"> <span id="txtStatus">Publish</span>
                                </div>
                            </div> 
                            <div class="text-right"> 
                                <input type="hidden" id="mode" name="mode" value="add">
                                <input type="hidden" name="modul" value="panduan"> 
                                <button type="submit" class="btn btn-primary btn-labeled btn_save_panduan"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
                                <button type="button" class="btn btn-danger btn-labeled btn_cancel"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
                            </div>
                        </div> 
                    </div>
                    </form>
                </div> 
                <!-- /basic layout -->

            </div>
        </div>
    </div>
</div>
<!-- /framed panel body table -->
<?php $this->load->view($footer); ?>
</div>
<div id="modal_default" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Konfirmasi Hapus</h5>
            </div>
            <div class="modal-body">
                <h6 class="text-semibold"></h6>
                <p id="txtHapus"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-labeled btnHapus"><b><i class="icon-trash-alt"></i></b>Hapus</button> 
                <button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>

            </div>
        </div>
    </div>
</div>
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/panduan.js"></script>    

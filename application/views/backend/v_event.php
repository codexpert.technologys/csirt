<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div> 
		<div class="heading-elements add_heading-elements">
			<div class="heading-btn-group">
				<a href="#" class="btn btn-link btn-float has-text" id="btn_add_event"><i class="icon-googleplus5 text-primary"></i><span>Tambah</span></a>  
			</div>
		</div> 
    </div>
	<div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i><?php echo $title; ?></a></li> 
        </ul>
 
    </div>
</div>
<!-- Content area -->
<div class="content">
    
    <!-- Framed panel body table -->
    <div class="panel panel-flat"> 
        <div class="panel-body list_event"> 
            <div class="table-responsive">
                <table id="tbl_event" class="table table-xxs datatable-basic table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-primary"> 
                            <th width="30%">Acara</th>   
							<th width="20%">Tgl Awal</th> 
							<th width="20%">Tgl Akhir</th> 
							<th width="25%">Tempat</th> 
							<th width="15%">Status</th>   
							<th width="10%">Aksi</th> 
                        </tr>
                    </thead>
                    <tbody>  
                    </tbody>
                </table>  
            </div>
        </div>
		
		<div class="panel-body form_event" style="display:none"> 
            <div class="table-responsive">
                 <div class="col-md-12">

							<!-- Basic layout-->
							<form name="myForm" id="myForm" class="form-horizontal"> 
							<?php //echo form_open_multipart('event/simpan', 'name="myForm" id="myForm" class="form-horizontal" '); ?>
								<div class="panel panel-flat"> 
									<div class="panel-body">
										<input type="hidden" class="form-control" name="id_event" id="id_event" maxlength="3"> 
										<div class="form-group">
											<label class="col-lg-2 control-label">Acara </label>
											<div class="col-lg-10">
												<input type="text" class="form-control" name="nama_event" id="nama_event">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Tanggal Awal</label>
											<div class="col-lg-3"> 
												<input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control daterange-single" value="<?php echo date("Y-m-d") ?>">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Tanggal Akhir</label>
											<div class="col-lg-3"> 
												<input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control daterange-single" value="<?php echo date("Y-m-d") ?>">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Tempat </label>
											<div class="col-lg-10">
												<input type="text" class="form-control" name="tempat" id="tempat">
											</div>
										</div> 
										<div class="form-group">
											<label class="col-lg-2 control-label">Status</label>
											<div class="col-lg-10">
												<input type="checkbox" name="status" id="status" value="1" checked="checked"> <span id="txtStatus">Publish</span>
											</div>
										</div> 
										<div class="form-group"> 
											<label class="col-lg-2 control-label"></label>
											<div class="col-lg-12">
												<button type="button" class="btn btn-primary btn-sm btn_add_materi" data-toggle="modal" data-target="#modal_theme_primary">Tambah Materi<i class="icon-file-upload position-right"></i>
											</div>
										</div>
										<div class="form-group"> 
											<div class="col-lg-12"> 
												<table id="tbl_materi" class="table table-xxs table-bordered table-striped table-hover">
													<thead>
														<tr class="bg-primary">
															<th width="50%">Nama Materi</th>
															<th width="25%">Nama File</th>
															<th width="15%">Ukuran File</th>
															<th width="10%">Aksi</tr>
														</tr>	
													</thead>
													<tbody>
													
													</tbody>
												</table>
											</div>
										</div> 
									 </div>
										
 
										<div class="text-right"> 
											<input type="hidden" id="mode" name="mode" value="add">
							                <input type="hidden" name="modul" value="event"> 
											<button type="button" class="btn btn-primary btn-labeled btn_save_event"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
											<button type="button" class="btn btn-danger btn-labeled btn_cancel"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
										</div>
									</div>
								</div>
							</form>
							<!-- /basic layout -->
							 
						</div>
            </div>
        </div>
    </div>
    <!-- /framed panel body table -->
     <?php $this->load->view($footer); ?>
</div>
<div id="modal_default" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Konfirmasi Hapus</h5>
			</div>
			<div class="modal-body">
				<h6 class="text-semibold"></h6>
				<p id="txtHapus"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-labeled btnHapus"><b><i class="icon-trash-alt"></i></b>Hapus</button> 
				<button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>
				
			</div>
		</div>
	</div>
</div>
<!-- /content area --> 
<!-- Primary modal -->
					<div id="modal_theme_primary" class="modal fade" tabindex="-1">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h6 class="modal-title">Unggah File</h6>
								</div>
 
								<form action="#" id="myFormU" enctype="multipart/form-data"  class="form-horizontal">
									<div class="modal-body">
										<div class="form-group">
											<label class="control-label col-sm-3">Nama Materi</label>
											<div class="col-sm-9">
												<input type="text" id="nama_materi" name="nama_materi" class="form-control" required="required">
												<label class="control-label col-sm-9 err_materi" style="display:none; color:red"></label>
											</div>
											
										</div>

										<div class="form-group">
											<label class="control-label col-sm-3">File</label>
											<div class="col-sm-9"> 
												<input name="fileToUpload" type="file" id="fileToUpload"  class="file-styled">
												<input name="nama_file" type="hidden" id="nama_file" class="form-control">
			                                    <input name="path_file" type="hidden" id="path_file" class="form-control">
			                                    <input name="size_file" type="hidden" id="size_file" class="form-control"> 
			                                    <input name="tipe_file" type="hidden" id="tipe_file" class="form-control">
											</div>
										</div> 
										<div class="form-group divMsg" style="display :''">
		                                    <label class="col-sm-3 control-label">.</label>
		                                    <div class="col-sm-9"> <span id="message">pdf Only</span>
		                                    </div>
		                                </div>
										<div class="modal-footer">
											<button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button> 
											<button type="button" class="btn btn-primary" id="btn_save_materi">Unggah</button>
										</div>
									</form>
									 	
							</div>
						</div>
					</div>
					<!-- /primary modal -->

<script src="<?php echo base_url();?>global_assets/js/backend/event.js"></script>    

<?php  
header('X-Frame-Options: DENY');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>:: <?php echo (isset($logo->title) ? $logo->title : ""); ?> ::</title>
	<script>var ci = {
                baseurl: "<?php echo base_url(); ?>"
            }
        </script>
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>global_assets/css/validationEngine.jquery.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?php echo base_url() ?>global_assets/js/plugins/loaders/pace.min.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/core/libraries/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/core/libraries/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?php echo base_url() ?>global_assets/js/plugins/notifications/noty.min.js"></script>
	<!-- <script src="<?php// echo base_url() ?>global_assets/js/plugins/forms/validation/validate.min.js"></script> -->
	<script src="<?php echo base_url() ?>global_assets/js/plugins/forms/inputs/touchspin.min.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/plugins/forms/selects/select2.min.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/plugins/forms/styling/switch.min.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/plugins/forms/styling/uniform.min.js"></script>
	
	<script src="<?php echo base_url();?>global_assets/js/app.js"></script>
	<!-- <script src="<?php //echo base_url() ?>global_assets/js/demo_pages/form_validation.js"></script> -->
	<script src="<?php echo base_url() ?>global_assets/js/plugins/validationengine/languages/jquery.validationEngine-en.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/plugins/validationengine/jquery.validationEngine.js"></script>
	 
	<!-- /theme JS files -->

</head>

<body class="login-container bg-slate-800">

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">
					<!-- Registration form -->
					<form action="#" id="myForm" class="form-horizontal form-validate-jquery" enctype="multipart/form-data" >
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<div class="panel registration-form">
									<div class="panel-body">
										<div class="text-center">
											<div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
											<h5 class="content-group-lg">Create account <small class="display-block">All fields are required</small></h5>
										</div>

										<div class="form-group has-feedback">
											<input type="text" name="username" id="username" class="form-control validate[required, custom[onlyLetterNumber]]" placeholder="Masukkan Username">
											<div class="form-control-feedback">
												<i class="icon-user-plus text-muted"></i>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-12">
												<div class="form-group has-feedback">
													<input type="password" name="password" id="password"  class="form-control validate[required]" placeholder="Buat Password">
													<div class="form-control-feedback">
														<i class="icon-user-lock text-muted"></i>
													</div>
												</div>
											</div> 
										</div>
										 <div class="form-group has-feedback">
											<input type="text" name="nama" id="nama" class="form-control validate[required]" placeholder="Masukkan Nama" >
											<div class="form-control-feedback">
												<i class="icon-user-plus text-muted"></i>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group has-feedback">
													<input type="email" name="email" class="form-control validate[required, custom[email]]" id="email" placeholder="Alamat Email">
													<div class="form-control-feedback">
														<i class="icon-mention text-muted"></i>
													</div>
												</div>
											</div> 
										</div>
										
										
											<div class="form-group">
												<label>Dokumen SK Penetapan :</label>
												<input type="file" id="fileToUpload" name="fileToUpload" class="form-control validate[required]">
												<input name="path_file" type="hidden" id="path_file">
												<input name="nama_file" type="hidden" id="nama_file">
												<input name="size_file" type="hidden" id="size_file">
												<input name="tipe_file" type="hidden" id="tipe_file">
												
											</div> 
										<div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback"> 
													<input type="text" name="nip" id="nip" placeholder="NIP" class="form-control validate[required, custom[onlyNumberSp]]">
													<div class="form-control-feedback">
														<i class="icon-medal2 text-muted"></i>
													</div>
												</div>
											</div> 
											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input type="text" name="nik" id="nik" placeholder="NIK" class="form-control validate[required, custom[onlyNumberSp]]">
													<div class="form-control-feedback">
														<i class="icon-medal2 text-muted"></i>
													</div>
												</div>
											</div> 
										</div> 
										<div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback"> 
													<input type="text" name="no_telp" id="no_telp" placeholder="No Telepon" class="form-control validate[required, custom[onlyNumberSp]]">
													<div class="form-control-feedback">
														<i class="icon-iphone text-muted"></i>
													</div>
												</div>
											</div> 
											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input type="text" name="asal_instansi" id="asal_instansi" placeholder="Asal Instansi" class="form-control validate[required]">
													<div class="form-control-feedback">
														<i class="icon-office text-muted"></i>
													</div>
												</div>
											</div> 
										</div> 
										<div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback"> 
													<input type="text" name="jabatan" id="jabatan" placeholder="Jabatan" class="form-control validate[required]" >
													<div class="form-control-feedback">
														<i class="icon-stars text-muted"></i>
													</div>
												</div>
											</div>  
										</div> 
										<div class="text-right"><input type="hidden" id="mode" name="mode" value="add"> 
										    <input type="hidden" id="role" name="role" value="2"> 
											<button type="button" onclick="javascript:history.back()" class="btn btn-link"><i class="icon-arrow-left13 position-left"></i> Login Form</button>
											<button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b> Buat Akun</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
					<!-- /registration form -->
					

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
			
<script src="<?php echo base_url();?>global_assets/js/backend/register1.js"></script>    
</body>
</html>

<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div>  
    </div>
	<div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i> <?php echo $title; ?></a></li> 
        </ul>
 
    </div>
</div>
<!-- Content area -->
<div class="content">
    
    <!-- Framed panel body table -->
    <div class="panel panel-flat">  
		<div class="panel-body form_layanan"> 
            <div class="table-responsive">
                 <div class="col-md-12">

							<!-- Basic layout-->
							<form name="myForm" id="myForm" class="form-horizontal"> 
								<div class="panel panel-flat"> 
									<div class="panel-body">
										<input type="hidden" class="form-control" name="layanan_id" id="layanan_id" maxlength="3"> 
										<div class="form-group">
											<div class="col-lg-12"> 
												<textarea class="form-control" rows="8" cols="9" id="editordata" name="editordata"><?php echo $long_desc; ?></textarea> 
											</div>
										</div> 
									 </div> 
										<div class="text-right"> 
											<input type="hidden" id="mode" name="mode" value="<?php echo ($portal_id == '' ? "add" : "edit" ) ?>"> 
											<button type="submit" class="btn btn-primary btn-labeled btn_save_layanan"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
											<button type="button" class="btn btn-danger btn-labeled btn_cancel"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
										</div>
									</div>
								</div>
							</form>
						</div>
            </div>
        </div>
    </div>
    <!-- /framed panel body table -->
     <?php $this->load->view($footer); ?>
</div>
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/layanan.js"></script>      
<script src="<?php echo base_url();?>global_assets/js/backend/summernote.js"></script>

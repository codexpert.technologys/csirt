<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div> 
		<div class="heading-elements add_heading-elements" id="rowAdd">
			<div class="heading-btn-group">
				<a href="#" class="btn btn-link btn-float has-text" id="btn_add_email"><i class="icon-googleplus5 text-primary"></i><span>Tambah</span></a>  
			</div>
		</div> 
    </div>
	<div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i><?php echo $title; ?></a></li> 
        </ul>
 
    </div>
</div>
<!-- Content area -->
<div class="content">
    
    <!-- Framed panel body table -->
    <div class="panel panel-flat"> 
        <div class="panel-body list_email"> 
            <div class="table-responsive">
                <table id="tbl_email" class="table table-xxs datatable-basic table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-primary"> 
                            <th width="30%">From Email</th>
							<th width="20%">From Name</th>
							<th width="20%">To Email</th> 
							<th width="10%">Aksi</th> 
                        </tr>
                    </thead>
                    <tbody>  
                    </tbody>
                </table>   
            </div> 
        </div>
		<div class="panel-body form_email" style="display:none"> 
            <div class="table-responsive">
                 <div class="col-md-12">
				 <form class="form-horizontal" id="myForm" action="#">
						<div class="panel panel-flat">
							 
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6">
										<fieldset>
											<legend class="text-semibold"><i class="icon-reading position-left"></i> email Detil </legend>
											<input type="hidden" class="form-control" name="id" id="id" maxlength="3"> 
											<div class="form-group">
												<label class="col-lg-3 control-label">From Email:<span class="text-danger">*</span></label>
												<div class="col-lg-9">
													<input type="text" name="from_email" id="from_email" class="form-control" required="required" placeholder="Masukkan email pengirim">
												</div>
											</div>

											<div class="form-group">
												<label class="col-lg-3 control-label">From Name:<span class="text-danger">*</span></label>
												<div class="col-lg-9">
													<input type="text" name="from_name" id="from_name" class="form-control" required="required" placeholder="Masukkan nama alias pengirim">
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">To Email:</label>
												<div class="col-lg-9">
													<input type="text" name="to_email" id="to_email" class="form-control" required="required" placeholder="Masukkan email tujuan">
												</div>
											</div>
										</fieldset>
									</div>
 
								</div>

								<div class="text-right">
									<input type="hidden" id="mode" name="mode" value="add"> 
                                    <button type="submit" class="btn btn-primary btn-labeled"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
                                    <button type="button" class="btn btn-danger btn-labeled btn_cancel"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
								</div>
							</div>
						</div>
					</form> 

				 </div>
            </div>
		
         </div>
    </div> 
    <!-- /framed panel body table -->
     <?php $this->load->view($footer); ?>
</div>
<div id="modal_default" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Konfirmasi Hapus</h5>
			</div>
			<div class="modal-body">
				<h6 class="text-semibold"></h6>
				<p id="txtHapus"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-labeled btnHapus"><b><i class="icon-trash-alt"></i></b>Hapus</button> 
				<button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>
				
			</div>
		</div>
	</div>
</div>
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/email.js"></script>    

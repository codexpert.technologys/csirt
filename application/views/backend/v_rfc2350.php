<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div>  
        <div class="heading-elements add_heading-elements">
            <div class="heading-btn-group">
                <a href="#" class="btn btn-link btn-float has-text" id="btn_add_rfc"><i class="icon-googleplus5 text-primary"></i><span>Upload File RFC</span></a>  
            </div>
        </div> 
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i> <?php echo $title; ?></a></li> 
        </ul>
    </div>
</div>
<!-- Content area -->
<div class="content">
    <!-- Framed panel body table -->
    <div class="panel panel-flat">  
        <div class="panel-body list_rfc" style="display:none"> 
            <form name="myForm1" id="myForm1" class="form-horizontal">
                <div class="table-responsive">
                    <table id="tbl_berita" class="table table-xxs table-bordered table-striped table-hover">
                        <thead>
                            <tr class="bg-primary"> 
                                <th width="30%">Judul</th>  
                                <th width="30%">Nama File</th> 
                                <th width="40%">Upload</th>    
                            </tr>
                        </thead>
                        <tbody>  
                            <?php 
                            foreach($dt_file as $row) {
                            ?>
                            <tr><td><?php echo $row->nama_rfc; ?></td>
                                <td><a href="<?php echo base_url() . $row->path_file.$row->nama_file; ?>" class="uf_<?php echo $row->id_rfc; ?>" target="_blank" download><span id="lbl_uf_<?php echo $row->id_rfc; ?>"><?php echo $row->nama_file; ?></span> </a></td>
                                <td><input name="fileToUpload<?php echo $row->id_rfc; ?>" type="file" id="fileToUpload<?php echo $row->id_rfc; ?>">
                                    <input name="nama_file_<?php echo $row->id_rfc; ?>" type="hidden" id="nama_file_<?php echo $row->id_rfc; ?>" class="form-control" value="<?php echo $row->nama_file; ?>">
                                    <input name="path_file_<?php echo $row->id_rfc; ?>" type="hidden" id="path_file_<?php echo $row->id_rfc; ?>" class="form-control" value="<?php echo $row->path_file; ?>">
                                    <input name="size_file_<?php echo $row->id_rfc; ?>" type="hidden" id="size_file_<?php echo $row->id_rfc; ?>" class="form-control" value="<?php echo $row->size_file; ?>"> 
                                    <input name="tipe_file_<?php echo $row->id_rfc; ?>" type="hidden" id="tipe_file_<?php echo $row->id_rfc; ?>" class="form-control" value="<?php echo $row->tipe_file; ?>">
                                </td>
                            </tr>
                            <?php } ?> 
                        </tbody>
                    </table>   
                </div><br>

                <div class="text-right"> 
                    <input type="hidden" id="mode" name="mode" value="add">
                    <input type="hidden" name="modul" value="panduan"> 
                    <button type="submit" class="btn btn-primary btn-labeled btn_save_panduan"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
                    <button type="button" class="btn btn-danger btn-labeled btn_cancel_rfc"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
                </div>
            </form>
        </div>
        <div class="panel-body form_rfc"> 
            <div class="table-responsive">
                <div class="col-md-12">
                    <!-- Basic layout-->
                    <form name="myForm" id="myForm" class="form-horizontal"> 
                        <div class="panel panel-flat"> 
                            <div class="panel-body">
                                <input type="hidden" class="form-control" name="profil_id" id="profil_id" maxlength="3"> 
                                <div class="form-group">
                                    <div class="col-lg-12"> 
                                        <textarea class="form-control" rows="8" cols="9" id="editordata" name="editordata"><?php echo $long_desc; ?></textarea> 
                                    </div>
                                </div> 
                            </div> 
                            <div class="text-right"> 
                                <input type="hidden" id="mode" name="mode" value="<?php echo ($portal_id == '' ? "add" : "edit" ) ?>"> 
                                       <button type="submit" class="btn btn-primary btn-labeled btn_save_profil"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
                                <button type="button" class="btn btn-danger btn-labeled btn_cancel"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /framed panel body table -->
<?php $this->load->view($footer); ?>
</div>
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/rfc2350.js"></script>    
<script src="<?php echo base_url();?>global_assets/js/backend/summernote.js"></script>
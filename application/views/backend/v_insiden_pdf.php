<?php  

class FPDF_AutoWrapTable extends FPDF {
  	private $data = array();
	private $data_ins = array();
	private $data_ser = array();
  	private $options = array(
  		'filename' => '',
  		'destinationfile' => '',
  		'paper_size'=>'A4',
  		'orientation'=>'P'
  	);
  	
  	function __construct($data = array(), $data_ins = array(), $data_ser = array(), $options = array()) {
    	parent::__construct(); 
    	$this->data = $data;
		$this->data_ins = $data_ins;
		$this->data_ser = $data_ser;
    	$this->options = $options;
		$this->CI =& get_instance();
	}
	
	public function rptDetailData () {
		//
		$border = 0;
		$this->AddPage();
		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;
		
		//header
		/*$this->SetFont("", "B", 15);
		$this->MultiCell(0, 12, 'Laporan ');
		$this->Cell(0, 1, " ", "B");
		$this->Ln(10);
		$this->SetFont("", "B", 12);
		$this->SetX($left); $this->Cell(0, 10, $title, 0, 1,'C');
		$this->Ln(3);
		$this->SetFont("", "B", 8);
		$this->SetX($left); $this->Cell(0, 10, $range, 0, 1,'C');*/
		$this->Ln(40);
		
		$h = 18;
		$left = 40;
		$top = 200;	
		#tableheader
		$this->SetFillColor(240,230,140);	
		//$left = $this->GetX();
		$this->SetX($left);
		$this->Cell(520,20,'A. INFORMASI UMUM',1,0,'L',true);
		$this->Ln(20);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,20,'1. IDENTITAS PELAPOR',1,0,'L',true);
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		
		$this->Ln(20);$this->SetX($left);
		$this->Cell(70,$h,'Nama Lengkap',1,0,'L',true);
		$this->Cell(190,$h,$this->data->nama_lengkap,1,0,'L',true);
		$this->Cell(60,$h,'Email',1,0,'L',true);
		$this->Cell(200,$h,$this->data->email,1,0,'L',true);
			
		$this->Ln(18);$this->SetX($left);
		$this->Cell(70, $h, 'Telp Organisasi' ,1 ,0 ,'L' ,true );
		$this->Cell(190, $h, $this->data->telp_organisasi ,1 ,0 ,'L' ,true );
		$this->Cell(60, $h, 'Handphone', 1, 0, 'L', true);
		$this->Cell(200, $h, $this->data->handphone ,1 ,0 ,'L' ,true );
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(70, $h, 'Organisasi' ,1 ,0 ,'L' ,true );
		$this->Cell(450, $h, $this->data->organisasi ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'2. TIPE LAPORAN ',1,0,'L',true);
		
		if ($this->data->tipe_laporan == "AW") {
			$tl = "Awal";
		} elseif ($this->data->tipe_laporan == "TL") {
			$tl = "Tindak Lanjut";
		} else {
			$tl = "Akhir";
		}
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->Cell(70, $h, 'Tipe' ,1 ,0 ,'L' ,true );
		$this->Cell(450, $h, $tl ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'3. WAKTU TERJADINYA INSIDEN',1,0,'L',true);
		
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->Cell(70, $h, 'Tanggal' ,1 ,0 ,'L' ,true );
		$this->Cell(450, $h, $this->data->tgl_insiden ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(70, $h, 'Jam' ,1 ,0 ,'L' ,true );
		$this->Cell(450, $h, $this->data->jam_insiden ,1 ,0 ,'L' ,true ); 
		
		
		$this->Ln(18);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'4. TIPE INSIDEN',1,0,'L',true);
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$arr_ti = explode(",", $this->data->tipe_insiden);
		$ni=0;
		foreach ($this->data_ins as $ins) {
			if (strlen(array_search($ins->id, $arr_ti)) >= 1) {
			    $ni ++;
				$this->Ln(18);$this->SetX($left);
				$this->Cell(520, $h, $ins->deskripsi ,1 ,0 ,'L' ,true ); 
			}	
		}
		
		if ($ni==0) {
			$this->Ln(18);$this->SetX($left);
			$this->Cell(520, $h, "-",1 ,0 ,'L' ,true ); 
		}
		
		$this->Ln(18);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'5. DESKRIPSI INSIDEN DISERTASI BUKTI (SCREENSHOT, DOMAIN NAME, URL, EMAIL, DLL)',1,0,'L',true);
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->MultiCell(520, $h, $this->data->deskripsi_insiden ,1 ); 
		$this->SetX($left);
		$this->Cell(70, $h, 'Bukti Insiden' ,1 ,0 ,'L' ,true );
		$this->Cell(450, $h, $this->data->nama_file,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'6. DAMPAK INSIDEN',1,0,'L',true);
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$di = "";
		$this->Cell(70, $h, 'Dampak' ,1 ,0 ,'L' ,true );
		if ($this->data->dampak_insiden == "P") {
			$di = "Jaringan Publik";
		} else if ($this->data->dampak_insiden == "I") {
			$di = "Jaringan Internal";
		} else if ($this->data->dampak_insiden == "L") {
			$di = "Lainnya";
		}
		 
		$this->Cell(450, $h, $di ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'7. TINDAK PENANGGULANGAN INSIDEN',1,0,'L',true);
		
		$this->SetFillColor(240,230,140);	
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->Cell(520,$h,'a. Respon Cepat/Awal(Jangka Pendek):',1,0,'L',true);
		$this->Ln(18);$this->SetX($left);
		$this->MultiCell(520, $h, $this->data->respon_pdk ,1 ); 
		$this->SetX($left);
		$this->Cell(520,$h,'b. Jangka Panjang:',1,0,'L',true);
		$this->Ln(18);$this->SetX($left);
		$this->MultiCell(520, $h, $this->data->respon_pjg ,1 ); 
		$this->SetX($left);
		$this->Cell(520,$h,'c. Apakah perencanaan BackUp System berhasil diimplementasikan? Jika iya, deskripsikan proses tersebut :',1,0,'L',true);
		$this->Ln(18);$this->SetX($left);
		$this->MultiCell(520, $h, $this->data->respon_bak ,1 ); 
		
		$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'8. APAKAH ORGANISASI LAIN DILAPORKAN? JIKA IYA, SEBUTKAN',1,0,'L',true);
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->MultiCell(520, $h, $this->data->laporan_lain ,1 ); 
		
		$this->Ln(18);
		
		$this->SetFillColor(240,230,140);	
		//$left = $this->GetX();
		$this->SetX($left);$this->SetFont("", "B", 9);
		$this->Cell(520,20,'B. INFORMASI KHUSUS',1,0,'L',true);
		$this->Ln(18);$this->SetX($left);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'9. ASET KRITIS YANG TERKENA DAMPAK',1,0,'L',true);
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->MultiCell(520, $h, $this->data->aset_kritis ,1 ); 
		
		$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'10. DAMPAK INSIDEN TERHADAP ASET',1,0,'L',true);
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->MultiCell(520, $h, $this->data->dampak_aset ,1 ); 
		
		$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'11. JUMLAH PENGGUNA YANG TERKENA DAMPAK',1,0,'L',true);
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->MultiCell(520, $h, $this->data->jumlah_dampak,1 ); 
		
		$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'12. DAMPAK TERHADAP ICT (INFORMATION AND COMMUNICATION TECHNOLOGIES)',1,0,'L',true);
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->MultiCell(520, $h, $this->data->dampak_ict ,1 ); 
		
		$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'13. PROFIL PENYERANG',1,0,'L',true);
		
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->Cell(100, $h, 'IP Penyerang' ,1 ,0 ,'L' ,true );
		$this->Cell(420, $h, $this->data->ip_penyerang ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(100, $h, 'Port Diserang' ,1 ,0 ,'L' ,true );
		$this->Cell(420, $h, $this->data->port_diserang,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'14. TIPE SERANGAN',1,0,'L',true);
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$arr_ts = explode(",", $this->data->tipe_serangan);
		$ns=0;
		foreach ($this->data_ser as $ser) {
			if (strlen(array_search($ser->id, $arr_ts)) >= 1) {
			    $ns ++;
				$this->Ln(18);$this->SetX($left);
				$this->Cell(520, $h, $ser->deskripsi ,1 ,0 ,'L' ,true ); 
			}	
		}
		
		if ($ns==0) {
			$this->Ln(18);$this->SetX($left);
			$this->Cell(520, $h, "-",1 ,0 ,'L' ,true ); 
		}
		
		$this->Ln(18);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'15. ANALISIS',1,0,'L',true);
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'a. Laporan Analisis Log ',1 ,0 ,'L' ,true );
		$this->Cell(220, $h, ($this->data->lap_analisis == "A" ? "Ada" : "Tidak Ada") ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'b. Laporan Forensik' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, ($this->data->lap_forensik == "A" ? "Ada" : "Tidak Ada") ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'c. Laporan Audit' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, ($this->data->lap_audit == "A" ? "Ada" : "Tidak Ada") ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'd. Laporan analisis lalu lintas jaringan (Network Traffic)' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, ($this->data->lap_traffic == "A" ? "Ada" : "Tidak Ada") ,1 ,0 ,'L' ,true ); 
		
		
		$this->Ln(18);$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'16. RINCIAN',1,0,'L',true);
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'a. Nama dan Versi Perangkat ',1 ,0 ,'L' ,true );
		$this->Cell(220, $h, $this->data->nama_perangkat ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'b. Lokasi Perangkat' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, $this->data->lokasi_perangkat ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'c. Sistem Operasi' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, $this->data->sistem_operasi ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'd. Terakhir Update Sistem Operasi/Firmware' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, $this->data->last_update_os, 1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'e. IP Address' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, $this->data->ip_address ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'f. MAC Address' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, $this->data->mac_address,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'g. DNS Entry' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, $this->data->dns_entry,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'h. Domain/Workgroup' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, $this->data->domain ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'i. Apakah perangkat yang terkena dampak terhubung ke jaringan?' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, ($this->data->konek_jaringan == "Y" ? "Ya" : "Tidak") ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'j. Apakah perangkat yang terkena dampak terhubung ke modem?' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, ($this->data->konek_modem == "Y" ? "Ya" : "Tidak") ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'k. Adakah pengamanan fisik terhadap perangkat?' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, ($this->data->pengamanan_fisik == "Y" ? "Ya" : "Tidak") ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'l. Adakah pengamanan logik terhadap perangkat?' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, ($this->data->pengamanan_logik == "Y" ? "Ya" : "Tidak") ,1 ,0 ,'L' ,true ); 
		
		$this->Ln(18);$this->SetX($left);
		$this->Cell(300, $h, 'm. Apakah perangkat sudah diputus dari jaringan?' ,1 ,0 ,'L' ,true );
		$this->Cell(220, $h, ($this->data->perangkat_putus == "Y" ? "Ya" : "Tidak") ,1 ,0 ,'L' ,true ); 
		
		$this->SetX($left);$this->SetFont("", "B", 9);
		$this->Ln(18);$this->SetX($left);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'16. STATUS INSIDEN',1,0,'L',true); 
		
		$this->SetFillColor(255,255,255);	
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->MultiCell(520, $h, $this->data->status_insiden,1 ); 
		
		$this->SetX($left);$this->SetFont("", "B", 9);
		$this->SetFillColor(238,232,170);	
		$this->Cell(520,$h,'17. APAKAH SUDAH PERNAH DITAWARKAN SISTEM MANAJEMEN KRISIS?',1,0,'L',true);
		$this->SetFont('Arial','',9);
		$this->Ln(18);$this->SetX($left);
		$this->MultiCell(520, $h, $this->data->manajemen_krisis ,1 ); 
		

		$this->SetFont('Arial','',8);
		$this->SetWidths(array(30,50,50,60,50,90,110,110));
		$this->SetAligns(array('C','L','L','L','L','L','L','L'));
		$no = 1; $this->SetFillColor(255);
		 
	}

	public function printPDF () {
				
		if ($this->options['paper_size'] == "A4") {
			$a = 8.3 * 72; //1 inch = 72 pt
			$b = 13.0 * 72;
			$this->FPDF($this->options['orientation'], "pt", array($a,$b));
		} else {
			$this->FPDF($this->options['orientation'], "pt", $this->options['paper_size']);
		}
		
	    $this->SetAutoPageBreak(false);
	    $this->AliasNbPages();
	    $this->SetFont("helvetica", "B", 10);
	    //$this->AddPage();
	
	    $this->rptDetailData();
		
		$this->SetY(-70);
	    $this->SetFont('arial','',8);
    	$this->Cell(95, 5, 'Printed on : '.date('d/m/Y h:n:s').' | by : '.$this->CI->session->userdata('name'),0,'LR','L');

	    $this->Output($this->options['filename'],$this->options['destinationfile']);
  	}
  	
  	
  	
  	private $widths;
	private $aligns;

	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}

	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}

	function Row($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=10*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,10,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}

	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
} //end of class
 
$data  = $hasil;
$data_ins  = $ref_i;
$data_ser  = $ref_s;
$title = 'ins';
//pilihan
$options = array(
	'filename' => $title.'_'.date("Ymd-Hs").'.pdf', //nama file penyimpanan, kosongkan jika output ke browser
	'destinationfile' => 'D', //I=inline browser (default), F=local file, D=download
	'paper_size'=>'A4',	//paper size: F4, A3, A4, A5, Letter, Legal
	'orientation'=>'P' //orientation: P=portrait, L=landscape
);

$tabel = new FPDF_AutoWrapTable($data, $data_ins, $data_ser, $options);
//$tabel->printPDF($title, $range);
$tabel->printPDF();
?>

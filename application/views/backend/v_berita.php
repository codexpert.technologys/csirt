<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div> 
		<div class="heading-elements add_heading-elements">
			<div class="heading-btn-group">
				<a href="#" class="btn btn-link btn-float has-text" id="btn_add_berita"><i class="icon-googleplus5 text-primary"></i><span>Tambah</span></a>  
			</div>
		</div> 
    </div>
	<div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i> <?php echo $title; ?></a></li> 
        </ul>
 
    </div>
</div>
<!-- Content area -->
<div class="content">
    
    <!-- Framed panel body table -->
    <div class="panel panel-flat"> 
        <div class="panel-body list_berita"> 
            <div class="table-responsive">
                <table id="tbl_berita" class="table table-xxs datatable-basic table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-primary">
                            <th >Kode</th> 
                            <th width="30%">Judul</th>  
							<th width="25%">Kategori</th>  
							<th width="20%">Tgl Publish</th>  
							<th width="15%">Status</th>   
							<th width="10%">Aksi</th> 
                        </tr>
                    </thead>
                    <tbody>  
                    </tbody>
                </table>   
            </div>
        </div>
		
		<div class="panel-body form_berita" style="display:none"> 
            <div class="table-responsive">
                 <div class="col-md-12">

							<!-- Basic layout-->
							<form name="myForm" id="myForm" class="form-horizontal"> 
							<!-- <?php //echo form_open_multipart('', 'name="myForm" id="myForm" class="form-horizontal" '); ?> -->
								<div class="panel panel-flat"> 
									<div class="panel-body">
										<input type="hidden" class="form-control" name="berita_id" id="berita_id" maxlength="3"> 
										<div class="form-group">
											<label class="col-lg-2 control-label">Kategori</label>
											<div class="col-lg-4">
												<select name="kategori_id" id="kategori_id" class="form-control" required="required">
													<option value="">-- Pilih Kategori --
													<?php foreach ($kategori as $row) {
														echo '<option value="'.$row->kategori_id.'">'.$row->kategori;
													}
													?>
													<!-- <option value="1">Berita
													<option value="2">Info Penting
													<option value="3">Peringatan Keamanan -->
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Judul </label>
											<div class="col-lg-10">
												<input type="text" class="form-control" name="title" id="title" required="required">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Short Desc</label>
											<div class="col-lg-10">
												<textarea rows="5"  class="form-control" name="short_desc" id="short_desc" required="required"></textarea>
 
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Upload Video</label>
											<div class="col-lg-10">
												<input name="fileToUpload" type="file" id="fileToUpload" class="form-control">
												<input name="nama_file" type="hidden" id="nama_file" class="form-control">
												<input name="path_file" type="hidden" id="path_file" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Judul Video</label>
											<div class="col-lg-10"> 
												<input name="judul_video" type="text" id="judul_video" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<!-- <div class="panel-body">
														<div class="summernote">  
														</div>
												</div> -->
												<textarea class="form-control" rows="8" cols="9" id="editordata" name="editordata"></textarea>  
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Tanggal Publish</label>
											<div class="col-lg-3"> 
												<input type="text" name="publish_dt" id="publish_dt" class="form-control daterange-single" value="<?php echo date("Y-m-d") ?>">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Status</label>
											<div class="col-lg-10">
												<input type="checkbox" name="status" id="status" value="1" checked="checked"> <span id="txtStatus">Publish</span>
											</div>
										</div> 
									 </div>
										
 
										<div class="text-right"> 
											<input type="hidden" id="mode" name="mode" value="add">
							                <input type="hidden" name="modul" value="berita"> 
											<button type="submit" class="btn btn-primary btn-labeled btn_save_berita"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
											<button type="button" class="btn btn-danger btn-labeled btn_cancel"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
										</div>
									</div>
								</div>
							</form>
							<!-- /basic layout -->
							 
						</div>
            </div>
        </div>
    </div>
    <!-- /framed panel body table -->
     <?php $this->load->view($footer); ?>
</div>
<div id="modal_default" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Konfirmasi Hapus</h5>
			</div>
			<div class="modal-body">
				<h6 class="text-semibold"></h6>
				<p id="txtHapus"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-labeled btnHapus"><b><i class="icon-trash-alt"></i></b>Hapus</button> 
				<button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>
				
			</div>
		</div>
	</div>
</div>
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/berita.js"></script>    

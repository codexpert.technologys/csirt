<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div>  
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i><?php echo $title; ?></a></li> 
        </ul>

    </div>
</div>
<!-- Content area -->
<div class="content">

    <!-- Framed panel body table -->
    <div class="panel panel-flat">  

        <div class="table-responsive" id="div_konten" style="display:none">
            <table class="table" id="tbl_forum" width="100%">
                <thead>
                    <tr class="bg-blue border-double">
                        <th colspan="4">Forum</th>
                        <th  width="10%" class="text-right"> 
                            <button class="btn btn-danger btn-labeled btn-xs" id="btn_add_forum"><b><i class="icon-pencil"></i></b>Buat Forum</button>
                        </th>
                    </tr>
                    <tr class="active border-double">
                        <th width="20%">Create By</th>																								
                        <th width="40%">Forum</th>
                        <th width="10%">Statistik</th>
                        <th width="20%">Last Posts</th> 
                        <th width="10%">Aksi</th> 
                    </tr>
                </thead>
                <tbody> 
                </tbody>
            </table>
        </div>   

        <div id="div_forum"  style="display:none" > 
            <div class="panel-heading">
                <h6 class="panel-title title_forum"></h6>
            </div>

            <div class="panel-body descr_forum"> 
            </div>

            <div class="panel-footer">
                <div class="heading-elements" style="background-color:beige">
                    <span class="heading-text text-semibold text-size-mini create_forum"></span>
                    <ul class="icons-list pull-right">
                        <li><span class="display-block text-muted text-size-mini date_forum"></span></li>
                    </ul>
               	</div>
            </div>
        </div>

        <div class="table-responsive" id="div_topik"  style="display:none" >
            <input type="hidden" name="flagD" id="flagD" value="0">
            <table class="table text-nowrap"  id="tbl_topik">
                <thead>
                    <tr class="bg-blue border-double">
                        <td colspan="3">Topik</td>
                        <td class="text-right"> 
                            <button class="btn btn-success btn-labeled btn-xs" id="btn_add_topik"><b><i class="icon-pencil"></i></b>Buat Topik</button>
                            <button class="btn btn-danger btn-labeled btn-xs" id="btn_back_topik"><b><i class="icon-arrow-left7"></i></b>.</button>
                        </td>
                    </tr>
                    <tr class="active border-double"> 
                        <th width="60%">Judul </th>
                        <th width="20%">Statistik</th>
                        <th width="10%">Last Posts</th> 
                        <th width="10%">Aksi</th> 
                        <th style="display:none"></th> 
                    </tr>
                </thead>
                <tbody> 
                </tbody>
            </table>
        </div> 

<!--        <div class="panel-body" id="div_reply"  style="display:none" > 
            <div class="panel-heading">
                <button type="button" class="btn btn-xs bg-teal-400 btn-labeled btn_balas"><b><i class="icon-reply"></i></b> Balas</button> 
                
            </div> 
            <div class="panel panel-white" id="div_header"> 	
                <div class="panel-heading">
                    <span class="badge bg-blue-700 pull-right"></span> 
                    <span class="text-semibold" id="div_header_title"></span>
                    <span class="display-block text-muted text-size-mini" id="div_header_by"></span> 
                </div>

                <div class="panel-body" id="div_header_descr"> 
                </div>
            </div>
            <div id="div_detil">

            </div>

        </div>-->
        <div class="panel-body" id="form_forum" style="display:none"> 
            <div class="table-responsive">
                <div class="col-md-12">
                    <form name="myForum" id="myForum" class="form-horizontal"> 
                        <div class="panel panel-flat"> 
                            <div class="panel-body">
                                <input type="hidden" class="form-control" name="id_forum" id="id_forum" maxlength="3">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nama Forum</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="nama_forum" id="nama_forum" maxlength="255">
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Deskripsi</label>
                                    <div class="col-lg-10">
										<span id="count"></span>
                                        <textarea rows="6" cols="100" class="form-control" id="deskripsi" name="deskripsi"></textarea>

                                    </div>
                                </div> 
                                <div class="text-right"> 
                                    <input type="hidden" id="mode" name="mode" value="add"> 
                                    <button type="submit" class="btn btn-primary btn-labeled btn_save_forum"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
                                    <button type="button" class="btn btn-danger btn-labeled" id="btn_cancel_forum"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
                                </div>
                            </div> 
                        </div>
                    </form>

                </div> 
            </div>
        </div>
        <div class="panel-body" id="form_topik" style="display:none"> 
            <div class="table-responsive">
                <div class="col-md-12">
                    <form name="myTopik" id="myTopik" class="form-horizontal"> 
                        <div class="panel panel-flat"> 
                            <div class="panel-body">

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nama Topik</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="nama_topik" id="nama_topik" maxlength="255">
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Deskripsi</label>
                                    <div class="col-lg-10">
                                        <textarea rows="6" cols="100" class="form-control" id="deskripsi_topik" name="deskripsi_topik"></textarea>

                                    </div>
                                </div> 
                                <div class="text-right"> 
                                    <input type="hidden" id="mode" name="mode" value="add"> 
                                    <input type="hidden" class="form-control" name="id_forumt" id="id_forumt">
                                    <input type="hidden" class="form-control" name="id_topik" id="id_topik">
                                    <button type="submit" class="btn btn-primary btn-labeled btn_save_topik"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
                                    <button type="button" class="btn btn-danger btn-labeled" id="btn_cancel_topik"><b><i class="icon-arrow-left13"></i></b> Kembali </button> 
                                </div>
                            </div> 
                        </div>
                    </form>

                </div> 
            </div>
        </div>

    </div>
</div>
<!-- /framed panel body table -->
<?php $this->load->view($footer); ?>
</div> 
<div id="modal_default" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Konfirmasi Hapus</h5>
            </div>
            <div class="modal-body">
                <h6 class="text-semibold"></h6>
                <p id="txtHapus"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-labeled btnHapus"><b><i class="icon-trash-alt"></i></b>Hapus</button> 
                <button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>

            </div>
        </div>
    </div>
</div>
<div id="modal_default1" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Konfirmasi Hapus</h5>
            </div>
            <div class="modal-body">
                <h6 class="text-semibold"></h6>
                <p id="txtHapusT"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-labeled btnHapusT"><b><i class="icon-trash-alt"></i></b>Hapus</button> 
                <button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>

            </div>
        </div>
    </div>
</div>
<!-- /content area --> 
<script src="<?php echo base_url();?>global_assets/js/backend/forum.js"></script>    

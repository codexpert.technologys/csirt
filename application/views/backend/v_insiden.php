<!-- Page header -->
<script src="<?php echo base_url() ?>global_assets/js/plugins/forms/wizards/steps.min.js"></script>
<script src="<?php echo base_url() ?>global_assets/js/demo_pages/wizard_steps.js"></script>
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-server position-left"></i> <?php echo $title; ?></h4>
        </div> 
        <div class="heading-elements add_heading-elements">
            <div class="heading-btn-group">
                <a href="#" class="btn btn-link btn-float has-text" id="btn_add_insiden"><i class="icon-googleplus5 text-primary"></i><span>Tambah</span></a>  
            </div>
        </div> 
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home position-left"></i><?php echo $title; ?></a></li> 
        </ul>

    </div>
</div>
<!-- Content area -->
<div class="content">

    <!-- Framed panel body table -->
    <div class="panel panel-flat"> 
        <div class="panel-body list_insiden" style="display:''"> 
            <div class="table-responsive">
                <table id="tbl_insiden" class="table table-xxs datatable-basic table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-primary"> 
                            <th width="30%">Nama Lengkap</th>
                            <th width="20%">Organisasi</th>
                            <th width="20%">Tgl Buat</th>
                            <th width="15%">Status</th>
                            <th width="15%">Aksi</th> 
                        </tr>
                    </thead>
                    <tbody>  
                    </tbody>
                </table>   
            </div> 
        </div>
        <div class="panel-body form_insiden" style="display:none"> 
            <div class="table-responsive">
                <div class="col-md-12"> 

                    <form class="form-horizontal"  id="myForm" action="#">
                        <h6>A. Informasi Umum</h6>
                        <fieldset> 
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="text-semibold">1. Identitas Pelapor </legend>
                                        <input type="hidden" class="form-control" name="id_insiden" id="id_insiden">  
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Nama Lengkap:<span class="text-danger">*</span></label>
                                            <div class="col-lg-9">
                                                <input readonly type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" required="required" value="<?php echo  $this->session->userdata("name");?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Telp Organisasi:<span class="text-danger">*</span></label>
                                            <div class="col-lg-9">
                                                <input type="text" name="telp_organisasi" id="telp_organisasi" class="form-control" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Organisasi:<span class="text-danger">*</span></label>
                                            <div class="col-lg-9">
                                                <input type="text" id="organisasi" name="organisasi" class="form-control" required="required">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="text-semibold">.</legend>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Email:<span class="text-danger">*</span></label>
                                            <div class="col-lg-9">
                                                <input readonly type="email" name="email" id="email" class="form-control" required="required">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">HandPhone:<span class="text-danger">*</span></label>
                                            <div class="col-lg-9">
                                                <input type="text" id="handphone" name="handphone" class="form-control" required="required" value="<?php echo  $this->session->userdata("name");?>">
                                            </div>
                                        </div> 
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend class="text-semibold">2. Tipe Laporan</legend>  
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Tipe : </label>
                                            <div class="col-lg-9">
                                                <label class="radio-inline">
                                                    <input type="radio"  name="tipe_laporan" value="AW" checked="checked">
                                                    Awal
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio"  name="tipe_laporan" value="TL">
                                                    Tindak Lanjut
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio"  name="tipe_laporan" value="AK">
                                                    Akhir
                                                </label>
                                            </div>
                                        </div> 
                                    </fieldset>
                                </div>
                            </div>	
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend class="text-semibold">3. Waktu Terjadinya Insiden</legend>  
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Tanggal :   </label>
                                            <div class="col-lg-9"> 
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-calendar3"></i></span>
                                                    <input type="date" name="tgl_insiden" id="tgl_insiden" class="form-control daterange-single" value=""> 
                                                </div>

                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Pukul: </label>
                                            <div class="col-lg-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-watch2"></i></span>
                                                    <input type="time" class="form-control" name="jam_insiden" id="jam_insiden">
                                                </div>
                                            </div>
                                        </div> 
                                    </fieldset>
                                </div>
                            </div>	
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend class="text-semibold">4. Tipe Insiden</legend>  
                                        <div class="form-group" id="ti">
                                            <?php
                                            foreach ($insiden as $row) {
                                                echo '<div class="radio">
                                                        <label>
                                                            <input type="checkbox" value="' . $row->id . '" name="tipe_insiden[]" class=""> ' . $row->deskripsi . ' 
                                                        </label>
                                                    </div>';
                                            }
                                            ?>  
                                        </div>  
                                    </fieldset>
                                </div>
                            </div>	
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend class="text-semibold">5. Deskripsi Insiden disertasi bukti (Screenshot, Domain Name, URL, Email, dll)</legend>  
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Deskripsi :<span class="text-danger">*</span></label>
                                            <div class="col-lg-9"> 
                                                <textarea class="form-control" id="deskripsi_insiden" name="deskripsi_insiden" rows="3" ></textarea>

                                            </div>
                                        </div>  
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Upload File (Bukti Insiden) :<span class="text-danger">*</span></label>
                                            <div class="col-lg-9"> 
                                                <input type="file" name="fileToUpload" id="fileToUpload"  class="file-styled"> 
                                                <input type="hidden" name="file_name" id="file_name">
                                                <span id="links"></span>
                                            </div>
                                        </div> 
                                    </fieldset>
                                </div>
                            </div>	
							<div class="row">
                                <div class="col-md-12">
                                    <fieldset> 
										 <div class="form-group divMsg" style="display :''">
		                                    <label class="col-sm-3 control-label">.</label>
		                                    <div class="col-sm-9"> <span id="message">pdf Only</span>
		                                    </div>
		                                </div>
                                    </fieldset>
                                </div>
                            </div>	
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend class="text-semibold">6. Dampak Insiden</legend>  
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Dampak :</label>
                                            <div class="col-lg-9">
                                                <label class="radio-inline">
                                                    <input type="radio" name="dampak_insiden" value="P" checked="checked">
                                                    Jaringan Publik
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="dampak_insiden" value="I">
                                                    Jaringan Internal
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="dampak_insiden" value="L">
                                                    Lainnya
                                                </label>
                                            </div>
                                        </div> 
                                    </fieldset>
                                </div>
                            </div>	
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend class="text-semibold">7. Tindakan Penanggulangan Insiden</legend>  
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">a. Respon Cepat/Awal(Jangka Pendek):</label>
                                            <div class="col-lg-9"> <textarea class="form-control" id="respon_pdk" name="respon_pdk"></textarea>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">b. Jangka Panjang:</label>
                                            <div class="col-lg-9"> <textarea class="form-control" id="respon_pjg" name="respon_pjg"></textarea>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">c. Apakah perencanaan BackUp System berhasil diimplementasikan? 
                                                Jika iya, deskripsikan proses tersebut
                                                : </label>
                                            <div class="col-lg-9"> <textarea class="form-control" id="respon_bak" name="respon_bak"></textarea>
                                            </div>
                                        </div> 
                                    </fieldset>
                                </div>
                            </div>	
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend class="text-semibold">8. Apakah Organisasi lain dilaporkan? Jika iya, sebutkan</legend>  
                                        <div class="form-group"> 
                                            <div class="col-lg-12"> 
                                                <textarea class="form-control" id="laporan_lain" name="laporan_lain" rows="3" ></textarea>

                                            </div>
                                        </div>  
                                    </fieldset>
                                </div>
                            </div> 
                        </fieldset>

                        <h6>B. Informasi Khusus</h6>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend class="text-semibold">9. Aset kritis yang terkena dampak</legend>  
                                        <div class="form-group"> 
                                            <div class="col-lg-12"> 
                                                <textarea class="form-control" id="aset_kritis" name="aset_kritis" rows="3" ></textarea>

                                            </div>
                                        </div> 
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset> 
                                        <legend class="text-semibold">10. Dampak insiden terhadap aset</legend>  
                                        <div class="form-group"> 
                                            <div class="col-lg-12"> 
                                                <textarea class="form-control" id="dampak_aset" name="dampak_aset" ></textarea>

                                            </div>
                                        </div>   
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset> 
                                        <legend class="text-semibold">11. Jumlah pengguna yang terkena dampak</legend>  
                                        <div class="form-group"> 
                                            <div class="col-lg-12"> 
                                                <input type="text" class="form-control" id="jumlah_dampak" name="jumlah_dampak"> 
                                            </div>
                                        </div>  
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset> 
                                        <legend class="text-semibold">12. Dampak terhadap ICT (Information and Communication Technologies)</legend>  
                                        <div class="form-group"> 
                                            <div class="col-lg-12"> 
                                                <input type="text" class="form-control" id="dampak_ict" name="dampak_ict">

                                            </div>
                                        </div>  
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset> 
                                        <legend class="text-semibold">13. Profil Penyerang</legend>  
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">IP Penyerang :</label>
                                            <div class="col-lg-9"> 
                                                <input type="text" class="form-control" id="ip_penyerang" name="ip_penyerang" maxlength="25">

                                            </div>
                                        </div>  
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Port yang diserang:</label>
                                            <div class="col-lg-9"> 
                                                <input type="text" class="form-control" id="port_diserang" name="port_diserang" maxlength="25">

                                            </div>
                                        </div> 
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset> 
                                        <legend class="text-semibold">14. Tipe Serangan </legend>  
                                        <div class="form-group" id="ts">
                                            <?php
                                            foreach ($serangan as $row) {
                                                echo '<div class="radio">
                                                        <label>
                                                            <input type="checkbox" name="tipe_serangan[]" value="' . $row->id . '" class=""> ' . $row->deskripsi . ' 
                                                        </label>
                                                    </div>';
                                            }
                                            ?>  
                                        </div> 
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset> 
                                        <legend class="text-semibold">15. Analisis </legend>  
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">a. Laporan Analisis Log:</label>
                                            <div class="col-lg-7"> 
                                                <label class="radio-inline">
                                                    <input type="radio" name="lap_analisis" value="A" checked="checked">
                                                    Ada
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="lap_analisis" value="T">
                                                    Tidak Ada
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="lap_analisis" value="P">
                                                    Sedang Proses
                                                </label>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">b. Laporan Forensik:</label>
                                            <div class="col-lg-7"> 
                                                <label class="radio-inline">
                                                    <input type="radio" name="lap_forensik" value="A" checked="checked">
                                                    Ada
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="lap_forensik" value="T">
                                                    Tidak Ada
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="lap_forensik" value="P">
                                                    Sedang Proses
                                                </label>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">c. Laporan Audit:</label>
                                            <div class="col-lg-7"> 
                                                <label class="radio-inline">
                                                    <input type="radio" name="lap_audit" value="A" checked="checked">
                                                    Ada
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="lap_audit" value="T">
                                                    Tidak Ada
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="lap_audit" value="P">
                                                    Sedang Proses
                                                </label>
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">d. Laporan analisis lalu lintas jaringan (Network Traffic):</label>
                                            <div class="col-lg-7"> 
                                                <label class="radio-inline">
                                                    <input type="radio" name="lap_traffic" value="A" checked="checked">
                                                    Ada
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="lap_traffic" value="T">
                                                    Tidak Ada
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="lap_traffic" value="P">
                                                    Sedang Proses
                                                </label>
                                            </div>
                                        </div> 
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset> 
                                        <legend class="text-semibold">16. Rincian </legend>  
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">a. Nama dan Versi perangkat:</label>
                                            <div class="col-lg-7"> 
                                                <input type="text" class="form-control" id="nama_perangkat" name="nama_perangkat" maxlength="100">
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">b. Lokasi Perangkat:</label>
                                            <div class="col-lg-7"> 
                                                <input type="text" class="form-control" id="lokasi_perangkat" name="lokasi_perangkat" maxlength="100">
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">c. Sistem Operasi:</label>
                                            <div class="col-lg-7"> 
                                                <input type="text" class="form-control" id="sistem_operasi" name="sistem_operasi" maxlength="50">
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">d. Terakhir Update Sistem Operasi/Firmware:</label>
                                            <div class="col-lg-7"> 
                                                <input type="text" class="form-control" id="last_update_os" name="last_update_os">
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">e. IP Address :</label>
                                            <div class="col-lg-7"> 
                                                <input type="text" class="form-control" id="ip_address" name="ip_address" maxlength="25">
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">f. MAC Address :</label>
                                            <div class="col-lg-7"> 
                                                <input type="text" class="form-control" id="mac_address" name="mac_address" maxlength="25">
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">g. DNS Entry:</label>
                                            <div class="col-lg-7"> 
                                                <input type="text" class="form-control" id="dns_entry" name="dns_entry" maxlength="25">
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">h. Domain/Workgroup:</label>
                                            <div class="col-lg-7"> 
                                                <input type="text" class="form-control" id="domain" name="domain" maxlength="50">
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">i. Apakah perangkat yang terkena dampak terhubung ke jaringan?:</label>
                                            <div class="col-lg-7"> 
                                                <label class="radio-inline">
                                                    <input type="radio" name="konek_jaringan" value="Y" checked="checked">
                                                    Ya
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="konek_jaringan" value="T">
                                                    Tidak
                                                </label>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">j. Apakah perangkat yang terkena dampak terhubung ke modem?:</label>
                                            <div class="col-lg-7"> 
                                                <label class="radio-inline">
                                                    <input type="radio" name="konek_modem" value="Y" checked="checked">
                                                    Ya
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="konek_modem" value="T">
                                                    Tidak
                                                </label>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">k. Adakah pengamanan fisik terhadap perangkat?:</label>
                                            <div class="col-lg-7"> 
                                                <label class="radio-inline">
                                                    <input type="radio" name="pengamanan_fisik" value="Y" checked="checked">
                                                    Ya
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="pengamanan_fisik" value="T">
                                                    Tidak
                                                </label>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">l. Adakah pengamanan logik terhadap perangkat?:</label>
                                            <div class="col-lg-7"> 
                                                <label class="radio-inline">
                                                    <input type="radio" name="pengamanan_logik" value="Y" checked="checked">
                                                    Ya
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="pengamanan_logik" value="T">
                                                    Tidak
                                                </label>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">m. Apakah perangkat sudah diputus dari jaringan?:</label>
                                            <div class="col-lg-7"> 
                                                <label class="radio-inline">
                                                    <input type="radio" name="perangkat_putus" value="Y" checked="checked">
                                                    Ya
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="perangkat_putus" value="T">
                                                    Tidak
                                                </label>
                                            </div>
                                        </div>  
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset> 
                                        <legend class="text-semibold">16. Status Insiden </legend>  
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="status_insiden" name="status_insiden" maxlength="50">
                                        </div> 
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset> 
                                        <legend class="text-semibold">17. Apakah sudah pernah ditawarkan sistem manajemen krisis?</legend>  
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="manajemen_krisis" name="manajemen_krisis" maxlength="50">
                                        </div> 
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset> 
                                        <i>
                                            Catatan :
                                            Segala informasi yang anda berikan tidak akan kami sebarluaskan kepada siapapun dan dijamin kerahasiaannya. 
                                            [*] : wajib diisi.</i>
                                    </fieldset>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <fieldset>  
                                        <div class="form-group">
                                            <input type="hidden" id="mode" name="mode" value="add"> 
                                            <button type="submit" class="btn btn-primary btn-labeled btn_save_insiden"><b><i class="icon-floppy-disk"></i></b> Simpan </button> 
                                            <button type="button" class="btn btn-danger btn-labeled btn_cancel"><b><i class="icon-arrow-left13"></i></b> Kembali </button>  
                                        </div> 
                                    </fieldset>
                                </div>

                        </fieldset>


                    </form> 

                </div> 
            </div>

        </div>
        <div id="temp">

        </div>

        <div id="customers" style="display:none"> 
        </div>
        <input type="hidden" name="flagPdf" id="flagPdf" value="0">
    </div> 
    <!-- /framed panel body table -->
    <?php $this->load->view($footer); ?>
</div>

<div id="modal_default" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Konfirmasi Hapus</h5>
            </div>
            <div class="modal-body">
                <h6 class="text-semibold"></h6>
                <p id="txtHapus"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-labeled btnHapus"><b><i class="icon-trash-alt"></i></b>Hapus</button> 
                <button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>

            </div>
        </div>
    </div>
</div>
<div id="modal_default1" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Konfirmasi Kirim</h5>
            </div>
            <div class="modal-body">
                <h6 class="text-semibold"></h6>
                <p id="txtKirim"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-labeled btnSend"><b><i class="icon-trash-alt"></i></b>Kirim</button> 
                <button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>

            </div>
        </div>
    </div>
</div>
<div id="modal_default2" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Download PDF</h5>
            </div>
            <div class="modal-body">
                <h6 class="text-semibold"></h6>
                <p id="txtKirim"></p>
            </div>
			<form action="<?php echo base_url()?>insiden/get_data">
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-labeled"><b><i class="icon-trash-alt"></i></b>Download</button> 
                <button type="button" class="btn btn-danger btn-labeled" data-dismiss="modal"><b><i class="icon-arrow-left13"></i></b> Tutup</button>

            </div>
			</form>
        </div>
    </div>
</div>
<!-- /content area --> 
<script src="<?php echo base_url() ?>global_assets/js/jspdf.min.js"></script>   
<script src="<?php echo base_url();?>global_assets/js/backend/insiden.js"></script>    

<?php  
header('X-Frame-Options: DENY');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>:: <?php echo (isset($logo->title) ? $logo->title : ""); ?> ::</title>

	<!-- Global stylesheets -->
	        <link rel="icon" type="image/png" href="<?php echo base_url(); ?><?php echo (isset($logo->nama_file) ? $logo->path_file .$logo->nama_file : ""); ?>"/> 
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?php echo base_url() ?>global_assets/js/plugins/loaders/pace.min.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/core/libraries/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/core/libraries/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?php echo base_url() ?>global_assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script src="<?php echo base_url();?>global_assets/js/app.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/demo_pages/login.js"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container bg-slate-800">

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Advanced login -->
					<?php echo form_open('login/validate');?>
						<div class="panel panel-body login-form">
							<div class="text-center">  
								<div class="icon-object border-info-400 text-info-400"><img width="64" height="64" src="<?php echo (isset($logo->nama_file) ? base_url().$logo->path_file .$logo->nama_file : ""); ?>" alt="LOGO"></div>
								<h5 class="content-group-lg"><?php echo (isset($logo->title) ? $logo->title : ""); ?><small class="display-block">Enter your credentialsd </small></h5>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" placeholder="Username" name="username" value="">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control" placeholder="Password" name="password" value="">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>
							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" placeholder="Captcha" name="captcha">
								<div class="form-control-feedback">
									<i class="icon-pencil6 text-muted"></i>
								</div> 
							</div>
							<div class="form-group has-feedback has-feedback-left"> 
								<button class="btn bg-teal" disabled type="button"><?php echo $image; ?></button> <font color="red"><?php echo $msg; ?> </font>
							</div>



							<div class="form-group">
								<button type="submit" class="btn bg-blue btn-block">Login <i class="icon-circle-right2 position-right"></i></button>
							</div>
							
							<div class="text-center">
								<a href="<?php echo base_url()."login/register" ?>">Register</a>
							</div>
 
						</div>
					</form>
					<!-- /advanced login -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>

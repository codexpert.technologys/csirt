<!-- Header -->
        <header>
            <!-- Header desktop -->
            <div class="container-menu-desktop">
                <div class="topbar">
                    <div class="content-topbar container h-100">
                        <div class="left-topbar">
                            <span class="left-topbar-item flex-wr-s-c">
                                <span>
                                    <img src="<?php echo base_url() ?>images/in-flag.png"> Official Website <?php echo $logo->title ?>
                                </span>  
                            </span>
                        </div>

                        <div class="right-topbar">
                            <img src="<?php echo base_url() ?>images/in-flag.png"> &nbsp;
                            <img src="<?php echo base_url() ?>images/en-flag.png">  
                        </div>
                    </div>
                </div>

                <!-- Header Mobile -->
                <div class="wrap-header-mobile">
                    <!-- Logo moblie -->		
                    <div class="logo-mobile">
                        <a href="<?php echo base_url(); ?>"><img width="64" height="64"  src="<?php echo base_url(); ?><?php echo (isset($logo->nama_file) ? $logo->path_file .$logo->nama_file : ""); ?>" alt="LOGO"></a>
                    	<font size="3"><b><?php echo $logo->title ?></b></font></h3>
					</div>

                    <!-- Button show menu -->
                    <div class="btn-show-menu-mobile hamburger hamburger--squeeze m-r--8">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </div>
                </div>

                <!-- Menu Mobile -->
                <div class="menu-mobile">
                    <ul class="topbar-mobile">
                        <li class="left-topbar">
                            <span class="left-topbar-item flex-wr-s-c">
                                <span>
                                    <img src="<?php echo base_url() ?>images/in-flag.png"> Official Website <?php echo $logo->title ?>
                                </span> 
                            </span>
                        </li> 
                        <li class="right-topbar">
                            <img src="<?php echo base_url() ?>images/in-flag.png"> &nbsp;
                            <img src="<?php echo base_url() ?>images/en-flag.png">  
                        </li>
                    </ul>

                    <?php $this->load->view("portal/menu-m"); ?> 
                </div>
                <!-- End Menu Mobile -->
                <!--  -->
                <div class="wrap-logo container">
                    <!-- Logo desktop -->		
                    <div class="logo">
                        <a href="<?php echo base_url(); ?>"><img width="64" height="64"  src="<?php echo base_url(); ?><?php echo (isset($logo->nama_file) ? $logo->path_file . $logo->nama_file : ""); ?> " alt="LOGO"></a>
						<font size="5"><b><?php echo $logo->title ?></b></font></h3>
                    </div>	

                    <!-- Banner -->
                    <div class="banner-header">
                            <!-- <a href="#"><img src="images/banner-01.jpg" alt="IMG"></a> -->
                    </div>
                </div>	

                <!--  -->
                <div class="wrap-main-nav">
                    <div class="main-nav">
                        <!-- Menu desktop -->
                        <?php $this->load->view("portal/menu"); ?>
                    </div>
                </div>	
            </div>
        </header>
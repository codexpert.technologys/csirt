<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
 
	<script src="<?php echo base_url() ?>global_assets/js/core/libraries/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/core/libraries/bootstrap.min.js"></script> 
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?php echo base_url() ?>global_assets/js/plugins/media/fancybox.min.js"></script> 
	<script src="<?php echo base_url() ?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script>

	<script src="<?php echo base_url();?>global_assets/js/app.js"></script>
	<script src="<?php echo base_url() ?>global_assets/js/demo_pages/gallery_library.js"></script>
	<!-- /theme JS files -->

</head>

<body> 


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content"> 

			<!-- Main content -->
			<div class="content-wrapper">
 
				<!-- Content area -->
				<div class="content">

					<!-- Media library -->
					<div class="panel panel-white">
						<div class="panel-heading">
							<h6 class="panel-title text-semibold">Media library emulation</h6>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<table class="table table-striped media-library table-lg">
	                        <thead>
	                            <tr>
	                            	<th><input type="checkbox" class="styled"></th>
	                                <th>Preview</th>
	                                <th>Name</th>
	                                <th>Author</th>
	                                <th>Date</th>
	                                <th>File info</th>
	                                <th class="text-center">Actions</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Ignorant saw her drawings</a></td>
			                        <td><a href="#">Eugene Kopyov</a></td>
			                        <td>Jun 10, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 215 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Case oh an that or away sigh</a></td>
			                        <td><a href="#">James Alexander</a></td>
			                        <td>Jun 9, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 636 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .png</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Acuteness you exquisite ourselves</a></td>
			                        <td><a href="#">Jeremy Victorino</a></td>
			                        <td>Jun 9, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 295 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .png</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Enquire ye without it garrets</a></td>
			                        <td><a href="#">Margo Baker</a></td>
			                        <td>Jun 8, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 593 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .png</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Interest received followed</a></td>
			                        <td><a href="#">Monica Smith</a></td>
			                        <td>Jun 8, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 993 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">His exquisite sincerity</a></td>
			                        <td><a href="#">Bastian Miller</a></td>
			                        <td>Jun 9, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 472 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">So we me unknown</a></td>
			                        <td><a href="#">Jordana Mills</a></td>
			                        <td>Jun 6, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 364 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Sufficient impossible him may</a></td>
			                        <td><a href="#">Buzz Brenson</a></td>
			                        <td>May 29, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 643 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .png</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Oppose exeter income simple</a></td>
			                        <td><a href="#">Zachary Willson</a></td>
			                        <td>Jun 2, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 643 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .png</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Scale began quiet up short</a></td>
			                        <td><a href="#">William Miles</a></td>
			                        <td>Jun 5, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 633 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .png</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Sportsmen shy forfeited</a></td>
			                        <td><a href="#">Freddy Walden</a></td>
			                        <td>Jun 7, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 543 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">View fine me gone this</a></td>
			                        <td><a href="#">Dori Laperriere</a></td>
			                        <td>Jun 1, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 655 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Compact greater and demands</a></td>
			                        <td><a href="#">Vanessa Aurelius</a></td>
			                        <td>May 28, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 237 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Park be fine easy am size</a></td>
			                        <td><a href="#">Monica Smith</a></td>
			                        <td>May 20, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 902 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">As society explain country</a></td>
			                        <td><a href="#">Jordana Mills</a></td>
			                        <td>May 20, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 983 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Sentiments nor everything off</a></td>
			                        <td><a href="#">Buzz Brenson</a></td>
			                        <td>May 21, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 760 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Concerns greatest margaret</a></td>
			                        <td><a href="#">Jeremy Victorino</a></td>
			                        <td>May 3, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 278 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Door neat week do find</a></td>
			                        <td><a href="#">Eugene Kopyov</a></td>
			                        <td>May 10, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 578 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Unpacked endeavor steepest</a></td>
			                        <td><a href="#">Margo Baker</a></td>
			                        <td>May 18, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 893 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Painted no or affixed</a></td>
			                        <td><a href="#">James Alexander</a></td>
			                        <td>May 9, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 534 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Exposed neither pressed</a></td>
			                        <td><a href="#">Bastian Miller</a></td>
			                        <td>May 8, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 689 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Warrant present garrets</a></td>
			                        <td><a href="#">Freddy Walden</a></td>
			                        <td>May 20, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 410 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Now seven world think</a></td>
			                        <td><a href="#">Zachary Willson</a></td>
			                        <td>May 4, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 357 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .jpg</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Up unwilling eagerness</a></td>
			                        <td><a href="#">William Miles</a></td>
			                        <td>May 12, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 346 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .png</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>

	                            <tr>
	                            	<td><input type="checkbox" class="styled"></td>
			                        <td>
				                        <a href="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" data-popup="lightbox">
					                        <img src="<?php echo base_url() ?>global_assets/images/placeholders/placeholder.jpg" alt="" class="img-rounded img-preview">
				                        </a>
			                        </td>
			                        <td><a href="#">Collecting if sympathize</a></td>
			                        <td><a href="#">Jordana Mills</a></td>
			                        <td>May 16, 2015</td>
			                        <td>
			                        	<ul class="list-condensed list-unstyled no-margin">					                        		
				                        	<li><span class="text-semibold">Size:</span> 378 Kb</li>
				                        	<li><span class="text-semibold">Format:</span> .png</li>
			                        	</ul>
			                        </td>
			                        <td class="text-center">
			                            <ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
													<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
													<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
												</ul>
											</li>
										</ul>
			                        </td>
	                            </tr>
	                        </tbody>
	                    </table>
                    </div>
                    <!-- /media library -->


					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>

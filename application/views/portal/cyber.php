<p>Dalam rangka membangun kesiapan stakeholder Pemerintah, Gov-CSIRT Indonesia menyelenggarakan kegiatan <em>Cyber Security Drill Test</em>. Pada kegiatan ini disimulasikan peserta sebagai system administrator yang memiliki aset berupa perangkat jaringan, server dan website, kemudian disimulasikan terjadinya insiden siber seperti DDOS, <em>web defacement</em>, dan <em>ransomware. </em>Diharapkan peserta dapat memiliki pandangan bagaimana situasi ketika terjadi insiden siber dan cara meresponnya :</p>
 
<ol class="p-t-35">
	<li class="flex-wr-sb-s p-b-22">
		 1. Mampu mengetahui cara mendeteksi adanya insiden siber.
	</li>
	<li class="flex-wr-sb-s p-b-22">
		 2. Mampu menilai dampak insiden siber.
	</li>
	<li class="flex-wr-sb-s p-b-22">
		3. Mampu menilai kapabilitas yang dibutuhkan untuk mengatasi insiden siber.
	</li>
	<li class="flex-wr-sb-s p-b-22">
		4. Mampu merespon insiden siber.
	</li>
	<li class="flex-wr-sb-s p-b-22">
		5. Mampu memulihkan dampak yang disebabkan karena insiden siber.
	</li> 
</ol>
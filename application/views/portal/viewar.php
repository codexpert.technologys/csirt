<?php  
header('X-Frame-Options: DENY');
?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>:: <?php echo $logo->title; ?> ::</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->	
        <link rel="icon" type="image/png" href="<?php echo base_url(); ?><?php echo (isset($logo->nama_file) ? $logo->path_file .$logo->nama_file : ""); ?>"/> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/css/bootstrap/bootstrap.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/fonts/fontawesome-5.0.8/css/fontawesome-all.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/vendor/animate/animate.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/vendor/css-hamburgers/hamburgers.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/vendor/animsition/css/animsition.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/css/util.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/css/main.css">
        <link href="<?php echo base_url() ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/styles.css" rel="stylesheet" type="text/css"> 
        <script>var ci = {
                baseurl: "<?php echo base_url(); ?>"
            }
        </script>
    </head>
    <body class="animsition">
        <?php $this->load->view("portal/header"); ?>

        <!-- Breadcrumb -->
        <div class="container">
            <div class="headline bg0 flex-wr-sb-c p-rl-20 p-tb-8">
                <div class="f2-s-1 p-r-30 m-tb-6">
                    <?php echo
                    '<a href="' . base_url() . '" class="breadcrumb-item f1-s-3 cl9">
                            Beranda
                    </a>
                    <span class="breadcrumb-item f1-s-3 cl9">
                             ' . $title . '
                    </span>';
                    ?>
                </div> 
            </div>
        </div>

        <!-- Content -->
        <section class="bg0 p-b-140 p-t-10">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8 p-b-30">
                        <div class="p-r-10 p-r-0-sr991">
                            <!-- Blog Detail -->
                            <div class="p-b-70">
                                <h3 class="f1-l-3 cl2 p-b-16 p-t-33 respon2">
                                     
                                </h3>
                                <p class="f1-s-11 cl6 p-b-25">
                                    <?php $this->load->view($page); ?>
                                </p>  
                            </div> 
                        </div>
                    </div>

                    <!-- Sidebar -->
                    <div class="col-md-10 col-lg-4 p-b-30">
                        <div class="p-l-10 p-rl-0-sr991 p-t-70">	 
                            <?php $this->load->view("portal/side_archieve"); ?>
							
							<?php $this->load->view("portal/side_peringatan"); ?>

                            <?php $this->load->view("portal/side_layanan"); ?>

                            <?php $this->load->view("portal/side_rfc2350"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer -->
            <?php $this->load->view("portal/footer"); ?>
        <!-- Back to top -->
        <div class="btn-back-to-top" id="myBtn">
            <span class="symbol-btn-back-to-top">
                <span class="fas fa-angle-up"></span>
            </span>
        </div>
        <script src="<?php echo base_url() ?>global_assets/vendor/jquery/jquery-3.2.1.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/demo_pages/datatables_basic.js"></script>
        <script src="<?php echo base_url() ?>global_assets/vendor/animsition/js/animsition.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/vendor/bootstrap/js/popper.js"></script>
        <script src="<?php echo base_url() ?>global_assets/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/main.js"></script>
        <script src="<?php echo base_url();?>global_assets/js/portal.js"></script>
    </body>
</html>
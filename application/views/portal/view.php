<?php  
header('X-Frame-Options: DENY');
?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>:: <?php echo $logo->title; ?> ::</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->	
        <link rel="icon" type="image/png" href="<?php echo base_url(); ?><?php echo (isset($logo->nama_file) ? $logo->path_file .$logo->nama_file : ""); ?>"/> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/css/bootstrap/bootstrap.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/fonts/fontawesome-5.0.8/css/fontawesome-all.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/vendor/animate/animate.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/vendor/css-hamburgers/hamburgers.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/vendor/animsition/css/animsition.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/css/util.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/css/main.css">

    </head>
    <body class="animsition">
        <?php $this->load->view("portal/header"); ?>

        <!-- Breadcrumb -->
        <div class="container">
            <div class="headline bg0 flex-wr-sb-c p-rl-20 p-tb-8">
                <div class="f2-s-1 p-r-30 m-tb-6">
                    <?php echo
                    '<a href="' . base_url() . '" class="breadcrumb-item f1-s-3 cl9">
					Beranda
                    </a>

                    <a href="#" onclick="javascript:history.back()" class="breadcrumb-item f1-s-3 cl9">
                            ' . (isset($dt->kategori) ? $dt->kategori : "") . '
                    </a>
                    <span class="breadcrumb-item f1-s-3 cl9">
                             ' . (isset($dt->title) ? $dt->title : ""). '
                    </span>';
                    ?>
                </div> 
            </div>
        </div>

        <!-- Content -->
        <section class="bg0 p-b-140 p-t-10">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8 p-b-30">
                        <div class="p-r-10 p-r-0-sr991">
                            <!-- Blog Detail -->
                            <div class="p-b-70">
                                <h3  class="f1-l-3 cl2 p-b-16 p-t-33 respon2">
                                    <?php echo (isset($dt->title) ? $dt->title : ""); ?>
                                </h3>

                                <div class="flex-wr-s-s p-b-40">
                                    <span class="f1-s-3 cl8 m-r-15">
                                        <span>
                                            <?php echo (isset($dt->kategori) ? $dt->kategori : redirect('portal') ); ?>
                                        </span>
                                    </span> 
                                </div> 

                                <p class="f1-s-11 cl6 p-b-25" style="text-align:justify">
                                    <?php echo html_entity_decode(isset($dt->long_desc) ? $dt->long_desc : ""); ?><br>
                                    <?php if (isset($dt->judul_video) ? $dt->judul_video : "") { ?> 
                                    <div>
                                        <div class="wrap-pic-w pos-relative"> 
                                            <button class="s-full ab-t-l flex-c-c fs-32 cl0 hov-cl10 trans-03" data-toggle="modal" data-target="#exampleModal">
                                                <span class="fab fa-youtube"></span>
                                            </button>
                                        </div>

                                        <div class="p-tb-16 p-rl-25 bg3">

                                            <h5 class="p-b-5">
                                                <a href="#" class="f1-m-3 cl0 hov-cl10 trans-03">
                                                    [Video] <?php echo $dt->judul_video; ?>
                                                </a>
                                            </h5>
                                        </div>
                                    </div>	
                                <?php }
                                ?>

                                </p>  
                            </div> 
                        </div>
                    </div>

                    <!-- Sidebar -->
                    <div class="col-md-10 col-lg-4 p-b-30">
                        <div class="p-l-10 p-rl-0-sr991 p-t-70">						
                            <?php $this->load->view("portal/side_archieve"); ?>
							
							<?php $this->load->view("portal/side_peringatan"); ?>

                            <?php $this->load->view("portal/side_layanan"); ?>

                            <?php $this->load->view("portal/side_rfc2350"); ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer -->
        <?php $this->load->view("portal/footer"); ?>
        <!-- Back to top -->
        <div class="btn-back-to-top" id="myBtn">
            <span class="symbol-btn-back-to-top">
                <span class="fas fa-angle-up"></span>
            </span>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?php echo $dt->judul_video; ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">�</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <video id="" width="100%" controls poster="video/thumb.jpg">
                            <source src="<?php echo base_url() . $dt->path_file . $dt->nama_file; ?>"> 
                            Your browser does not support the video tag.
                        </video>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
                    </div>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url() ?>global_assets/vendor/jquery/jquery-3.2.1.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/vendor/animsition/js/animsition.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/vendor/bootstrap/js/popper.js"></script>
        <script src="<?php echo base_url() ?>global_assets/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/main.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#exampleModal').modal({
                    show: false
                }).on('hidden.bs.modal', function () {
                    $(this).find('video')[0].pause();
                });
            });
        </script>
    </body>
</html>
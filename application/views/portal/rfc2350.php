<article>
<?php echo ($long_desc != "" ? $long_desc : redirect('portal') ); ?> 
</article> 
<!-- 
<p style="text-align: left;"><strong>RFC 2350 Gov-CSIRT Indonesia</strong><br />
<strong>1. Informasi Mengenai Dokumen</strong><br />
Dokumen ini berisi deskripsi Gov-CSIRT Indonesia berdasarkan RFC 2350, yaitu<br />
informasi dasar mengenai Gov-CSIRT Indonesia, menjelaskan tanggung jawab, layanan<br />
yang diberikan, dan cara untuk menghubungi Gov-CSIRT Indonesia.<br /><br>

<strong>1.1. Tanggal Update Terakhir</strong><br />
Dokumen merupakan dokumen versi 1.0 yang diterbitkan pada tanggal 20 Desember<br />
2018.<br /><br>

<strong>1.2. Daftar Distribusi untuk Pemberitahuan</strong><br />
Tidak ada daftar distribusi untuk pemberitahuan mengenai pembaharuan dokumen.<br /><br>

<strong>1.3. Lokasi dimana Dokumen ini bisa didapat</strong><br />
Versi terbaru dari dokumen ini tersedia pada :<br />
https://govcsirt.bssn.go.id/static/rfc2350/rfc2350-id.pdf (versi Bahasa Indonesia)<br />
https://govcsirt.bssn.go.id/static/rfc2350/rfc2350-en.pdf (versi Bahasa Inggris)<br />
<br>
<strong>1.4. Keaslian Dokumen</strong><br />
Kedua dokumen telah ditanda tangani dengan PGP Key milik Pusat Operasi<br />
Keamanan Siber Nasional (Pusopskamsinas) &#8211; Badan Siber dan Sandi Negara<br />
(BSSN). Untuk lebih jelas dapat dilihat pada Subbab 2.8.<br />
<br>
<strong>1.5 Identifikasi Dokumen</strong><br />
Kedua dokumen (versi bahasa inggris dan bahasa Indonesia) memiliki atribut yang<br />
sama, yaitu :<br />
Judul : RFC 2350 Gov-CSIRT Indonesia<br />
Versi : 1.0<br />
Tanggal Publikasi : 20 Desember 2018<br />
Kedaluwarsa : Dokumen ini valid hingga dokumen terbaru dipublikasikan</p>
<br>
<br>
<p style="text-align: left;"><strong>2. Informasi Data/Kontak</strong><br />
<br>
<br>
<strong>2.1. Nama Tim</strong><br />
Government &#8211; Computer Security Incident Response Team (CSIRT) Indonesia<br />
Disingkat : Gov-CSIRT Indonesia<br />
<br>
<strong>2.2. Alamat</strong><br />
BSSN<br />
Jl. Harsono RM No.70,<br />
Ragunan &#8211; 12550<br />
Pasar Minggu, Jakarta Selatan<br />
Indonesia</p>
<br>
<p style="text-align: left;"><strong>2.3. Zona Waktu</strong><br />
Jakarta (GMT+07:00)<br />
<br>
<strong>2.4. Nomor Telepon</strong><br />
Telepon (021) 78833610<br />
<br>
<strong>2.5. Nomor Fax</strong><br />
Tidak Ada<br />
<br>
<strong>2.6. Telekomunikasi Lain</strong><br />
Tidak Ada<br />
<br>
<strong>2.7. Alamat Surat Elektronik (E-mail)</strong><br />
bantuan70[at]bssn.go.id<br />
<br>
<strong>2.8. Kunci Publik (Public Key) dan Informasi/Data Enkripsi lain</strong><br />
Bits : 4096<br />
ID : 0x73802BD6<br />
Key Fingerprint : 1A35 DAEF E63B BE93 C314 3272 CE5D 2119 7380 2BD6<br />
<br>
&#8212;&#8211;BEGIN PGP PUBLIC KEY BLOCK&#8212;&#8211;<br />
mQINBFtex4QBEADfLdjiJbwGTgOXUwyt/emyua3wIfYufUgpAKAzk2Dz8t9aj5bt<br />
Co3adcXQw+5WnKSHbD7Q2VFUgLd+whIVuf6rAUraMcMrR10xWvvq2x4kEIEQiBXQ<br />
CZOLgbN/9n+u2GqcD3x/XimyUDSN+I7DGh8+CioTWcahRQfcX70AqTlw5+VNFHT6<br />
mrwAYfH8aQN2aPG+vW7j5K3AIEHVYFLYnU8F0FqBpcyFFlAWhqRgp6Jscsn9w0Ty<br />
dR/v8laoaX1iE35XVyX3TXjS8TH+DCBuSP3BV0LVJJyISoEO4X0plKmERGW5UzaQ<br />
CEbawtopt73QgWKcO5DTgMI247X3kekMchU8ENf25LdzrZ8znw8+DH/PggcCu6Hh<br />
R/bccgXoFhQbrieZbDtuXKYn22/jJMWDKpJMQkGsPV2+qlMdYOXRrU87MhBE4dk2<br />
dXLYCJki2qYnwddZp0HxRn6zznQ2VIrF+N3cBnQQB8izBFqcgy6gvkmJiUrGRn9n<br />
upRryX7Wp1djfA13Veb1HftQNauOcWsJQt//fj5+MC9P6r3A4S2rgnojQv3zuPxP<br />
XUVuvZOE0ywqXTfxPd7DdJE3ilP8fLvdWEZoFHlZkBkAZtFFsbjNlEhUc7IBQtOR<br />
B7wRptGQxajH26ru/atRpcfxAFx6pfYG5Hr0X1a7xqmpvPdxFcs5dQ0NnwARAQAB<br />
tDRCYW50dWFuNzAgUHVzb3Bza2Ftc2luYXMgQlNTTiA8YmFudHVhbjcwQGJzc24u<br />
Z28uaWQ+iQJUBBMBCAA+FiEEGjXa7+Y7vpPDFDJyzl0hGXOAK9YFAltex4QCGyMF<br />
CQlmAYAFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AACgkQzl0hGXOAK9Y9lA/+NULC<br />
uXxF+Ko/l3x482/7yJS6oEIhqY17nskNFjmBqM6fwTFdQybarqs1AgxN3ne26MWs<br />
VcmhSLsOaiN7tEnD0jPIRgqCZ4SnXeqthbuIoCb6cMI4Mae1gRRM4pb1ec4OyriW<br />
infNAa+zWolZNuQG0cz/xuVme34Imv3Nv9WutCuyjGR3Renixlg68Sww7tV4x3gw<br />
bkqu/3HReG9t39maeDafw6w0//oHyAqPA8vk36sK9Pt0zjro/q8s8W3NzsHnh/Ca<br />
HX40WsX7oZPGBm0ILdHxwCXhhXmBY/aYBsSIC4AvYLrHRTHFCWxk6B6pL8rte06b<br />
xyzICVQmQJLfn/QF9OhttIpYTY02yppGRRs9Tvyf+xTfZyIMoKeDjJmymZ1D421B<br />
BguRR+zaxdrQwza1B3RlQ+8VKG/Mjf/zmnRAxSssXLm4LJ/KtpFWhI1lUDMhaMc8<br />
fFOxH3Oj+N+tAPvM34LN1EJrDR/r6AggwBciM5ak1gtijlJS85NLFWHbFhQwHdZM<br />
3kiTYNgYJ2uvfq7enswzMk/jy2bjd17UVTzfxpg3gz0iZ52hBnHl8DEtw4l5KQgS<br />
ys4Nmen51ZyIyT+NfD73vk3nS41cI36d4YV97FlQ7rbaitNVFFBKV5fVSfkFrwKW<br />
akU0T8oeCOiNuwLQWXdg3447BLyhNObQWL3YoKq5Ag0EW17HhAEQALwqDRRG5byw<br />
MDLVQTTWdqeK5cezTKw5Ebj50tk0VSTaG3hDfwkwyPJjzTkuwPUEQb+6mEtOqQ2P<br />
x9jlQylumI0Uy3NBd5wkaS9ZxEa9HU70VXeDlvAx+0eJeVNMCUcdgU28/nCnzAIr3+5lseT</p>
<p style="text-align: left;">g4MRrSo9xfgqYFd+QE5wmYGRO7/gXhvMf9vrVr8lcIvWWxYKUGl0bYoct<br />
5ZSepTZ2mDJoJoOeuoTMW0WOfbGHzs1jS950PqXri+n9LzupYc2FF3NEBRw1NuLY<br />
MnwwDkqbGeLSnFEaOXce8BD9Ppnh1CK0dMwTWBCUlAUJXRNWRMN5prREs8gVRkJ0<br />
GuRaMmji+lJHg5HfuXV3zKWJ3UBnCM1MpIpvFReMH2OPHWzeObAXcpg6OsPCk+99<br />
MLjmkC/C2cZtHf48JJCRMtrTUN0165DJFXoJcCinaNRUx+YnHVTC5Wuu4+DVaCzY<br />
5WbNc6LaQkA1PMK9oqBXFDtERXirbacw4kOpvoC+J0B6xnYDrAOQ8cAc9pSO7QSf<br />
Y0NZYhDoJz6o5++TSt6P8Ol/LdsVFIK0TLhf1qiqMhuibCQi6Fom7r6D5wtuZ+22<br />
Mn2Jw8nuMYvo7ze3p5jwoErCpPswh6AqSia8kTgMUoDNPJwuoC7m0dzWvQ99reNr<br />
K9QdhjC3LtclPQolqibVz/Hfm3trciGlABEBAAGJAjwEGAEIACYWIQQaNdrv5ju+<br />
k8MUMnLOXSEZc4Ar1gUCW17HhAIbDAUJCWYBgAAKCRDOXSEZc4Ar1jzKEACI7rht<br />
7nF1cYEZpbwU3u8MTZXSCxu/kgmxYmJlnQhRhahwtWf5N/xn0lJtMGoic5wbpAvu<br />
JqE/5OOTyd3dUx5eOtBjaEFf5Zw1Ar96K9x764YtJIyiq2WYuMK2EEYX8uoqpGCu<br />
9iGqnis1EWOca5cSzjo60McN+UoMSTItja8XgLOAVkIxcz9CepRucBf/yugc6ENT<br />
eUcA6Dv84tO4f0E5aKuXxk7ESMk/Whukz2PCsSaqs5K+1yCAZvU2aio4XYr2GJr9<br />
VpI++A57r7lJqrhdIUJjuJmLGdV1HOTl3ITWkXi4XhNbsgeAjZ9iU3wjl1kSwf3P<br />
aunf2wLww+j9sTuIalZ6UWpnLCRbsA8IkVtFFczuM4NrBktFKx5y1QmdfTHgsmCS<br />
8McEOEOZyGLngRWUwuhrKQI7okrxXhbQGMlNdSQ1luPw0Bx7aYsUWnEFqMOApLAB<br />
2Zm7CqYfrwsNGp6sWAwimO+05AOvr7jcqeBfwYyfDImO0Rf75YjPU6yJ+4NyEUWE<br />
JubnHIYk47fV4T6O7BjvdgHIYHe51qDKo6xxmt32Wcn05fzGxTaPclgBC3krrwNB<br />
vkPTTMDlkJ6fEBe5q476Xs+7RPRmlr4FE5tu7/GoVGKJCKIvXJWCZrYawhACjE8h<br />
WGksMO/XgZgXAA1/KIljfJUJ0rjPMjgktq23Zg==<br />
=yC+0<br />
&#8212;&#8211;END PGP PUBLIC KEY BLOCK&#8212;&#8211;<br />
<br>
<br>
File PGP key ini tersedia pada :<br />
https://bssn.go.id/wp-content/uploads/2018/08/Publik-Key-Bantuan70-pub.asc<br />
<br>
<strong>2.9. Anggota Tim</strong><br />
Ketua Gov-CSIRT Indonesia adalah Direktur Penanggulangan dan Pemulihan<br />
Pemerintah, Deputi Bidang Penanggulangan dan Pemulihan, BSSN. Yang<br />
termasuk anggota tim adalah seluruh staf BSSN di sektor pemerintah.<br />
<br>
<strong>2.10. Informasi/Data lain</strong><br />
Tidak ada.<br />
<br>
<strong>2.11. Catatan-catatan pada Kontak Gov-CSIRT Indonesia</strong><br />
Metode yang disarankan untuk menghubungi Gov-CSIRT Indonesia adalah melalui<br />
e-mail pada alamat bantuan70[at]bssn.go.id atau melalui nomor telepon (021)<br />
78833610 ke Pusopskamsinas yang siaga selama 24/7.</p>
<br>
<br>
<p style="text-align: left;"><strong>3. Mengenai GovCSIRT</strong><br />
<br>
<strong>3.1. Misi</strong><br />
Tujuan dari Gov-CSIRT Indonesia, yaitu :<br />
a. membangun, mengoordinasikan, mengolaborasikan dan mengoperasionalkan<br />
sistem mitigasi, manajemen krisis, penanggulangan dan pemulihan terhadap<br />
insiden keamanan siber pada sektor pemerintah<br />
b. membangun kerja sama dalam rangka penanggulangan dan pemulihan insiden<br />
keamanan siber pada sektor pemerintahc. membangun kapasitas sumber daya penanggulangan dan pemulihan insiden<br />
keamanan siber pada sektor pemerintah<br />
d. mendorong pembentukan CSIRT (Computer Security Incident Response Team)<br />
pada sektor pemerintah<br />
<br>
<strong>3.2. Konstituen</strong><br />
Konstituen Gov-CSIRT Indonesia meliputi Pemerintah Pusat, Pemerintah Daerah<br />
wilayah I, dan II yaitu :<br />
a. Pemerintah Pusat adalah Presiden Republik Indonesia yang memegang<br />
kekuasaan pemerintahan negara Republik Indonesia yang dibantu oleh Wakil<br />
Presiden dan Menteri sebagaimana dimaksud dalam Undang-Undang Dasar<br />
Negara Republik Indonesia Tahun 1945<br />
b. Pemerintah Daerah Wilayah I adalah Pemerintah Daerah Provinsi yang meliputi<br />
wilayah Provinsi Aceh, Sumatera Utara, Riau, Sumatera Barat, Kepulauan<br />
Riau, Jambi, Sumatera Selatan, Bangka Belitung, Bengkulu, Lampung, Daerah<br />
Khusus Ibu Kota Jakarta, Jawa Barat, Banten, Jawa Tengah, Daerah Istimewa<br />
Yogyakarta, Jawa Timur, dan Bali<br />
c. Pemerintah Daerah Wilayah II adalah Pemerintah Daerah Provinsi yang<br />
meliputi wilayah Provinsi Kalimantan Barat, Kalimantan Tengah, Kalimantan<br />
Selatan, Kalimantan Timur, Kalimantan Utara, Sulawesi Utara, Gorontalo,<br />
Sulawesi Tenggara, Sulawesi Tengah, Sulawesi Selatan, Sulawesi Barat, Nusa<br />
Tenggara Timur, Nusa Tenggara Barat, Papua Barat, Papua, Maluku, dan<br />
Maluku Utara<br />
<br>
<strong>3.3. Sponsorship dan/atau Afiliasi</strong><br />
Gov-CSIRT Indonesia merupakan bagian dari BSSN sehingga seluruh pembiayaan<br />
bersumber dari APBN.<br />
<br>
<strong>3.4. Otoritas</strong><br />
Berdasarkan Peraturan Presiden Nomor 53 Tahun 2017 tentang BSSN<br />
sebagaimana telah diubah dengan Peraturan Presiden Nomor 133 Tahun 2017,<br />
Gov-CSIRT Indonesia memiliki kewenangan untuk melakukan penanggulangan<br />
insiden, mitigasi insiden, investigasi dan analisis dampak insiden, serta pemulihan<br />
pasca insiden keamanan siber pada sektor pemerintah.<br />
Gov-CSIRT Indonesia melakukan penanggulangan dan pemulihan atas permintaan<br />
dari konstituennya.</p>
<br>
<br>
<p style="text-align: left;"><strong>4. Kebijakan � Kebijakan</strong><br />
<br>
<strong>4.1. Jenis-jenis Insiden dan Tingkat/Level Dukungan</strong><br />
Gov-CSIRT Indonesia memiliki otoritas untuk menangani berbagai insiden<br />
keamanan siber yang terjadi atau mengancam konstituen kami (dapat dilihat pada<br />
Subbab 3.2).<br />
Dukungan yang diberikan oleh Gov-CSIRT Indonesia kepada konstituen dapat<br />
bervariasi bergantung dari jenis dan dampak insiden.4.2. Kerja sama, Interaksi dan Pengungkapan Informasi/ data<br />
Gov-CSIRT Indonesia akan melakukan kerjasama dan berbagi informasi dengan<br />
CSIRT atau organisasi lainnya dalam lingkup keamanan siber.<br />
Seluruh informasi yang diterima oleh Gov-CSIRT Indonesia akan dirahasiakan.<br />
<br>
<strong>4.3. Komunikasi dan Autentikasi</strong><br />
Untuk komunikasi biasa Gov-CSIRT Indonesia dapat menggunakan alamat e-mail<br />
tanpa enkripsi data (e-mail konvensional) dan telepon. Namun, untuk komunikasi<br />
yang memuat informasi sensitif/terbatas/rahasia dapat menggunakan enkripsi PGP<br />
pada e-mail.</p>
<br>
<br>
<p style="text-align: left;"><strong>5. Layanan</strong><br />
<br>
<strong>5.1. Respon Insiden</strong><br />
Gov-CSIRT Indonesia akan membantu konstituen untuk melakukan<br />
penanggulangan dan pemulihan insiden keamanan siber dengan aspek-aspek<br />
manajemen insiden keamanan siber berikut :<br />
<br>
<strong>5.1.1. Triase Insiden (Incident Triage)</strong><br />
a. Memastikan kebenaran insiden dan pelapor<br />
b. Menilai dampak dan prioritas insiden<br />
<br>
<strong>5.1.2. Koordinasi Insiden</strong><br />
a. Mengkoordinasikan insiden dengan konstituen<br />
b. Menentukan kemungkinan penyebab insiden<br />
c. Memberikan rekomendasi penanggulangan berdasarkan panduan/SOP<br />
yang dimiliki Gov-CSIRT Indonesia kepada konstituen<br />
d. Mengkoordinasikan insiden dengan CSIRT atau pihak lain yang terkait<br />
<br>
<strong>5.1.3. Resolusi Insiden</strong><br />
a. Melakukan investigasi dan analisis dampak insiden<br />
b. Memberikan rekomendasi teknis untuk pemulihan pasca insiden<br />
c. Memberikan rekomendasi teknis untuk memperbaiki kelemahan sistem<br />
Gov-CSIRT Indonesia menyajikan data statistik mengenai insiden yang terjadi pada<br />
sektor pemerintah sebagai bentuk sentra informasi keamanan siber pada sektor<br />
pemerintah.<br />
<br>
<strong>5.2. Aktivitas Proaktif</strong><br />
Gov-CSIRT Indonesia secara aktif membangun kesiapan instansi pemerintah<br />
dalam melakukan penanggulangan dan pemulihan insiden keamanan siber melalui<br />
kegiatan :<br />
a. Cyber Security Drill Test<br />
b. Workshop atau Bimbingan Teknis<br />
c. Asistensi Pembentukan CSIRT organisasi6. Pelaporan Insiden<br />
Laporan insiden keamanan siber dapat dikirimkan ke bantuan70[at]bssn.go.id dengan<br />
melampirkan sekurang-kurangnya :<br />
a. Foto/scan kartu identitas<br />
b. Bukti insiden berupa foto atau screenshoot atau log file yang ditemukan</p>
<br>
<br>
<p style="text-align: left;"><strong>7. Disclaimer</strong><br />
Tidak ada</p> -->
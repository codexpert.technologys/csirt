<nav class="menu-desktop">
	<a class="logo-stick" href="index.html">
		<img src="<?php echo base_url();?>images/icons/logo-01.png" alt="LOGO">
	</a>

	<ul class="main-menu">
		<li class="main-menu-active">
			<a href="<?php echo base_url();?>">Beranda</a> 
		</li>  
		<li>
			<a href="<?php echo base_url();?>portal/profil">Profil</a> 
		</li>  
		<li>
			<a href="<?php echo base_url();?>portal/rfc2350">RFC 2350</a>
			<ul class="sub-menu">
				<?php 
				if ($dt_file != null) {
					foreach($dt_file as $row) {
						echo "<li><a href='".base_url().$row->path_file.$row->nama_file."'>".$row->nama_rfc."</a></li>";
						}
				}
				?>
				 
			</ul>
		</li>  
		<li>
			<a href="<?php echo base_url();?>portal/layanan">Layanan</a>
			<ul class="sub-menu">
				<!-- <li><a href="<?php echo base_url();?>portal/asistensi">Asistensi Pembentukan CSIRT</a></li>
				 <li><a href="<?php echo base_url();?>portal/bimbingan">Bimbingan Teknis</a></li> 
				<li><a href="<?php echo base_url();?>portal/cyber">Cyber Security Drill Test</a></li>
				<li><a href="https://bssn.go.id/koppi-siber">Koppi Siber</a></li> --> 
				<li><a href="<?php echo base_url();?>portal/panduan">Panduan/Pedoman Teknis</a></li> 
				<li><a href="<?php echo base_url();?>portal/galeri">Galeri</a></li> 
			</ul>
		</li> 
		<li>
			<a href="<?php echo base_url();?>portal/aduan">Aduan Siber</a>  
		</li> 
		<li>
			<a href="<?php echo base_url();?>portal/event">Event</a> 
		</li>  
		<li>
			<a href="<?php echo base_url();?>portal/kontak">Hubungi Kami</a> 
		</li> 
	</ul>
</nav>

<footer>
		<div class="bg2 p-t-40 p-b-25">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 p-b-20">
						<div class="size-h-3 flex-s-c">
							<h5 class="f1-m-7 cl0">
								Profil
							</h5>
						</div>

						<div>
							<p class="f1-s-1 cl11 p-b-16">
								Government - Computer Security Incident Response Team (CSIRT) Indonesia, disingkat Gov-CSIRT Indonesia merupakan CSIRT sektor Pemerintah Indonesia yang ditetapkan oleh Kepala Badan Siber dan Sandi Negara dalam Keputusan Kepala Badan Siber dan Sandi Negara Nomor 570 Tahun 2018 tanggal 20 Desember 2018.
							</p> 
						</div>
					</div>
 
					<div class="col-sm-6 col-lg-4 p-b-20">
						<div class="size-h-3 flex-s-c">
							<h5 class="f1-m-7 cl0">
								Link
							</h5>
						</div>

						<ul class="m-t--12">
							<li class="how-bor1 p-rl-5 p-tb-10">
								<a href="<?php echo base_url().'portal/profil' ;?>" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
									Profil
								</a>
							</li>

							<li class="how-bor1 p-rl-5 p-tb-10">
								<a href="<?php echo base_url();?>portal/rfc2350" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
									RFC 2350
								</a>
							</li>

							<li class="how-bor1 p-rl-5 p-tb-10">
								<a href="<?php echo base_url();?>portal/aduan" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
									Aduan Siber
								</a>
							</li>

							<li class="how-bor1 p-rl-5 p-tb-10">
								<a href="<?php echo base_url();?>portal/event" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
									Event
								</a>
							</li>

							<li class="how-bor1 p-rl-5 p-tb-10">
								<a href="<?php echo base_url();?>portal/kontak" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
									Hubungi Kami
								</a>
							</li>
						</ul>
					</div>
					
					<div class="col-lg-4 p-b-20">
						<div class="size-h-3 flex-s-c">
							<h5 class="f1-m-7 cl0">
								<?php echo $logo->title ?>
							</h5>
							
						</div>

						<div>
							<p class="f1-s-1 cl11 p-b-16">
								<?php echo $hubungi->alamat_lokasi. 
								"<br>
								Telepon : " . $hubungi->telepon . "<br>
								Email: " . $hubungi->email; ?>


							</p>

							<p class="f1-s-1 cl11 p-b-16">
								Silahkan gunakan PGP untuk komunikasi e-mail terenkripsi.<br> 
								PGP Key dapat diunduh di <a href="<?php echo base_url(). "assets/Publik-Key-Bantuan70-pub.asc" ;?>">sini</a>.
							</p>
 
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="bg11">
			<div class="container size-h-4 flex-c-c p-tb-15">
				<span class="f1-s-1 cl0 txt-center">
					Hak Cipta <?php echo $logo->title ?> &copy; 2019
				</span>
			</div>
		</div>
	</footer>
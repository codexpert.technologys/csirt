<p>Computer Security Insident Response Team (CSIRT) merupakan tim atau entitas dalam suatu lembaga yang menyediakan layanan dan dukungan kepada organisasi untuk mencegah, mengelola dan menanggapi insiden keamanan informasi. Tim-tim ini biasanya terdiri dari para spesialis yang bertindak sesuai dengan prosedur dan kebijakan untuk merespon dengan cepat dan efektif terhadap insiden keamanan dan untuk mengurangi risiko serangan cyber.</p>
<p>Manfaat pembentukan Tim CSIRT :</p>
<p>1) Dapat mengendalikan Dampak Seminimal Mungkin</p>
<p>2) Dapat Memulihkan Layanan Terdampak</p>
<p>3) Dapat Mencegah Insiden Selanjutnya</p>
<p>4) Dapat Mendeteksi Secepat Mungkin</p>
<p>5) Dapat Mengendalikan Insiden Setepat Mungkin</p>
<p>Gov-CSIRT Indonesia memberikan layanan asistensi pembentukan CSIRT dengan tujuan agar setiap konstituen/stakeholder dapat membangun CSIRT di instansi masing-masing. Diharapkan dengan adanya CSIRT tiap instansi pemerintah akan lebih efektif dan efisien dalam melakukan manajemen insiden keamanan siber.</p>
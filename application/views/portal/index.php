<?php  
header('X-Frame-Options: DENY');
?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>:: <?php echo $logo->title; ?> ::</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <link rel="icon" type="image/png" href="<?php echo base_url(); ?><?php echo (isset($logo->nama_file) ? $logo->path_file .$logo->nama_file : ""); ?>"/> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/css/bootstrap/bootstrap.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/fonts/fontawesome-5.0.8/css/fontawesome-all.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/vendor/animate/animate.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/vendor/css-hamburgers/hamburgers.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/vendor/animsition/css/animsition.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/css/util.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>global_assets/css/main.css">

        <!--===============================================================================================-->
    </head>
    <body class="animsition">

        <?php $this->load->view("portal/header"); ?> 

        <!-- Headline -->
        <!-- <div class="container">
                <div class="bg0 flex-wr-sb-c p-rl-20 p-tb-8">
                        <div class="f2-s-1 p-r-30 size-w-0 m-tb-6 flex-wr-s-c">
                                <span class="text-uppercase cl2 p-r-8">
                                        Info Penting :
                                </span>

                                <span class="dis-inline-block cl6 slide100-txt pos-relative size-w-0" data-in="fadeInDown" data-out="fadeOutDown">
                                        <span class="dis-inline-block slide100-txt-item animated visible-false">
                                                content here
                                        </span> 
                                </span>
                        </div> 
                </div>
        </div> -->

        <!-- Feature post -->

        <section class="bg0">
            <div class="container">
                <div class="row m-rl--1">
                    <div class="col-sm-6 col-lg-12 p-rl-1 p-b-2">
                        <div class="bg-img1 size-a-12 how1 pos-relative" style="background-image: url(assets/banner/<?php echo (isset($banner->nama_file) ? $banner->nama_file : ""); ?>);">
                            <a href="#" class="dis-block how1-child1 trans-03"></a>

                            <div class="flex-col-e-s s-full p-rl-25 p-tb-11">

                                <h3 class="how1-child2 m-t-10">
                                    <a href="#" class="f1-m-1 cl0 hov-cl10 trans-03">
                                        <?php echo (isset($banner->keterangan) ? $banner->keterangan : ""); ?>
                                    </a>
                                </h3>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </section>
        <!-- Post -->
        <section class="bg0 p-t-10">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8">
                        <div class="p-b-20"> 
                            <div class="tab01 p-b-20">
                                <div class="tab01-head how2 bocl12 flex-s-c m-r-10 m-r-0-sr991"><!-- how2-cl2  -->
                                    <!-- Brand tab -->
                                    <h3 class="f1-m-2 cl13 tab01-title">
                                        Berita <?php echo $logo->title ?>
                                    </h3>  
                                </div> 
                                <div class="tab-content p-t-35"> 
                                    <div class="tab-pane fade show active" id="tab1-1" role="tabpanel">
                                        <div class="row">
                                            <div class="col-sm-6 p-r-25 p-r-15-sr991"> 
                                                <?php
                                                $x = 0;
                                                foreach ($berita5 as $row) {
                                                    $x++;
                                                    if ($x <= 2) {
                                                        echo '<div class="flex-wr-sb-s m-b-30"> 
																<div class="size-w-0">
                                                                        <h5 class="p-b-5">
                                                                                <a href="' . base_url() . 'portal/berita/' . $row->berita_id . '" class="f1-s-18x cl3 hov-cl10 trans-03">
                                                                                        ' . $row->title . '
                                                                                </a>
                                                                        </h5>
                                                                        <span class="f1-s-11x-a3">														 
                                                                                        ' . $row->kategori . ' 
                                                                        </span>
                                                                        <p>
                                                                        ' . $row->short_desc . '
                                                                        </p>
                                                                        <span class="f1-s-11x-navy">														 
                                                                                        <a href="' . base_url() . 'portal/berita/' . $row->berita_id . '">Selengkapnya ...</a><br>
                                                                                        <hr>
                                                                        </span>
                                                                    </div>
                                                                </div>';
                                                    }
                                                }
                                                ?> 
                                            </div>
                                            <div class="col-sm-6 p-r-25 p-r-15-sr991"> 
                                                <?php
                                                $y = 0;
                                                foreach ($berita5 as $row) {
                                                    $y++;
                                                    if ($y > 2) {
                                                        echo '<div class="flex-wr-sb-s m-b-30"> 
                                                                    <div class="size-w-0">
                                                                            <h5 class="p-b-5">
                                                                                    <a href="' . base_url() . 'portal/berita/' . $row->berita_id . '" class="f1-s-18x cl3 hov-cl10 trans-03">
                                                                                            ' . $row->title . '
                                                                                    </a>
                                                                            </h5>
                                                                            <span class="f1-s-11x-a3">														 
                                                                                            ' . $row->kategori . ' 
                                                                            </span>
                                                                            <p>
                                                                            ' . $row->short_desc . '
                                                                            </p>
                                                                            <span class="f1-s-11x-navy">														 
                                                                                            <a href="' . base_url() . 'portal/berita/' . $row->berita_id . '">Selengkapnya ...</a><br>
                                                                                            <hr>
                                                                            </span>
                                                                    </div>
                                                            </div>';
                                                    }
                                                }
                                                ?> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <!-- z -->
                                <div class="p-b-20"> 
                                    <div class="tab01 p-b-20">
                                        <div class="tab01-head how2 bocl12 flex-s-c m-r-10 m-r-0-sr991"><!-- how2-cl2  -->
                                            <!-- Brand tab -->
                                            <h3 class="f1-m-2 cl19 tab01-title">
                                                Panduan Pedoman Teknis
                                            </h3>  
                                        </div> 
                                        <div class="tab-content p-t-35"> 
                                            <div class="tab-pane fade show active" id="tab1-1" role="tabpanel">
                                                <div class="rowa"> 
                                                    <table class="table datatable-basic" style="width:90%">
                                                        <thead>
                                                            <tr>
                                                                <th>Nama Panduan</th> 
                                                                <th>Ukuran File</th> 
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            foreach ($panduan as $row) {
                                                                echo '
                                                                <tr>
                                                                        <td><a onclick="hit(' . $row->id_panduan . ')" target="_blank" href="' . base_url() . $row->path_file . $row->nama_file . '" download>' . $row->nama_panduan . '</a></td> 
                                                                        <td>' . $row->size_file . ' Kb</td> 
                                                                </tr>
                                                                ';
                                                            }
                                                            ?> 
                                                        </tbody>
                                                    </table>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- x -->
                            </div>
                        </div>
                    </div>

                    <div class="col-md-10 col-lg-4">
                        <div class="p-l-10 p-rl-0-sr991 p-b-20">
                            <!--  -->
                            <?php $this->load->view("portal/side_peringatan"); ?>

                            <?php $this->load->view("portal/side_layanan"); ?>

                            <?php $this->load->view("portal/side_rfc2350"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer -->
        <?php $this->load->view("portal/footer"); ?>

        <!-- Back to top -->
        <div class="btn-back-to-top" id="myBtn">
            <span class="symbol-btn-back-to-top">
                <span class="fas fa-angle-up"></span>
            </span>
        </div>
        <script src="<?php echo base_url() ?>global_assets/vendor/jquery/jquery-3.2.1.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/vendor/animsition/js/animsition.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/vendor/bootstrap/js/popper.js"></script>
        <script src="<?php echo base_url() ?>global_assets/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>global_assets/js/main.js"></script>
    </body>
</html>
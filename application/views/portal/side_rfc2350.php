<div class="p-t-50">
    <div class="how2 flex-s-c">
        <h3 class="f1-m-2 cl13 tab01-title">
            RFC 2350 
        </h3>
    </div>

    <ul class="p-t-35">
		<?php 
			if ($dt_file != null) {
				foreach($dt_file as $row) {
					echo "<li class='how-bor3 p-rl-4'><a class='dis-block f1-s-10 text-uppercase cl2 hov-cl10 trans-03 p-tb-13' href='".base_url().$row->path_file.$row->nama_file."'>".$row->nama_rfc."</a></li>";
					}
			}
		?> 
    </ul>
</div>
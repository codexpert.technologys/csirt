<div>
    <div class="how2  flex-s-c">
        <h3 class="f1-m-2 cl12 tab01-title">
            Peringatan Keamanan
        </h3>
    </div>
    <ul class="p-t-35">
        <?php
        $no = 1;
        if ($warning != null) {
			foreach ($warning as $row) {
	            echo '<li class="flex-wr-sb-s p-b-22">
	                        <div class="size-a-8 flex-c-c borad-3 size-a-8 bg2 f1-m-4 cl0 m-b-6">
	                                ' . $no++ . '
	                        </div>
	
	                        <a href="' . base_url() . 'portal/berita/' . $row->berita_id . '" class="size-w-3 f1-s-6 cl3 hov-cl10 trans-03">
	                                ' . $row->title . '
	                        </a>
	                </li>';
	        }
		}	
        ?> 
    </ul>
</div>
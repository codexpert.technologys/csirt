					<div class="row">
						<?php 
						if ($results != null) {  
					      foreach($results as $row){ 
						  	echo '
									<div class="col-sm-3 p-r-25 p-r-15-sr991">
									<!-- Item latest -->	
									<div class="m-b-45">
										<a href="'.base_url().'portal/galeri_d/'.$row->id_galeri.'" class="wrap-pic-w hov1 trans-03">
											<img width="207" height="137" src="'.base_url().$row->path_file.$row->nama_file.'" alt="IMG" class="img-rounded img-preview">
										</a>
		
										<div class="p-t-16">
											<div class="p-b-5">
												<a href="'.base_url().'portal/galeri_d/'.$row->id_galeri.'" class="f1-s-11 cl2 hov-cl10 trans-03">
													'.$row->nama.'
												</a>
											</div>
		
											<span class="cl8">
												 
												<span class="f1-s-11x-a3">
													'.date_format(date_create($row->create_dt), "d-m-Y" ).'
												</span>
											</span>
										</div>
									</div>
								</div>
							';
						  }
						}  
						
						?>
						  
					</div>
 					<div id="pagination">
						<div class="col-md-12"> 
							<div class="text-right">
								<?php
									if(isset($links)){
										echo $links;
									 } 
									    ?>
							</div> 
						</div>
					</div>
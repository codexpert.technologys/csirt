<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('m_global');
        $this->load->model('m_user');
		$this->load->model('m_portal');
    }

    public function index($msg = NULL) {
        $dt = $this->auth->buat_captcha();
        $data["msg"] = $msg;
        $data["image"] = $dt["image"];
		$data['logo'] = $this->m_portal->get_logo();
        $this->load->view('backend/login', $data);
    }

    public function register() {
		$data['logo'] = $this->m_portal->get_logo();
        $this->load->view('backend/register', $data);
    }

    public function validate() {
		$data['logo'] = $this->m_portal->get_logo();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_error_delimiters(' <span style="color:#FF0000">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $input_captcha = $this->input->post('captcha');
            $success = $this->auth->do_login($username, $password, $input_captcha);
            if ($this->auth->is_logged_in() == false) {
                $this->index($success);
            } else {
                redirect('login/welcome');
            }
        }
    }

    public function welcome() {
        $page = "backend/v_dashboard";
        $this->data["footer"] = "backend/footer";
        $this->data["title"] = "Berita";
        $this->data["menu"] = $this->m_user->load_menu();
        $this->data['page'] = $page;
		$this->data['logo'] = $this->m_portal->get_logo();
        $this->load->view('backend/view', $this->data);
    }

    public function load_menu() {
        $this->auth->restrict();
        $role = $this->session->userdata('role');
        $this->load->model('m_user');
        $this->data['menu'] = $this->m_user->get_menu_for_role($role);
    }

    function logout() {
        if ($this->auth->is_logged_in() == true) { // jika login, destroy session
            $this->auth->do_logout();
        }
        // goto halaman utama
        redirect('login');
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
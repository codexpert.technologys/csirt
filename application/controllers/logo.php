<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logo extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("m_user");
        $this->load->model("m_logo");
        $this->load->model("m_global");
    }

    public function index() {
        $this->data["menu"] = $this->m_user->load_menu();
        $this->auth->cek_menu($this->config->item("logo"));
        $this->data["page"] = "backend/v_logo";
        $this->data["footer"] = "backend/footer";
        $this->data["title"] = "Logo";
		$this->data['logo'] = $this->m_global->get_logo();
        $this->load->view('backend/view', $this->data);
    }

    public function list_logo() {
        $dt = $this->m_global->get_data("logo");
        $this->data["recordsTotal"] = 1;
        $this->data["recordsFiltered"] = 1;
        $this->data["firstUrutan"] = 1;
        $this->data["lastUrutan"] = 1;
        $this->data["draw"] = 0;
        $this->data["data"] = $dt;
        echo json_encode($this->data);
    }

    function hapus() {
        $dt = $this->m_global->dele("logo", "id_logo", $this->input->post("id"), $this->input->post("nama_file"));
        echo json_encode($dt);
    }

    function simpan() {
        $mode = $this->input->post("mode");
        $dt = $this->m_logo->simpan($mode);
        echo json_encode($dt);
    }
 
    public function saveGambar() {
        if (isset($_FILES["file"]["name"])) {
            $date_start = date('Y-m-d');

            $url = './assets/logo/' ;
            (is_dir($url) <> '1' ? mkdir($url) : '');
             

            $config['upload_path'] = $url;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['overwrite'] = true;
            $config['create_thumb'] = true;
            $config['maintain_ratio'] = FALSE;
            $config['width'] = 600;
            $config['height'] = 400;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $dt = $this->upload->display_errors();
                echo json_encode($dt);
            } else {
                $files = $this->upload->data();
                $this->data["nama_file"] = str_replace(" ", "_", $_FILES["file"]["name"]);
                $this->data["tipe_file"] = $files['file_type'];
                $this->data["size_file"] = $files['file_size'];
                $this->data["path_file"] = $url;
				$this->data["data"] = $files;
                echo json_encode($this->data);
            }
        }
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
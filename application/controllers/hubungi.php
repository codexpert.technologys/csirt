<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Hubungi extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_user');
	  $this->load->model('m_global');
	  $this->load->model('m_hubungi');
   	}
   	
	function index() { 
		$this->data["menu"] = $this->m_user->load_menu(); 
		$this->auth->cek_menu($this->config->item("hubungi"));
		$this->data['logo'] = $this->m_global->get_logo();
		$this->data["page"] = "backend/v_hubungi"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "Hubungi Kami"; 
		$this->load->view('backend/view', $this->data); 
	}
	
	function list_hubungi()
   	{   
		$dt = $this->m_global->get_data("hubungi");    
		$this->data["recordsTotal"] = 1;
		$this->data["recordsFiltered"] = 1;
		$this->data["firstUrutan"] = 0;
		$this->data["lastUrutan"] = 0; 
		$this->data["draw"] = 0; 
		$this->data["data"] = $dt;  
		echo json_encode($this->data);		
	}
	
	function simpan() 
	{   $mode = $this->input->post("mode");
		if ($mode == "add") {
			$dt = $this->m_hubungi->insert_hubungi(); 
		} else {
			$dt = $this->m_hubungi->update_hubungi();  
		} 
		echo json_encode($dt);		
	}
	  
	
	function hapus() {
		$dt = $this->m_global->dele("hubungi", "id_hubungi", $this->input->post('id'));
		echo json_encode($dt);
	} 
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
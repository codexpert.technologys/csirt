<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Kategori extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_user');
	  $this->load->model('m_global');
	  $this->load->model('m_kategori');
   	}
   	
	function index() { 
		$this->data["menu"] = $this->m_user->load_menu(); 
		$this->auth->cek_menu($this->config->item("kategori"));
		$this->data["page"] = "backend/v_kategori"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "Kategori"; 
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view('backend/view', $this->data); 
	}
	
	function list_kategori()
   	{   
		$dt = $this->m_global->get_list_by_id("kategori", "is_aktif", "1");    
		$this->data["recordsTotal"] = 1;
		$this->data["recordsFiltered"] = 1;
		$this->data["firstUrutan"] = 0;
		$this->data["lastUrutan"] = 0; 
		$this->data["draw"] = 0; 
		$this->data["data"] = $dt;  
		echo json_encode($this->data);		
	}
	
	function simpan() 
	{   $mode = $this->input->post("mode");
		if ($mode == "add") {
			$dt = $this->m_kategori->insert_kategori(); 
		} else {
			$dt = $this->m_kategori->update_kategori();  
		} 
		echo json_encode($dt);		
	}
	  
	
	function hapus() {
		$dt = $this->m_kategori->dele_kategori();
		echo json_encode($dt);
	} 
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
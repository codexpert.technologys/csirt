<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Event extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model("m_user");
	  $this->load->model("m_event");
	  $this->load->model("m_global");
   	}
   	
	public function index()
   	{   $this->data["menu"] = $this->m_user->load_menu(); 
		$this->auth->cek_menu($this->config->item("event"));
		$this->data["page"] = "backend/v_event"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "Event";  
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view('backend/view', $this->data);
	}
	
	public function list_event()
   	{   $dt = $this->m_global->get_data("event");   
		$this->data["recordsTotal"] = 1;
		$this->data["recordsFiltered"] = 1;
		$this->data["firstUrutan"] = 1;
		$this->data["lastUrutan"] = 1; 
		$this->data["draw"] = 0; 
		$this->data["data"] = $dt; 
		echo json_encode($this->data);		
	}
	
	function simpan() {
		$dt = $this->m_event->simpan();
		echo json_encode($dt);		
		
	}
	
	function hapus() {
		$dt = $this->m_event->dele("event", "id_event", $this->input->post("id"), 'Y');
		echo json_encode($dt);
	}
	
	function hapus_file() {   
		$dt = $this->m_global->dele("materi", "id_materi", $this->input->post("id"), "Y" );
		 
		echo json_encode($dt);
	}
	
	public function upload()
    {
     if(isset($_FILES["file"]["name"]))  
     {   $file_name =  $_FILES["file"]["name"]; 
		 $arrFile = explode(".", $file_name);  
		 $arrCount = count($arrFile); 
		 $new_name = "";
		 $separator = "";
		 
		 for ($i=0; $i<$arrCount - 1; $i++) {
		 	$new_name = $new_name.$separator.$arrFile[$i];
			$separator = "_";
		 }
		 
		 $new_name = str_replace(" ", "-", $new_name);
		 $new_name_ext = $new_name.".".$arrFile[$arrCount-1]; 
		  
		 //$date_start =  date('Y');
		 
		   $url = './assets/event/';//.$date_start.'/';    //tahun
			 
			if (is_dir($url)<>'1') {
				mkdir($url);
			}
		  
          $config['upload_path'] = $url;  
          $config['allowed_types'] = 'pdf';  
		  $config['overwrite'] = true;
		  $config['file_name'] = $new_name_ext;
          $this->load->library('upload', $config);  
          if(!$this->upload->do_upload('file'))  
          {  
               $dt = $this->upload->display_errors();  
			   
               echo json_encode($dt);  
          }  
          else  
          {  	//$new_name = str_replace(" ", "_", $new_name);
                $dt = $this->upload->data();    
				$data["path_file"] = $url;
				$data["data"] = $dt;
                echo json_encode($data);  
          }   
     } 
	}
	
	/*public function upload()
    {
     if(isset($_FILES["file"]["name"]))  
     {   $file_name =  $_FILES["file"]["name"];
	 	 $filenameNoExt = explode(".", $file_name);  
	 	 $date_start =  date('Y-m-d');
		 
		   $url = './assets/event/'.$date_start.'/';    //tahun
			 
			if (is_dir($url)<>'1') {
				mkdir($url);
			}
		  $new_name = str_replace(".", "-", $filenameNoExt[0]) . substr($file_name, -4);
          $config['upload_path'] = $url;  
          $config['allowed_types'] = 'pdf';  
		  $config['overwrite'] = true;
		  $config['file_name'] = $new_name;
          $this->load->library('upload', $config);  
          if(!$this->upload->do_upload('file'))  
          {  
               $dt = $this->upload->display_errors();  
               echo json_encode($dt);  
          }  
          else  
          {  	$new_name = str_replace(" ", "_", $new_name);
                $data = $this->upload->data();                 
                echo json_encode($data);  
          }  
     } 
	}*/
	
	
	function materi()
   	{   $dt = $this->m_global->get_list_by_id("materi", "id_event", $this->input->post("id"));    
		$this->data["data"] = $dt; 
		echo json_encode($this->data);		
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
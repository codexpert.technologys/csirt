<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Panduan extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model("m_user");
	  $this->load->model("m_panduan");
	  $this->load->model("m_global");
   	}
   	
	public function index()
   	{   $this->data["menu"] = $this->m_user->load_menu(); 
		$this->auth->cek_menu($this->config->item("panduan"));
		$this->data["page"] = "backend/v_panduan"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "Dokumen";  
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view('backend/view', $this->data);
	}
	
	public function list_panduan()
   	{   $dt = $this->m_global->get_data("panduan");   
		$this->data["recordsTotal"] = 1;
		$this->data["recordsFiltered"] = 1;
		$this->data["firstUrutan"] = 1;
		$this->data["lastUrutan"] = 1; 
		$this->data["draw"] = 0; 
		$this->data["data"] = $dt; 
		echo json_encode($this->data);		
	}
	
	function hapus() {
		$dt = $this->m_global->dele("panduan", "id_panduan", $this->input->post("id"), $this->input->post("nama_file"));
		echo json_encode($dt);
	}
	
	function simpan() 
	{   $this->data["nama_panduan"] = $this->input->post("nama_panduan"); 
		$this->data["nama_file"] = $this->input->post("nama_file"); 
		$this->data["tipe_file"] = $this->input->post("tipe_file"); 
		$this->data["size_file"] = $this->input->post("size_file"); 
		$this->data["path_file"] = $this->input->post("path_file"); 
		$this->data["status"] = $this->input->post("status"); 
		$this->data["mode"] = $this->input->post("mode"); 
		$dt = $this->m_panduan->simpan($this->data);  
		echo json_encode($dt);		
	}
	 
	
	/*function do_upload($filename)
    {   $filenameNoExt = explode(".", $filename);  
		$url = './assets/panduan/';    //path 
		 
		if (is_dir($url)<>'1') {
			mkdir($url);
		}
		 
        if($_FILES['fileToUpload']['error']==4)  //if No file was uploaded.
            return false;        
        $config['upload_path'] = $url;
		$config['allowed_types'] = 'pdf';
		$config['max_size']        = 0; //0 for no limit; 4096;//5MB size allowed 
		$config["overwrite"] = true;
		$this->load->library('upload');
		$this->upload->initialize($config);

        if( $this->upload->do_upload('fileToUpload') )
        {    
            $files = $this->upload->data(); 
			
			$this->data["nama_panduan"] = $this->input->post("nama_panduan");
			$this->data["nama_file"] = str_replace(" ", "_", $filename);  
			$this->data["tipe_file"] = $files['file_type'];
			$this->data["size_file"] = $files['file_size'];
			$this->data["path_file"] = $url;
			$this->data["status"] = $this->input->post("status"); 
			$this->data["mode"] = $this->input->post("mode"); 
			  
			$this->m_panduan->simpan($this->data);  
			$this->data["title"] = "Panduan Teknis";  
			$this->data["menu"] = $this->m_user->load_menu(); 
			$this->data["page"] = "backend/v_panduan"; 
			$this->data["footer"] = "backend/footer"; 
			$this->load->view('backend/view', $this->data);
        } else {
			$msg = str_replace('<p>','',str_replace('</p>','',$this->upload->display_errors())); 
			echo $msg; 
		}
    }*/
	
	
	public function upload()
    {
     if(isset($_FILES["file"]["name"]))  
     {   $file_name =  $_FILES["file"]["name"]; 
		 $arrFile = explode(".", $file_name);  
		 $arrCount = count($arrFile); 
		 $new_name = "";
		 $separator = "";
		 for ($i=0; $i<$arrCount - 1; $i++) {
		 	$new_name = $new_name.$separator.$arrFile[$i];
			$separator = "_";
		 }
		 
		 $new_name = str_replace(" ", "-", $new_name);
		 $new_name_ext = $new_name.".".$arrFile[$arrCount-1]; 
		  
		 $url = './assets/panduan/';
			 
			if (is_dir($url)<>'1') {
				mkdir($url);
			}
		  
          $config['upload_path'] = $url;  
          $config['allowed_types'] = 'pdf';  
		  $config['overwrite'] = true;
		  $config['file_name'] = $new_name_ext;
          $this->load->library('upload', $config);  
          if(!$this->upload->do_upload('file'))  
          {  
               $dt = $this->upload->display_errors();  
			   
               echo json_encode($dt);  
          }  
          else  
          {  	//$new_name = str_replace(" ", "_", $new_name);
                $dt = $this->upload->data();    
				$data["path_file"] = $url;
				$data["data"] = $dt;
                echo json_encode($data);  
          }   
     } 
	}
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
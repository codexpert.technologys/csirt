<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Dashboard extends CI_Controller {

	public function __construct()
   	{
      parent::__construct();
	  $this->load->model('m_dashboard');
	  $this->load->model('m_global'); 
   	}
   	
	public function index()
   	{    
		$page = "backend/v_dashboard";
		$this->load_menu(); 
		$this->data['page'] = $page; 
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view('backend/view', $this->data);
	}
	
	public function load_menu() {
		$this->auth->restrict();
		$role = $this->session->userdata('role');
		$this->data['panduan'] = $this->m_global->get_list_by_id("panduan", "status", "1"); 
		$this->load->model('m_user');
		$this->data['menu'] = $this->m_user->get_menu_for_role($role);
	}  
	
	function get_hit_stat() {
		$dt[] = $this->m_dashboard->get_tanggal7();
		$dt[] = $this->m_dashboard->get_hit_page();
		$dt[] = $this->m_dashboard->get_hit_value();
		$dt[] = $this->m_dashboard->get_hit_month();
		$dt[] = $this->m_dashboard->get_hit_panduan();
		echo json_encode($dt);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
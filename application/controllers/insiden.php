<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Insiden extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_global');
        $this->load->model('m_insiden');
    }

    function index() {
        $this->data["menu"] = $this->m_user->load_menu();
        $this->auth->cek_menu($this->config->item("insiden"));
        $this->data["page"] = "backend/v_insiden";
        $this->data["footer"] = "backend/footer";
        $this->data["title"] = "Laporan Insiden";
        $this->data["insiden"] = $this->m_global->get_data("ref_insiden");
        $this->data["serangan"] = $this->m_global->get_list_by_id("ref_insiden", "serangan", "1");
		$this->data['logo'] = $this->m_global->get_logo();
        $this->load->view('backend/view', $this->data);
    }

    function list_insiden() {
		if ($this->session->userdata("role") != 2) {
        	$dt = $this->m_global->get_data("insiden");
		} else {
			$dt = $this->m_global->get_list_by_id("insiden", "create_by", $this->session->userdata("username") ); 
		}
        $this->data["recordsTotal"] = 1;
        $this->data["recordsFiltered"] = 1;
        $this->data["firstUrutan"] = 0;
        $this->data["lastUrutan"] = 0;
        $this->data["draw"] = 0;
        $this->data["data"] = $dt;
        echo json_encode($this->data);
    }

    function simpan() {
        $mode = $this->input->post("mode");
        $dt = $this->m_insiden->simpan($mode);
        echo json_encode($dt);
    }

    function hapus() {
        $dt = $this->m_global->dele("insiden", "id_insiden", $this->input->post('id'));
        echo json_encode($dt);
    }

	public function upload()
    {
     if(isset($_FILES["file"]["name"]))  
     {   $file_name =  $_FILES["file"]["name"]; 
		 $arrFile = explode(".", $file_name);  
		 $arrCount = count($arrFile); 
		 $new_name = "";
		 $separator = "";
		 for ($i=0; $i<$arrCount - 1; $i++) {
		 	$new_name = $new_name.$separator.$arrFile[$i];
			$separator = "_";
		 }
		 
		 $new_name = str_replace(" ", "-", $new_name);
		 $new_name_ext = $new_name.".".$arrFile[$arrCount-1]; 
		  
		 $date_start = date('Y-m');

            $url = './assets/insiden/' . $date_start . '/';    //tahun
			 
			if (is_dir($url)<>'1') {
				mkdir($url);
			}
		  
          $config['upload_path'] = $url;  
          $config['allowed_types'] = 'pdf';  
		  $config['overwrite'] = true;
		  $config['file_name'] = $new_name_ext;
          $this->load->library('upload', $config);  
          if(!$this->upload->do_upload('file'))  
          {  
               $dt = $this->upload->display_errors();  
			   
               echo json_encode($dt);  
          }  
          else  
          {  	//$new_name = str_replace(" ", "_", $new_name);
                $dt = $this->upload->data();    
				$data["path_file"] = $url;
				$data["data"] = $dt;
                echo json_encode($data);  
          }   
     } 
	}
	
    function send() {
		
        $dt = $this->m_global->get_record_by_id("insiden", "id_insiden", $this->input->post("id"));
        $ref_i = $this->m_global->get_data("ref_insiden");
        $ref_s = $this->m_global->get_list_by_id("ref_insiden", "serangan", "1");
        $str_ins = "";
        $str_ser = "";
        $arr_ti = explode(",", $dt->tipe_insiden);
        $arr_ts = explode(",", $dt->tipe_serangan);
        foreach ($ref_i as $row) {
            $cek = (strlen(array_search($row->id, $arr_ti)) >= 1 ? "checked='checked'" : "");
            $str_ins = $str_ins . '<div class="radio">
                              <label> 
                                  <input type="checkbox" value="' . $row->id . '" name="tipe_insiden[]" ' . $cek . '> ' . $row->deskripsi . ' 
                              </label>
                          </div>';
        }

        foreach ($ref_s as $row) {
            $cek = (strlen(array_search($row->id, $arr_ts)) >= 1 ? "checked='checked'" : "");
            $str_ser = $str_ser . '<div class="radio">
                              <label> 
                                  <input type="checkbox" value="' . $row->id . '" name="tipe_insiden[]" ' . $cek . '> ' . $row->deskripsi . ' 
                              </label>
                          </div>';
        }

        $str = "";
        //foreach ($dt as $row) {
        $str = '
			<table border="1" width="100%">
			<tr>
			    <td colspan="4" bgcolor="khaki"><h4>A. INFORMASI UMUM</h4></td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">1. IDENTITAS PELAPOR</td> 
			</tr>
			<tr>
			    <td width="25%">Nama Lengkap</td>
			    <td width="25%"> ' . $dt->nama_lengkap . '</td>
				<td width="25%">Email</td>
			    <td width="25%"> ' . $dt->email . '</td>
			</tr>
			<tr>
			    <td>Telp Organisasi</td>
			    <td> ' . $dt->telp_organisasi . '</td>
				<td>Handphone</td>
			    <td> ' . $dt->handphone . '</td>
			</tr>
			<tr>
			    <td>Organisasi</td>
			    <td> ' . $dt->organisasi . '</td>
				<td></td>
			    <td></td>
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">2. TIPE LAPORAN </td> 
			</tr>
			<tr>
			    <td >Tipe</td> 
				<td ><label class="radio-inline">
                         <input type="radio"  name="tipe_laporan" value="AW" ' . ($dt->tipe_laporan == "AW" ? "checked='checked'" : "") . '>
                              Awal
                          </label>
                          <label class="radio-inline">
                              <input type="radio"  name="tipe_laporan" value="TL" ' . ($dt->tipe_laporan == "TL" ? "checked='checked'" : "") . '>
                              Tindak Lanjut
                          </label>
                          <label class="radio-inline">
                              <input type="radio"  name="tipe_laporan" value="AK" ' . ($dt->tipe_laporan == "AK" ? "checked='checked'" : "") . '>
                              Akhir
                          </label></td> 
				<td colspan="2"></td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">3. WAKTU TERJADINYA INSIDEN</td> 
			</tr>
			<tr>
			    <td>Tanggal</td> 
				<td> ' . $dt->tgl_insiden . '</td>  
				<td colspan="2"></td> 
			</tr>
			<tr>
			    <td>Jam</td> 
				<td> ' . $dt->jam_insiden . '</td>  
				<td colspan="2"></td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">4. TIPE INSIDEN</td> 
			</tr>
			<tr>
			    <td colspan="4">' . $str_ins . '</td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">5. DESKRIPSI INSIDEN</td> 
			</tr>
			<tr>
			    <td>Deskripsi </td>
				<td colspan="3"> ' . $dt->deskripsi_insiden . '</td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">6. DAMPAK INSIDEN</td> 
			</tr>
			<tr>
			    <td>Dampak</td>
				<td colspan="3"> 
						<label class="radio-inline">
                             <input type="radio" name="dampak_insiden" value="P"' . ($dt->dampak_insiden == "P" ? "checked='checked'" : "") . '>
                             Jaringan Publik
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="dampak_insiden" value="I"' . ($dt->dampak_insiden == "I" ? "checked='checked'" : "") . '>
                             Jaringan Internal
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="dampak_insiden" value="L"' . ($dt->dampak_insiden == "L" ? "checked='checked'" : "") . '>
                             Lainnya
                         </label>
				</td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">7. TINDAKAN PENANGGULANGAN INSIDEN</td> 
			</tr>
			<tr>
				<td>a. Respon Cepat/Awal(Jangka Pendek)</td>
				<td colspan="3"> ' . $dt->respon_pdk . '</td> 
			</tr>
			<tr>
				<td>b. Jangka Panjang</td>
				<td colspan="3"> ' . $dt->respon_pjg . '</td> 
			</tr>
			<tr>
				<td colspan="1">c. Apakah Rencana BackUp System sukses diimplementasikan?</td>
				<td colspan="3"> ' . $dt->respon_bak . '</td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">8. APAKAH ORGANISASI LAIN DILAPORKAN? JIKA IYA, SEBUTKAN</td> 
			</tr>
			<tr>
			    <td colspan="4">' . $dt->laporan_lain . '</td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="khaki"><H4>B. INFORMASI KHUSUS</h4></td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">9.  ASET KRITIS YANG TERKENA DAMPAK</td> 
			</tr>
			<tr>
			    <td colspan="4">' . $dt->aset_kritis . '</td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">10. DAMPAK INSIDEN TERHADAP ASET</td> 
			</tr>
			<tr>
			    <td colspan="4">' . $dt->dampak_aset . '</td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">11. JUMLAH PENGGUNA YANG TERKENA DAMPAK</td> 
			</tr>
			<tr>
			    <td colspan="4">' . $dt->jumlah_dampak . '</td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">12. DAMPAK TERHADAP ICT </td> 
			</tr>
			<tr>
			    <td colspan="4">' . $dt->dampak_ict . '</td> 
			</tr>
			<tr>
			    <td colspan="4" bgcolor="LemonChiffon">13. PROFIL PENYERANG</td> 
			</tr>
			<tr>
				<td>IP Penyerang</td>
			    <td colspan="3"> ' . $dt->ip_penyerang . '</td> 
			</tr>
			<tr>
				<td>Port yang Diserang</td>
			    <td colspan="3"> ' . $dt->port_diserang . '</td> 
			</tr>
			<tr>
				<td colspan="4" bgcolor="LemonChiffon">14. TIPE SERANGAN</td> 
			</tr>
			<tr> 
			    <td colspan="4">' . $str_ser . '</td> 
			</tr>
			<tr>
				<td colspan="4" bgcolor="LemonChiffon">15. ANALISIS</td> 
			</tr>
			<tr> 
			    <td colspan="1">a. Laporan Analisis Log</td> 
				<td colspan="3"> <label class="radio-inline">
                                      <input type="radio" name="lap_analisis" value="A" ' . ($dt->lap_analisis == "A" ? "checked='checked'" : "") . '>
                                      Ada
                                  </label>
                                  <label class="radio-inline">
                                      <input type="radio" name="lap_analisis" value="T" ' . ($dt->lap_analisis == "T" ? "checked='checked'" : "") . '>
                                      Tidak Ada
                                  </label>
                                  <label class="radio-inline">
                                      <input type="radio" name="lap_analisis" value="P" ' . ($dt->lap_analisis == "P" ? "checked='checked'" : "") . '>
                                      Sedang Proses
                                  </label>
				</td> 
			</tr>
			<tr> 
			    <td colspan="1">b. Laporan Forensik</td> 
				<td colspan="3"> <label class="radio-inline">
                                      <input type="radio" name="lap_forensik" value="A" ' . ($dt->lap_forensik == "A" ? "checked='checked'" : "") . '>
                                      Ada
                                  </label>
                                  <label class="radio-inline">
                                      <input type="radio" name="lap_forensik" value="T" ' . ($dt->lap_forensik == "T" ? "checked='checked'" : "") . '>
                                      Tidak Ada
                                  </label>
                                  <label class="radio-inline">
                                      <input type="radio" name="lap_forensik" value="P" ' . ($dt->lap_forensik == "P" ? "checked='checked'" : "") . '>
                                      Sedang Proses
                                  </label>
				</td> 
			</tr>
			<tr> 
			    <td colspan="1">c. Laporan Audit</td> 
				<td colspan="3"> <label class="radio-inline">
                                      <input type="radio" name="lap_audit" value="A" ' . ($dt->lap_audit == "A" ? "checked='checked'" : "") . '>
                                      Ada
                                  </label>
                                  <label class="radio-inline">
                                      <input type="radio" name="lap_audit" value="T" ' . ($dt->lap_audit == "T" ? "checked='checked'" : "") . '>
                                      Tidak Ada
                                  </label>
                                  <label class="radio-inline">
                                      <input type="radio" name="lap_audit" value="P" ' . ($dt->lap_audit == "P" ? "checked='checked'" : "") . '>
                                      Sedang Proses
                                  </label>
				</td> 
			</tr>
			<tr> 
			    <td colspan="1">d. Laporan Lalu Lintas Jaringan </td> 
				<td colspan="3"> <label class="radio-inline">
                                      <input type="radio" name="lap_traffic" value="A" ' . ($dt->lap_traffic == "A" ? "checked='checked'" : "") . '>
                                      Ada
                                  </label>
                                  <label class="radio-inline">
                                      <input type="radio" name="lap_traffic" value="T" ' . ($dt->lap_traffic == "T" ? "checked='checked'" : "") . '>
                                      Tidak Ada
                                  </label>
                                  <label class="radio-inline">
                                      <input type="radio" name="lap_traffic" value="P" ' . ($dt->lap_traffic == "P" ? "checked='checked'" : "") . '>
                                      Sedang Proses
                                  </label>
				</td> 
			</tr>
			<tr> 
			    <td colspan="4" bgcolor="LemonChiffon">16. RINCIAN</td>  
			</tr>
			<tr> 
			    <td colspan="1">a. Nama dan Versi Perangkat</td>  
				<td colspan="3">' . $dt->nama_perangkat . '</td>  
			</tr>
			<tr> 
			    <td colspan="1">b. Lokasi Perangkat</td>  
				<td colspan="3">' . $dt->lokasi_perangkat . '</td>  
			</tr>
			<tr> 
			    <td colspan="1">c. Sistem Operasi </td>  
				<td colspan="3">' . $dt->sistem_operasi . '</td>  
			</tr>
			<tr> 
			    <td colspan="1">d. Terakhir Update Firmware</td>  
				<td colspan="3">' . $dt->last_update_os . '</td>  
			</tr>
			<tr> 
			    <td colspan="1">e. IP Address </td>  
				<td colspan="3">' . $dt->ip_address . '</td>  
			</tr>
			<tr> 
			    <td colspan="1">f. Mac Address</td>  
				<td colspan="3">' . $dt->mac_address . '</td>  
			</tr>
			<tr> 
			    <td colspan="1">g. DNS Entry</td>  
				<td colspan="3">' . $dt->dns_entry . '</td>  
			</tr>
			<tr> 
			    <td colspan="1">h. Domain/Workgroup</td>  
				<td colspan="3">' . $dt->domain . '</td>  
			</tr>
			<tr> 
			    <td colspan="1">i. Perangkat terhubung kejaringan ?</td>  
				<td colspan="3">
						<label class="radio-inline">
                           <input type="radio" name="konek_jaringan" value="Y" ' . ($dt->konek_jaringan == "Y" ? "checked='checked'" : "") . '>
                           Ya
                       </label>
                       <label class="radio-inline">
                           <input type="radio" name="konek_jaringan" value="T" ' . ($dt->konek_jaringan == "T" ? "checked='checked'" : "") . '>
                           Tidak
                       </label>
				</td>  
			</tr>
			<tr> 
			    <td colspan="1">j. Perangkat terhubung ke modem ?</td>  
				<td colspan="3">
						<label class="radio-inline">
                           <input type="radio" name="konek_modem" value="Y" ' . ($dt->konek_modem == "Y" ? "checked='checked'" : "") . '>
                           Ya
                       </label>
                       <label class="radio-inline">
                           <input type="radio" name="konek_modem" value="T" ' . ($dt->konek_modem == "T" ? "checked='checked'" : "") . '>
                           Tidak
                       </label>
				</td>  
			</tr>
			<tr> 
			    <td colspan="1">k. Adakah pengamanan fisik terhadap perangkat?</td>  
				<td colspan="3">
						<label class="radio-inline">
                           <input type="radio" name="pengamanan_fisik" value="Y" ' . ($dt->pengamanan_fisik == "Y" ? "checked='checked'" : "") . '>
                           Ya
                       </label>
                       <label class="radio-inline">
                           <input type="radio" name="pengamanan_fisik" value="T" ' . ($dt->pengamanan_fisik == "T" ? "checked='checked'" : "") . '>
                           Tidak
                       </label>
				</td>  
			</tr>
			<tr> 
			    <td colspan="1">l. Adakah pengamanan logik terhadap perangkat?</td>  
				<td colspan="3">
						<label class="radio-inline">
                           <input type="radio" name="pengamanan_logik" value="Y" ' . ($dt->pengamanan_logik == "Y" ? "checked='checked'" : "") . '>
                           Ya
                       </label>
                       <label class="radio-inline">
                           <input type="radio" name="pengamanan_logik" value="T" ' . ($dt->pengamanan_logik == "T" ? "checked='checked'" : "") . '>
                           Tidak
                       </label>
				</td>  
			</tr>
			<tr> 
			    <td colspan="1">m. Apakah perangkat sudah diputus dari jaringan?</td>  
				<td colspan="3">
						<label class="radio-inline">
                           <input type="radio" name="perangkat_putus" value="Y" ' . ($dt->perangkat_putus == "Y" ? "checked='checked'" : "") . '>
                           Ya
                       </label>
                       <label class="radio-inline">
                           <input type="radio" name="perangkat_putus" value="T" ' . ($dt->perangkat_putus == "T" ? "checked='checked'" : "") . '>
                           Tidak
                       </label>
				</td>  
			</tr>
			<tr> 
			    <td colspan="4" bgcolor="LemonChiffon">16. STATUS INSIDEN</td>    
			</tr>
			<tr> 
			    <td colspan="4">' . $dt->status_insiden . '</td>    
			</tr>
			<tr> 
			    <td colspan="4" bgcolor="LemonChiffon">17. APAKAH SUDAH PERNAH DITAWARKAN SISTEM MANAJEMEN KRISIS?</td>    
			</tr>
			<tr> 
			    <td colspan="4">' . $dt->manajemen_krisis . '</td>    
			</tr>
			
			</table>
			
			
			';
        //}
		
		// get setting email from-to
		$d = $this->m_global->get_data("setting_email"); 
		if ($d != null) {
			foreach ($d as $row) {
				$from_email = $row->from_email;
				$from_name = $row->from_name;
				$to_email = $row->to_email;
			}  
		
	        $this->load->library('email');
	        //$config['protocol'] = 'sendmail';
	        //$config['mailpath'] = '/usr/sbin/sendmail';
	        $config['charset'] = 'iso-8859-1';
	        $config['wordwrap'] = TRUE;
	        $config['mailtype'] = "html";
	
	        $this->email->initialize($config);
	
	        //$this->email->from("hr@candiargojoyo.co.id", "System CSIRT");
			$this->email->from($from_email, $from_name);
	        $this->email->subject("Aduan Insiden (" . $dt->nama_lengkap . ")");
	        $this->email->to($to_email);
	        if ($dt->nama_file != "") {
	            if (file_exists($dt->path_file . $dt->nama_file)) {
	                $this->email->attach($dt->path_file . $dt->nama_file);
	            }
	        }
	        $this->email->message($str);
	        if ($this->email->send()) {
	            //echo 'Your Email has successfully been sent.';
	            $data = array(
	                'status' => 'S'
	            );
	            $this->db->where('id_insiden', $this->input->post('id'));
	            $this->db->update("insiden", $data);
	            $dbRet = array(
	                'errMsg' => "Data Terkirim",
	                'errNum' => $this->db->_error_number()
	            );
	            echo json_encode($dbRet);
	        } else {
	            show_error($this->email->print_debugger());
	        }
		} else {
			$dbRet = array(
	                'errMsg' => "Email pengirim dan tujuan belum di setting<br>Hubungi Administrator",
	                'errNum' => 99
	            );
			 echo json_encode($dbRet);	
		}	
    }
    
    function get_insiden() {
        $dt1 = $this->m_insiden->get_tipe_insiden();
        $dt2 = $this->m_insiden->get_tipe_serangan();
        $this->data["di"] = $dt1;
        $this->data["ds"] = $dt2;
        echo json_encode($this->data);
        
    }
	
	
	//tambahan pdf
	function get_data() {
		define('FPDF_FONTPATH', $this->config->item('fonts_path'));
		//$this->load_menu();
		//$modul = "laporan";
        //$table = 'video_file';
		//$judul = 'Video';
		//$page = ;
		//$rangetgl = 'Tanggal Input ' ; 
		//$this->auth->cek_menu($this->config->item($table));
		$this->load->library('fpdf'); 
		$id = $this->input->post("id");
		$id = 7;
		$this->data['hasil'] = $this->m_global->get_record_by_id('insiden', 'id_insiden', $id); 
		//$this->data['title'] = 'Laporan ' . $judul;
		//$this->data['range'] =  $rangetgl . ':'. date('d-m-Y',strtotime($this->input->post('tgl1'))) .' s/d '.date('d-m-Y',strtotime($this->input->post('tgl2')));
		//$this->data['image'] = 'laporan.jpg';
		$this->load->view('backend/v_insiden_pdf', $this->data);
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Berita extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_berita');
	  $this->load->model('m_global');
	  $this->load->model('m_user'); 
   	}
  
  public function index()
   	{   $this->data["menu"] = $this->m_user->load_menu(); 
		$this->auth->cek_menu($this->config->item("berita"));
		$this->data["page"] = "backend/v_berita"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "Berita";  
		$this->data["kategori"] = $this->m_global->get_list_by_id("kategori", "is_aktif", "1");    
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view('backend/view', $this->data);
	}
	
	public function list_berita()
   	{   $dt = $this->m_global->get_data_order("berita", "berita_id");   
		$this->data["recordsTotal"] = 1;
		$this->data["recordsFiltered"] = 1;
		$this->data["firstUrutan"] = 1;
		$this->data["lastUrutan"] = 1; 
		$this->data["draw"] = 0; 
		$this->data["data"] = $dt; 
		echo json_encode($this->data);		
	}
	
	function hapus_berita() {
		$dt = $this->m_global->dele("berita", "berita_id", $this->input->post("id"), "adafile");
		echo json_encode($dt);
	}
	
	public function saveGambar()
    {
         if(isset($_FILES["file"]["name"]))  
     {     $date_start =  date('Y-m-d');
		 
		   $url = 'assets/images/berita/'.substr($date_start,0,4).'/';    //tahun
			 
			if (is_dir($url)<>'1') {
				mkdir($url);
			}
			
			$url = $url.substr($date_start,5,2).'/';  //bulan
			
			if (is_dir($url)<>'1') {
				mkdir($url);
			} 
		 
		  $new_name = str_replace(" ", "-", $_FILES["file"]["name"]);
		  
          $config['upload_path'] = $url;  
          $config['allowed_types'] = 'jpg|jpeg|png|gif';  
		  $config['overwrite'] = true;
		  $config['max_size']        = 0;
		  $config['file_name'] = $new_name;
          $this->load->library('upload', $config);  
          if(!$this->upload->do_upload('file'))  
          {  
               $this->upload->display_errors();  
               return FALSE;
          }  
          else  
          {  
               $data = $this->upload->data();                 
                echo json_encode(base_url().$url.$new_name);  
          }  
     } 
  }
	
	public function saveVideo()
    {  
      if(isset($_FILES["file"]["name"]))  
     {     $date_start =  date('Y-m-d');
		   $url = 'assets/images/berita/'.substr($date_start,0,4).'/';    //tahun
			 
			if (is_dir($url)<>'1') {
				mkdir($url);
			}
			
			$url = $url.substr($date_start,5,2).'/';  //bulan
			
			if (is_dir($url)<>'1') {
				mkdir($url);
			}  
	
		  $new_name = str_replace(" ", "-", $_FILES["file"]["name"]);
		  $config['upload_path'] = $url;
		  $config['overwrite'] = true;
		  $config['file_name'] = $new_name;
          $config['allowed_types'] = 'mpg|mpeg|mkv|mp4';  
          $this->load->library('upload', $config);  
          if(!$this->upload->do_upload('file'))  
          {  
               $this->data["errNum"]= 99;  
			   $this->data["errMsg"]= $this->upload->display_errors();   
               //return FALSE;
			   

          }  
          else  
          {  
               $data = $this->upload->data();  
			   $this->data["errNum"]= 0; 
			   $this->data["fileName"] = $new_name;; 
			   
          }  
		  echo json_encode($this->data);
     } 
	 
  }
   
	public function data_berita() {
	
		
	    $filename = $this->input->post("nama_file");
	 	$date_start =  date('Y-m-d');
		$url = './assets/images/berita/'.substr($date_start,0,4).'/';    //tahun
		$url = $url.substr($date_start,5,2).'/';  //bulan
	    	
	    $this->load->library('form_validation');
	    $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[3]');          
	    //$this->form_validation->set_rules('editordata', 'Tex', 'required|min_length[10]');    
	    //$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
	    
	    if($this->form_validation->run()) { 
	        $mode = $this->input->post("mode");
			$data = $this->m_berita->simpan($mode, $filename, $url);
	    } else {
	      foreach ($_POST as $key => $value) {
	        $data['messages'][$key] = form_error($key);
	      }     

    	}
    	echo json_encode($data);
	}
	 
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct()
   	{
      parent::__construct();  
	  $this->load->model('m_global');
   	}
	
	public function index()
	{
		$this->data['page'] = 'dashboard';  
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view("view", $this->data);
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
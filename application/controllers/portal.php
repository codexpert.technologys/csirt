<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Portal extends CI_Controller {

	function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_global');
	  $this->load->model('m_portal');
	  
   	}
   	
	function index()
   	{    
		$this->data['banner'] = $this->m_portal->get_banner();
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['warning'] = $this->m_portal->list_peringatan_keamanan();
		//$this->data['berita1'] = $this->m_portal->list_berita_1();
		$this->data['panduan'] = $this->m_global->get_list_by_id("panduan", "status", "1"); 
		$this->data['berita5'] = $this->m_portal->list_berita_5();
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->hit_stat();
		$this->load->view('portal/index', $this->data );
	}
	
	function berita()
   	{   $id = $this->uri->segment(3); 
	    $this->data['warning'] = $this->m_portal->list_peringatan_keamanan();
		$this->data['dt'] = $this->m_global->get_record_join("berita", "kategori", "kategori_id", "berita_id", $id);
		$this->hit_stat();
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['arsip'] = $this->m_portal->list_arsip();
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('portal/view', $this->data);
	}
	
	function profil() {
	    $this->data['warning'] = $this->m_portal->list_peringatan_keamanan(); 
		$this->data['title'] = "Profil";
		$this->data['page'] = "portal/profil";
		$dt = $this->m_global->get_record_by_id("portal", "portal_id", "profil");   
		$this->data["long_desc"] = ($dt != null ? $dt->long_desc : '');
		$this->hit_stat();
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('portal/views', $this->data);
	}
	
	function layanan() {
	    $this->data['warning'] = $this->m_portal->list_peringatan_keamanan(); 
		$this->data['title'] = "Layanan";
		$this->data['page'] = "portal/layanan";
		$dt = $this->m_global->get_record_by_id("portal", "portal_id", "layanan");   
		$this->data["long_desc"] = ($dt != null ? $dt->long_desc : '');
		$this->hit_stat();
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('portal/views', $this->data);
	}
	
	function asistensi() {
	    $this->data['warning'] = $this->m_portal->list_peringatan_keamanan(); 
		$this->data['title'] = "Asistensi Pembentukan CSIRT";
		$this->data['page'] = "portal/asistensi";
		$this->hit_stat();
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('portal/views', $this->data);
	}
	
	function bimbingan() {
	    $this->data['warning'] = $this->m_portal->list_peringatan_keamanan(); 
		$this->data['title'] = "Bimbingan Teknis";
		$this->data['page'] = "portal/bimbingan";
		$this->hit_stat();
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('portal/views', $this->data);
	}
	
	function cyber() {
	    $this->data['warning'] = $this->m_portal->list_peringatan_keamanan(); 
		$this->data['title'] = "Cyber Security Drill Test";
		$this->data['page'] = "portal/cyber";
		$this->hit_stat();
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('portal/views', $this->data);
	}
	
	function panduan() {
	    $this->data['warning'] = $this->m_portal->list_peringatan_keamanan(); 
		$this->data['panduan'] = $this->m_global->get_list_by_id("panduan", "status", "1"); 
		$this->data['title'] = "Panduan / Pedoman";
		$this->data['page'] = "portal/panduan";
		$this->hit_stat();
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('portal/views', $this->data);
	}
	
	function aduan() {
	    $this->data['warning'] = $this->m_portal->list_peringatan_keamanan(); 
		$this->data['title'] = "Aduan Siber";
		$this->data['page'] = "portal/aduan";
		$dt = $this->m_global->get_record_by_id("portal", "portal_id", "aduan");   
		$this->data["long_desc"] = ($dt != null ? $dt->long_desc : '');
		$this->hit_stat();
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('portal/views', $this->data);
	}
	
	function rfc2350() {
	    $this->data['warning'] = $this->m_portal->list_peringatan_keamanan(); 
		$this->data['title'] = "RFC 2350";
		$this->data['page'] = "portal/rfc2350";
		$dt = $this->m_global->get_record_by_id("portal", "portal_id", "rfc2350");   
		$this->data["long_desc"] = ($dt != null ? $dt->long_desc : '');
		$this->hit_stat();
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('portal/views', $this->data);
	}
	
	function event() {
	    $this->data['warning'] = $this->m_portal->list_peringatan_keamanan(); 
		$this->data['event'] = $this->m_portal->list_event(); 
		$this->data['title'] = "Event";
		$this->data['page'] = "portal/event";
		$this->hit_stat();
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('portal/viewsev', $this->data);
	}
	
	function kontak() {
	    $this->data['warning'] = $this->m_portal->list_peringatan_keamanan();  
		$dt  = $this->m_global->get_data("hubungi");
		$this->data['row'] = $dt;
		$this->data['title'] = "Hubungi Kami";
		$this->data['page'] = "portal/kontak";
		$this->hit_stat();
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('portal/viewsev', $this->data);
	}
	
	function galeri_d() { 
		$id = $this->security->xss_clean($this->uri->segment(3));
		$dt  = $this->m_global->get_record_by_id("galeri", "id_galeri", $id);
		$this->data['row'] = $dt;
		$this->data['title'] = "Galeri";
		$this->data['page'] = "portal/galeri_d";
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		//$this->hit_stat();
		$this->load->view('portal/viewsev', $this->data);
		//$this->load->view('portal/galeri_lib', $this->data);
	} 
	
	public function galeri() { 
        //set records per page
		$this->load->library('pagination');
		$this->data['warning'] = $this->m_portal->list_peringatan_keamanan(); 
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
        $limit_page = 10; 
        $page = ($this->security->xss_clean($this->uri->segment(3))) ? ($this->security->xss_clean($this->uri->segment(3)) - 1) : 0;
        $total = $this->m_portal->get_total_galeri(); 
        $this->data['results'] = $this->m_portal->get_current_page_galeri($limit_page, $page * $limit_page);
        if ($total > 0) {
            
            // get current page records
            

            $config['base_url'] = base_url() . 'portal/galeri/' . $this->security->xss_clean($this->uri->segment(4));
            $config['total_rows'] = $total;
            $config['per_page'] = $limit_page;
            $config['uri_segment'] = 3;

            //paging configuration
            $config['num_links'] = 5;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;

            //bootstrap pagination 
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = '&laquo; First';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = 'Last &raquo';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = false;
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = false;
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#" class="flex-c-c pagi-item hov-btn1 trans-03 m-all-7 pagi-active">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
			$config['anchor_class'] = 'class="flex-c-c pagi-item hov-btn1 trans-03 m-all-7 "';

            $this->pagination->initialize($config);

            // build paging links
            $this->data['links'] = $this->pagination->create_links();
        }

        //$this->load->view('member_table', $params);
        $this->data["page_ke"] = $page;
        $this->data["limit_page"] = $limit_page;
		$this->data['title'] = "Galeri";
		$this->data['page'] = "portal/galeri"; 
		$this->load->view('portal/viewsev', $this->data);  
    }
	
	function arsip()
   	{   $id = $this->security->xss_clean($this->uri->segment(4));
	    $year = $this->security->xss_clean($this->uri->segment(3));
		$this->load->library('pagination');
		$this->data['warning'] = $this->m_portal->list_peringatan_keamanan(); 
		$this->data['logo'] = $this->m_portal->get_logo();
		$this->data['hubungi'] = $this->m_global->get_record_by_id("hubungi", "id_hubungi", "0"); 
        $limit_page = 10; 
        $page = ($id) ? ($id - 1) : 0;
        $total = $this->m_portal->get_total_berita($year); 
        $this->data['results'] = $this->m_portal->get_current_page_berita($limit_page, $page * $limit_page, $year);
		$this->data['arsip'] = $this->m_portal->list_arsip();
        if ($total > 0) { 
            // get current page records
            

            $config['base_url'] = base_url() . 'portal/arsip/' . $year;
            $config['total_rows'] = $total;
            $config['per_page'] = $limit_page;
            $config['uri_segment'] = 4;

            //paging configuration
            $config['num_links'] = 10;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;

            //bootstrap pagination 
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = '&laquo;';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = '&raquo';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = false;
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = false;
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#" class="flex-c-c pagi-item hov-btn1 trans-03 m-all-7 pagi-active">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
			$config['anchor_class'] = 'class="flex-c-c pagi-item hov-btn1 trans-03 m-all-7 "';

            $this->pagination->initialize($config);

            // build paging links
            $this->data['links'] = $this->pagination->create_links();
        }
		
		$this->data["page_ke"] = $page;
        $this->data["limit_page"] = $limit_page;
		$this->data['title'] = "Arsip";
		$this->data['page'] = "portal/arsip"; 
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('portal/viewar', $this->data);
	}
	
	function hit(){
		$dt = $this->m_portal->hit();
		echo json_encode($dt);
	}
	
	function hit_stat() {
		$dt = $this->m_portal->hit_stat();
		
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class kontak extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_user');
	  $this->load->model('m_global');
	  $this->load->model('m_kontak');
   	}
   	
	function index() { 
		$this->data["menu"] = $this->m_user->load_menu(); 
		$this->auth->cek_menu($this->config->item("kontak"));
		$this->data["page"] = "backend/v_kontak"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "Kontak"; 
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view('backend/view', $this->data); 
	}
	
	function list_kontak()
   	{   
		$dt = $this->m_global->get_data("kontak");    
		$this->data["recordsTotal"] = 1;
		$this->data["recordsFiltered"] = 1;
		$this->data["firstUrutan"] = 0;
		$this->data["lastUrutan"] = 0; 
		$this->data["draw"] = 0; 
		$this->data["data"] = $dt;  
		echo json_encode($this->data);		
	}
	
	function simpan() 
	{   $mode = $this->input->post("mode"); 
		$dt = $this->m_kontak->simpan($mode);  
		echo json_encode($dt);		
	}
	  
	
	function hapus() {
		$dt = $this->m_global->dele("kontak", "id_kontak", $this->input->post('id'));
		echo json_encode($dt);
	} 
	
	public function upload()
    {
	     if(isset($_FILES["file"]["name"]))  
	     {    $file_name =  $_FILES["file"]["name"];
		 	  $filenameNoExt = explode(".", $file_name);  
		 	  $date_start =  date('Y-m-d');
			  $url = './assets/kontak/';
			  (is_dir($url)<>'1' ? mkdir($url) : "");  
			 
			  $url = './assets/kontak/'.$date_start.'/';    //tahun
			  (is_dir($url)<>'1' ? mkdir($url) : "");   
			 
			  //$new_name = str_replace(".", "-", $filenameNoExt[0]) . "." . $filenameNoExt[1];
			  $new_name = date('Y-m-d-h-i-s') . "." . $filenameNoExt[1];
	          $config['upload_path'] = $url;  
	          $config['allowed_types'] = 'xlsx';  
			  $config['overwrite'] = true;
			  $config['file_name'] = $new_name;
	          $this->load->library('upload', $config);  
	          if(!$this->upload->do_upload('file'))  
	          {  
	               $dt = $this->upload->display_errors();  
				   $data["errMsg"] = $dt;
				   $data["errNum"] = 99; 
	          }  
	          else  
	          {  	$new_name = str_replace(" ", "_", $new_name);
	                $data = $this->upload->data();                 
					$data["errMsg"] = $url.$new_name;
				    $data["errNum"] = 0;
					
	          }  
			  
	          echo json_encode($data);  
	     } 
	}
	
	public function simpan_upload () {
		$file = $this->input->post("path") ;//'./assets/kontak/'.$upload_data['file_name']; 
				//load the excel library
				$this->load->library('excel');
				 
				//read file from path
				$objPHPExcel = PHPExcel_IOFactory::load($file);
				 
				//get only the Cell Collection
				$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
				 
				//extract to a PHP readable array format
				foreach ($cell_collection as $cell) {
				    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
				    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
				    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
				 
				    //The header will/should be in row 1 only. of course, this can be modified to suit your need.
				    if ($row == 1) {
				        $header[$row][$column] = $data_value;
				    } else {
				        $arr_data[$row][$column] = $data_value;
						
				    }
				}
				 
	              unlink($file);
				   
	              $data = $this->m_kontak->loaddata($arr_data); 
				  echo json_encode($data);  
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
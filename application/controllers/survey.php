<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Survey extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_user');
	  $this->load->model('m_global');
	  $this->load->model('m_survey');
   	}
   	
	function index() { 
		$this->data["menu"] = $this->m_user->load_menu(); 
		$this->data["page"] = "backend/v_survey"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "Laporan survey"; 
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view('backend/view', $this->data); 
	}
	
	function list_survey()
   	{   
		$dt = $this->m_global->get_data("survey");    
		$this->data["recordsTotal"] = 1;
		$this->data["recordsFiltered"] = 1;
		$this->data["firstUrutan"] = 0;
		$this->data["lastUrutan"] = 0; 
		$this->data["draw"] = 0; 
		$this->data["data"] = $dt;  
		echo json_encode($this->data);		
	}
	
	function simpan() 
	{   //$mode = $this->input->post("mode"); 
		//$dt = $this->m_survey->simpan($mode);  
		//echo json_encode($dt);	
			
	}
	  
	
	function hapus() {
		$dt = $this->m_global->dele("survey", "id_survey", $this->input->post('id'));
		echo json_encode($dt);
	} 
	 
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
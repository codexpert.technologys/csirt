<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Aduan extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_aduan');
	  $this->load->model('m_global');
	  $this->load->model('m_user');
   	}
  
  public function index()
   	{   $this->data["menu"] = $this->m_user->load_menu(); 
		$this->auth->cek_menu($this->config->item("portal"));
		$this->data["page"] = "backend/v_aduan"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "aduan";   
		$dt = $this->m_global->get_record_by_id("portal", "portal_id", "aduan");   
		$this->data["long_desc"] = ($dt != null ? $dt->long_desc : '');
		$this->data["portal_id"] = ($dt != null ? $dt->portal_id : '');
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view('backend/view', $this->data);
	}
	
	
	public function simpan() {
		$data = $this->m_aduan->simpan();
		echo json_encode($data);
	}	 
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Galeri  extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model("m_user");
	  $this->load->model("m_galeri");
	  $this->load->model("m_global");
   	}
   	
	public function index()
   	{   $this->data["menu"] = $this->m_user->load_menu(); 
		$this->auth->cek_menu($this->config->item("galeri"));
		$this->data["page"] = "backend/v_galeri"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "Galeri";  
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view('backend/view', $this->data);
	}
	
	public function list_galeri()
   	{   $dt = $this->m_global->get_data("galeri");   
		$this->data["recordsTotal"] = 1;
		$this->data["recordsFiltered"] = 1;
		$this->data["firstUrutan"] = 1;
		$this->data["lastUrutan"] = 1; 
		$this->data["draw"] = 0; 
		$this->data["data"] = $dt; 
		echo json_encode($this->data);		
	}
	
	function hapus() {
		$dt = $this->m_global->dele("galeri", "id_galeri", $this->input->post("id"), $this->input->post("nama_file"));
		echo json_encode($dt);
	}
	
	function simpan() 
	{   $mode = $this->input->post("mode"); 
		$dt = $this->m_galeri->simpan($mode);  
		echo json_encode($dt);		
	}
	
	/*function simpan() {
		if($_FILES['fileToUpload']['error']==4) { //if No file was uploaded.
			$this->data["nama_galeri"] = $this->input->post("nama_galeri"); 
			$this->data["nama_file"] = "";
			$this->data["tipe_file"] = "";
			$this->data["size_file"] = "";
			$this->data["path_file"] = "";
			$this->data["status"] = $this->input->post("status"); 
			$this->data["mode"] = $this->input->post("mode"); 
			  
			$this->m_galeri->simpan($this->data);  
			$this->index();
		} else {
			$filename = $_FILES['fileToUpload']["name"];
			$this->do_upload($filename);
		}
	}
	
	
	function do_upload($filename)
    {   $filenameNoExt = explode(".", $filename);  
		$url = './assets/galeri/';    //path 
		 
		if (is_dir($url)<>'1') {
			mkdir($url);
		}
		 
        if($_FILES['fileToUpload']['error']==4)  //if No file was uploaded.
            return false;        
        $config['upload_path'] = $url;
		$config['allowed_types'] = 'pdf|jpg|png|jpeg';
		$config['max_size']        = 4096;//5MB size allowed 
		$config["overwrite"] = true;
		$this->load->library('upload');
		$this->upload->initialize($config);

        if( $this->upload->do_upload('fileToUpload') )
        {    
            $files = $this->upload->data(); 
			
			$this->data["nama_galeri"] = $this->input->post("nama_galeri");
			$this->data["nama_file"] = str_replace(" ", "_", $filename);  
			$this->data["tipe_file"] = $files['file_type'];
			$this->data["size_file"] = $files['file_size'];
			$this->data["path_file"] = $url;
			$this->data["status"] = $this->input->post("status"); 
			$this->data["mode"] = $this->input->post("mode"); 
			  
			$this->m_galeri->simpan($this->data);  
			$this->index();
        } else {
			$msg = str_replace('<p>','',str_replace('</p>','',$this->upload->display_errors()));
			//$msg = str_replace('<p>','',str_replace('</p>','',$this->upload->file_type));
			echo $msg;
			//redirect('dokumen/error/'.$modul.'/'.$msg);
		}
    }*/
	
	public function saveGambar()
    {
         if(isset($_FILES["file"]["name"]))  
     {     $date_start =  date('Y-m');
		 
		   $url = './assets/galeri/'.$date_start.'/';    
			 
			if (is_dir($url)<>'1') {
				mkdir($url);
			} 
			 
          $config['upload_path'] = $url;  
          $config['allowed_types'] = 'jpg|jpeg|png';  
		  $config['overwrite'] = true;
			    $config['create_thumb']= true;
                $config['maintain_ratio']= FALSE;
                $config['width']= 600;
                $config['height']= 400;
				
          $this->load->library('upload', $config);  
          if(!$this->upload->do_upload('file'))  
          {  
               $dt = $this->upload->display_errors();  
               echo json_encode($dt);  
          }  
          else  
          {    	$files = $this->upload->data(); 
		  		$this->data["nama_file"] = str_replace(" ", "_", $_FILES["file"]["name"]);  
			   	$this->data["tipe_file"] = $files['file_type'];
				$this->data["size_file"] = $files['file_size'];
				$this->data["path_file"] = $url;        
				$this->data["data"] = $files; 
               echo json_encode($this->data);  
          }  
     } 
  }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
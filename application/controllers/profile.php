<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Profile extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_profile');
	  $this->load->model('m_global');
	  $this->load->model('m_user');
   	}
   	 
	
	function index() {  
		$this->data["menu"] = $this->m_user->load_menu(); 
		//$this->auth->cek_menu($this->config->item("user")); 
		$this->data["role"] = $this->m_global->get_data("role"); 
		$this->data["page"] = "backend/v_profile"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "Profile User"; 
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view('backend/view', $this->data); 
	}
	 
	
	function simpan() 
	{   $dt = $this->m_profile->simpan();  
		echo json_encode($dt);		
	}
	
	function get() 
	{   $dt = $this->m_global->get_record_by_id("user", "user_username", $this->session->userdata("username")) ;
		echo json_encode($dt);		
	} 
	 
	
	public function saveSk()
    {
         if(isset($_FILES["file"]["name"]))  
     {      
		   $url = './assets/users/';    //tahun
			 
			if (is_dir($url)<>'1') {
				mkdir($url);
			}
			 
          $config['upload_path'] = $url;  
          $config['allowed_types'] = 'pdf|jpg|jpeg|png|gif';  
		  $config['overwrite'] = true;
          $this->load->library('upload', $config);  
          if(!$this->upload->do_upload('file'))  
          {  
               $this->upload->display_errors();  
               return FALSE;
          }  
          else  
          {    	$files = $this->upload->data(); 
		  		$this->data["nama_file"] = str_replace(" ", "_", $_FILES["file"]["name"]);  
			   	$this->data["tipe_file"] = $files['file_type'];
				$this->data["size_file"] = $files['file_size'];
				$this->data["path_file"] = $url;         
               echo json_encode($this->data);  
          }  
     } 
  }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Rfc2350 extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_rfc2350');
	  $this->load->model('m_global');
	  $this->load->model('m_user');
   	}
  
  public function index()
   	{   $this->data["menu"] = $this->m_user->load_menu(); 
		$this->auth->cek_menu($this->config->item("portal"));
		$this->data["page"] = "backend/v_rfc2350"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "RFC 2350";   
		$dt = $this->m_global->get_record_by_id("portal", "portal_id", "rfc2350");   
		$this->data["long_desc"] = ($dt != null ? $dt->long_desc : '-');
		$this->data["portal_id"] = ($dt != null ? $dt->portal_id : '');
		$this->data['logo'] = $this->m_global->get_logo();
		$this->data['dt_file'] = $this->m_global->get_data("rfc_file");   
		$this->load->view('backend/view', $this->data);
	}
	
	public function uploadRfc()
    {
     if(isset($_FILES["file"]["name"]))  
     {   $file_name =  $_FILES["file"]["name"]; 
		 $arrFile = explode(".", $file_name);  
		 $arrCount = count($arrFile); 
		 $new_name = "";
		 $separator = "";
		 
		 for ($i=0; $i<$arrCount - 1; $i++) {
		 	$new_name = $new_name.$separator.$arrFile[$i];
			$separator = "_";
		 }
		 
		 $new_name = str_replace(" ", "-", $new_name);
		 $new_name_ext = $new_name.".".$arrFile[$arrCount-1]; 
		   
		   $url = './assets/rfc2350/'; 
			 
			if (is_dir($url)<>'1') {
				mkdir($url);
			}
		  
          $config['upload_path'] = $url;  
          $config['allowed_types'] = 'pdf';  
		  $config['overwrite'] = true;
		  $config['file_name'] = $new_name_ext;
          $this->load->library('upload', $config);  
          if(!$this->upload->do_upload('file'))  
          {  
               $dt = $this->upload->display_errors();  
			   
               echo json_encode($dt);  
          }  
          else  
          {  	//$new_name = str_replace(" ", "_", $new_name);
                $dt = $this->upload->data();    
				$data["path_file"] = $url;
				$data["data"] = $dt;
                echo json_encode($data);  
          }   
     } 
	}
	
	public function simpan() {
		$data = $this->m_rfc2350->simpan();
		echo json_encode($data);
	}
	
	public function simpan_file() {
		$data = $this->m_rfc2350->simpan_file();
		echo json_encode($data);
	}	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Email extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_user');
	  $this->load->model('m_global');
	  $this->load->model('m_email');
   	}
   	
	function index() { 
		
		$this->data["menu"] = $this->m_user->load_menu(); 
		$this->auth->cek_menu($this->config->item("email"));
		$this->data["page"] = "backend/v_email"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "Setting Email"; 
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view('backend/view', $this->data); 
	}
	
	function list_email()
   	{   
		$dt = $this->m_global->get_data("setting_email");    
		$this->data["recordsTotal"] = 1;
		$this->data["recordsFiltered"] = 1;
		$this->data["firstUrutan"] = 0;
		$this->data["lastUrutan"] = 0; 
		$this->data["draw"] = 0; 
		$this->data["data"] = $dt;  
		echo json_encode($this->data);		
	}
	
	function simpan() 
	{   $mode = $this->input->post("mode"); 
		$dt = $this->m_email->simpan($mode);  
		echo json_encode($dt);		
	}
	  
	 
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
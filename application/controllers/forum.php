<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Forum extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_global');
        $this->load->model('m_forum');
        $this->load->library('pagination');
    }

    function index() {
        $this->data["menu"] = $this->m_user->load_menu();
        $this->auth->cek_menu($this->config->item("forum"));
        $this->data["page"] = "backend/v_forum";
        $this->data["footer"] = "backend/footer";
        $this->data["title"] = "Forum";
		$this->data['logo'] = $this->m_global->get_logo();
        $this->load->view('backend/view', $this->data);
    }

    function list_forum() {
        $dt = $this->m_forum->get_forum();
        $this->data["recordsTotal"] = 1;
        $this->data["recordsFiltered"] = 1;
        $this->data["firstUrutan"] = 0;
        $this->data["lastUrutan"] = 0;
        $this->data["draw"] = 0;
        $this->data["data"] = $dt;
        echo json_encode($this->data);
    }

    function list_topik() {
        $dt = $this->m_forum->get_topik();
        $this->data["recordsTotal"] = 1;
        $this->data["recordsFiltered"] = 1;
        $this->data["firstUrutan"] = 0;
        $this->data["lastUrutan"] = 0;
        $this->data["draw"] = 0;
        $this->data["data"] = $dt;
        echo json_encode($this->data);
    }

    function list_reply() {
        //$dt = $this->m_global->get_list_by_id("reply", "id_topik", $this->input->post("id")); 
        $dt = $this->m_forum->get_reply();
        echo json_encode($dt);
    }

    function simpanf() {
        $mode = $this->input->post("mode");
        $dt = $this->m_forum->simpanf($mode);
        echo json_encode($dt);
    }

    function simpant() {
        $mode = $this->input->post("mode");
        $dt = $this->m_forum->simpant($mode);
        echo json_encode($dt);
    }

    function simpanr() {
        $mode = $this->input->post("mode");
        $dt = $this->m_forum->simpanr($mode);
        echo json_encode($dt);
    }

    function hapusf() {
        $this->m_global->dele("reply", "id_forum", $this->input->post('id'));
        $this->m_global->dele("topik", "id_forum", $this->input->post('id'));
        $dt = $this->m_global->dele("forum", "id_forum", $this->input->post('id'));
        echo json_encode($dt);
    }
    
    function hapust() {
        $this->m_global->dele("reply", "id_topik", $this->input->post('id'));
        $dt = $this->m_global->dele("topik", "id_topik", $this->input->post('id')); 
        echo json_encode($dt);
    }

    public function reply() { 
        //set records per page
        $limit_page = 10; 
        $page = ($this->security->xss_clean($this->uri->segment(4))) ? ($this->security->xss_clean($this->uri->segment(4)) - 1) : 0;
        $total = $this->m_forum->get_total();
        $this->data['topik'] = $this->m_forum->get_topik_by_id($this->security->xss_clean($this->uri->segment(3)));
        $this->data['results'] = $this->m_forum->get_current_page($limit_page, $page * $limit_page);
        if ($total > 0) {
            
            // get current page records
            

            $config['base_url'] = base_url() . 'forum/reply/' . $this->security->xss_clean($this->uri->segment(3));
            $config['total_rows'] = $total;
            $config['per_page'] = $limit_page;
            $config['uri_segment'] = 4;

            //paging configuration
            $config['num_links'] = 5;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;

            //bootstrap pagination 
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = '&laquo; First';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = 'Last &raquo';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '<li>';
            $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '<li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';

            $this->pagination->initialize($config);

            // build paging links
            $this->data['links'] = $this->pagination->create_links();
        }

        //$this->load->view('member_table', $params);
        $this->data["page_ke"] = $page;
        $this->data["limit_page"] = $limit_page;
        $this->data["menu"] = $this->m_user->load_menu();
        $this->data["page"] = "backend/v_reply";
        $this->data["footer"] = "backend/footer";
        $this->data["title"] = "Reply";
		$this->data['logo'] = $this->m_global->get_logo();
        $this->load->view('backend/view', $this->data);
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
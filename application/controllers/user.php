<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class User extends CI_Controller {

	public function __construct()
   	{
      parent::__construct(); 
	  $this->load->model('m_user');
	  $this->load->model('m_global');
   	}
   	 
	
	function index() {  
		$this->data["menu"] = $this->m_user->load_menu(); 
		$this->auth->cek_menu($this->config->item("user")); 
		$this->data["role"] = $this->m_global->get_data("role"); 
		$this->data["page"] = "backend/v_user"; 
		$this->data["footer"] = "backend/footer"; 
		$this->data["title"] = "User"; 
		$this->data['logo'] = $this->m_global->get_logo();
		$this->load->view('backend/view', $this->data); 
	}
	
	function list_user()
   	{   
		$this->auth->restrict();
	    $this->auth->cek_uri($this->config->item("user"), "list_user");
		$dt = $this->m_global->get_list_not_by_id("user", "user_role", "0");   
		//$rowcount = $dt->num_rows();  
		$this->data["recordsTotal"] = 1;
		$this->data["recordsFiltered"] = 1;
		$this->data["firstUrutan"] = 0;
		$this->data["lastUrutan"] = 0; 
		$this->data["draw"] = 0; 
		$this->data["data"] = $dt; 
		$this->data["role"] = $this->m_global->get_list_not_by_id("role", "id", $this->session->userdata('role'));  
		echo json_encode($this->data);		
	}
	
	function simpan() 
	{   $mode = $this->input->post("mode");
		$dt = $this->m_user->simpan($mode);  
		echo json_encode($dt);		
	}
	
	function is_change() 
	{   $dt = $this->m_global->get_column_by_id("user", "is_change", "user_username", $this->session->userdata("username")) ;
		echo json_encode($dt);		
	} 
	
	function save_change() 
	{   $dt = $this->m_user->save_change();
		echo json_encode($dt);		
	} 
	
	function hapus() {
		$dt = $this->m_global->dele("user", "user_username", $this->input->post('id'), "Y");
		echo json_encode($dt);
	}
	  
	
	public function saveSk()
    {
         if(isset($_FILES["file"]["name"]))  
     {      
		   $url = './assets/users/';    //tahun
			 
			if (is_dir($url)<>'1') {
				mkdir($url);
			}
			 
          $config['upload_path'] = $url;  
          $config['allowed_types'] = 'pdf';  
		  $config['overwrite'] = true;
          $this->load->library('upload', $config);  
          if(!$this->upload->do_upload('file'))  
          {  
               $dt = $this->upload->display_errors();  
			   
               echo json_encode($dt);  
          }  
          else  
          {    /*	$files = $this->upload->data(); 
		  		$this->data["nama_file"] = str_replace(" ", "_", $_FILES["file"]["name"]);  
			   	$this->data["tipe_file"] = $files['file_type'];
				$this->data["size_file"] = $files['file_size'];
				$this->data["path_file"] = $url;         
               echo json_encode($this->data);  */
			   $dt = $this->upload->data();    
				$data["path_file"] = $url;
				$data["data"] = $dt;
                echo json_encode($data);  
          }  
     } 
  }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
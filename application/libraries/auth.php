<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Auth library
 */

class Auth {

    var $CI = NULL;

    function __construct() {
        // get CI's object
        $this->CI = & get_instance();
        $data['captcha'] = "--";
    }

    // untuk validasi 
    function do_login($username, $password, $i_captcha) {
        // cek di database, ada ga?
        $md5_pass = md5($password);
        $this->CI->db->from('user');
        $this->CI->db->where('user_username', $username);
        $this->CI->db->where('user_password', $md5_pass);
        $this->CI->db->where('aktif', '1');
        $result = $this->CI->db->get();
        if ($result->num_rows() == 0) {
            // username dan password tidak ada
            return false;
        } else {
            if ($i_captcha == $this->CI->session->userdata("mycaptcha")) {
                $userdata = $result->row();

                session_start();
                $sessionlama = session_id();
                session_regenerate_id();
                $sessionbaru = session_id();

                $session_data = array(
                    'name' => $userdata->user_name,
                    'username' => $userdata->user_username,
                    'role' => $userdata->user_role,
                    'email' => $userdata->email,
                    'sessid' => $sessionbaru
                );

                // buat session
                $this->CI->session->set_userdata($session_data);
                $msg = "";
                return $msg;
            } else {
                $msg = "Captcha salah";
                ;
                return $msg;
            }
        }
    }

    // untuk mengecek apakah user sudah login/belum
    function is_logged_in() {
        if ($this->CI->session->userdata('username') == '') {
            return false;
        }
        return true;
    }

    // untuk validasi di setiap halaman yang mengharuskan authentikasi
    function restrict() {
        if ($this->is_logged_in() == false) {
            redirect('login');
        }
    }

    function cek_menu($idmenu) {
        $this->CI->load->model('m_user');
        $status_user = $this->CI->session->userdata('role');
        $allowed_role = $this->CI->m_user->get_array_menu($idmenu);
        if (in_array($status_user, $allowed_role) == false) {
            die("Maaf, Anda tidak berhak untuk mengakses halaman ini.");
        }
    }

    function cek_uri($idmenu, $cek_uri) {
        $this->CI->load->model('m_user'); 
        $allowed_uri = $this->CI->m_user->get_array_menu_uri($idmenu); 
        if (strcmp($cek_uri, $allowed_uri) <> 0) {
            die("Anda tidak berhak untuk mengakses halaman ini.");
        } 
    }

    function do_logout() {
        $this->CI->session->sess_destroy();
    }

    function buat_captcha() {
        $vals = array(
            'img_path' => './assets/captcha/',
            'img_url' => base_url() . 'assets/captcha/',
            'img_width' => '150',
            'img_height' => 30,
            'border' => 1,
            'expiration' => 7200,
            'font_path' => base_url('./global_assets/fonts/Roboto/Roboto-Bold.ttf') //'./path/to/fonts/texb.ttf',
        );
        // create captcha image
        $cap = create_captcha($vals);
        // store image html code in a variable
        $data['image'] = $cap['word']; //$cap['image']; 
        $data['msg'] = "";
        // store the captcha word in a session
        $this->CI->session->set_userdata('mycaptcha', $cap['word']);
        return $data;
    }

}

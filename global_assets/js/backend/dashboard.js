function initStat(dt, pg, val, pie, lbl_rose, data_rose){ 

	var pie_basic_element = document.getElementById('pie_basic'); 
	var columns_stacked_element = document.getElementById('columns_stacked'); 
    var pie_rose_element = document.getElementById('pie_rose');

    // Stacked columns
    if (columns_stacked_element) {

        // Initialize chart
        var columns_stacked = echarts.init(columns_stacked_element);

        // Options
        columns_stacked.setOption({

            // Define colors
            color: ['#2ec7c9','#b6a2de','#5ab1ef','#ffb980','#d87a80'],

            // Global text styles
            textStyle: {
                fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                fontSize: 13
            },

            // Chart animation duration
            animationDuration: 750,

            // Setup grid
            grid: {
                left: 0,
                right: 10,
                top: 35,
                bottom: 0,
                containLabel: true
            },

            // Add legend
            legend: {
                data: pg,// ----------------------------------------------- page
                itemHeight: 8,
                itemGap: 20
            },

            // Add tooltip
            tooltip: {
                trigger: 'axis',
                backgroundColor: 'rgba(0,0,0,0.75)',
                padding: [10, 15],
                textStyle: {
                    fontSize: 13,
                    fontFamily: 'Roboto, sans-serif'
                },
                axisPointer: {
                    type: 'shadow',
                    shadowStyle: {
                        color: 'rgba(0,0,0,0.025)'
                    }
                }
            },

            // Horizontal axis
            xAxis: [{
                type: 'category',
                data:  pg, // ----------------------------------- page
                axisLabel: {
                    color: '#333'
                },
                axisLine: {
                    lineStyle: {
                        color: '#999'
                    }
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#eee',
                        type: 'dashed'
                    }
                }
            }],

            // Vertical axis
            yAxis: [{
                type: 'value',
                axisLabel: {
                    color: '#333'
                },
                axisLine: {
                    lineStyle: {
                        color: '#999'
                    }
                },
                splitLine: {
                    lineStyle: {
                        color: '#eee'
                    }
                },
                splitArea: {
                    show: true,
                    areaStyle: {
                        color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                    }
                }
            }],

            // Add series - datanya series
            series: [
                {
                    name: 'Hit',
                    type: 'bar',
					stack: 'Advertising',
                    data: val //[320, 332, 301, 334, 390, 330, 320]
                } 
            ]
        });
    }
 	
	 // Basic pie chart
    if (pie_basic_element) {

        // Initialize chart
        var pie_basic = echarts.init(pie_basic_element);


        //
        // Chart config
        //

        // Options
        pie_basic.setOption({

            // Colors
            color: [
                '#2ec7c9','#b6a2de','#5ab1ef','#ffb980','#d87a80',
                '#8d98b3','#e5cf0d','#97b552','#95706d','#dc69aa',
                '#07a2a4','#9a7fd1','#588dd5','#f5994e','#c05050',
                '#59678c','#c9ab00','#7eb00a','#6f5553','#c14089'
            ],

            // Global text styles
            textStyle: {
                fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                fontSize: 13
            },

            // Add title
            title: {
                text: 'Hits Page',
                subtext: 'Statistik Halaman Pengunjung',
                left: 'center',
                textStyle: {
                    fontSize: 17,
                    fontWeight: 500
                },
                subtextStyle: {
                    fontSize: 12
                }
            },

            // Add tooltip
            tooltip: {
                trigger: 'item',
                backgroundColor: 'rgba(0,0,0,0.75)',
                padding: [10, 15],
                textStyle: {
                    fontSize: 13,
                    fontFamily: 'Roboto, sans-serif'
                },
                formatter: "{a} <br/>{b}: {c} ({d}%)"
            },

            // Add legend
            legend: {
                orient: 'vertical',
                top: 'center',
                left: 0,
                data: pg , 
                itemHeight: 8,
                itemWidth: 8
            },

            // Add series
            series: [{
                name: 'Halaman',
                type: 'pie',
                radius: '70%',
                center: ['50%', '57.5%'],
                itemStyle: {
                    normal: {
                        borderWidth: 1,
                        borderColor: '#fff'
                    }
                },
                data:pie  
            }]
        });
    }
	
	// Rose without labels
    if (pie_rose_element) {

        // Initialize chart
        var pie_rose = echarts.init(pie_rose_element);


        //
        // Chart config
        //

        // Options
        pie_rose.setOption({

            // Colors
            color: [
                '#2ec7c9','#b6a2de','#5ab1ef','#ffb980','#d87a80',
                '#8d98b3','#e5cf0d','#97b552','#95706d','#dc69aa',
                '#07a2a4','#9a7fd1','#588dd5','#f5994e','#c05050',
                '#59678c','#c9ab00','#7eb00a','#6f5553','#c14089'
            ],

            // Global text styles
            textStyle: {
                fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                fontSize: 13
            },

            // Add title
            title: {
                text: '',
                subtext: '',
                left: 'center',
                textStyle: {
                    fontSize: 17,
                    fontWeight: 500
                },
                subtextStyle: {
                    fontSize: 12
                }
            },

            // Add tooltip
            tooltip: {
                trigger: 'item',
                backgroundColor: 'rgba(0,0,0,0.75)',
                padding: [10, 15],
                textStyle: {
                    fontSize: 13,
                    fontFamily: 'Roboto, sans-serif'
                },			
                formatter: '{a} <br/>{b}: {c} ({d}%)'
            },

            // Add legend
            legend: {
                orient: 'vertical',
                top: 'center',
                left: 0,
                data: lbl_rose,  
                itemHeight: 8,
                itemWidth: 8
            },

            // Add series
            series: [
                {
                    name: 'Panduan',
                    type: 'pie',
                    radius: ['15%', '80%'],
                    center: ['50%', '57.5%'],
                    roseType: 'radius',
                    itemStyle: {
                        normal: {
                            borderWidth: 1,
                            borderColor: '#fff',
                            label: {
                                show: false
                            },
                            labelLine: {
                                show: false
                            }
                        },
                        emphasis: {
                            label: {
                                show: true
                            },
                            labelLine: {
                                show: true
                            }
                        }
                    },
                    data: data_rose  
                }
            ]
        });
    }
    //
    // Resize charts
    //

    // Resize function
    var triggerChartResize = function() { 
        columns_stacked_element && columns_stacked.resize(); 
    };

    // On sidebar width change
    $(document).on('click', '.sidebar-control', function() {
        setTimeout(function () {
            triggerChartResize();
        }, 0);
    });

    // On window resize
    var resizeCharts;
    window.onresize = function () {
        clearTimeout(resizeCharts);
        resizeCharts = setTimeout(function () {
            triggerChartResize();
        }, 200);
    };
	 
}
 
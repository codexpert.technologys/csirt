$(document).ready(
    function () { 
		initDataTable();
		
		$('#tbl_kontak tbody').on('click', 'tr', function () {
		     
        	 selectedRow = table.row(this).data(); 
        	 $("#id_kontak").val(selectedRow.id_kontak);   
			 $("#nama_kontak").val(selectedRow.nama_kontak);   
			 $("#nama_instansi").val(selectedRow.nama_instansi);   
			 $("#alamat").val(selectedRow.alamat);   
			 $("#no_telp").val(selectedRow.no_telp);   
			 $("#fax").val(selectedRow.fax);   
			 $("#email").val(selectedRow.email);   
			 $('#txtHapus').text('Data : "' + selectedRow.nama_instansi  + '" akan di hapus ? ');
			 $("#mode").val("edit"); 
    	});
		
		$("#tbl_kontak").on('click','.btnEdit',function(){   
	         $(".list_kontak").hide();
			 $(".form_kontak").show();  
			 
		});
		
		$("#btn_add_kontak").on('click',function(){  
			$('#myForm').trigger("reset");
	         $(".list_kontak").hide();
			 $(".form_kontak").show();  
			 $(".upload_kontak").hide();  
			 $("#kontak_id").prop('readonly', false); 
			 $("#mode").val("add"); 
			 $("#message").html('');
			 control = $("#fileToUpload");
				control.replaceWith( control.val('').clone( true ) );
		});
		
		$("#fileToUpload").on('change', function () {
                uploadFile($('#fileToUpload')[0].files[0]);
            });
		
		$("#btn_upload_xls").on('click',function(){  
			$('#myFormX').trigger("reset");
	         $(".list_kontak").hide();
			 $(".form_kontak").hide();  
			 $(".upload_kontak").show();  
			 $("#kontak_id").prop('readonly', false); 
			 $("#mode").val("add"); 
		});
		 
		$(".btn_cancel").on('click',function(){  
	         $(".list_kontak").show();
			 $(".form_kontak").hide();   
		});
		   
		
		$("#tbl_kontak").on('click','.btnDelete',function(){    
			 $('#modal_default').modal('show');  
			
    	});
		
		$(".btnHapus").click(function(){  
			$.ajax({
                        type:"post",
                        url:ci.baseurl + 'kontak/hapus', 
                        data:  {id:selectedRow.id_kontak} ,  
						dataType: 'json', 
                        success: function(response){  
							message_hapus(response.errNum, response.errMsg);   
							table.ajax.reload(); 
							$('#modal_default').modal('hide'); 
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
							 msg_error(XMLHttpRequest.statusText);
                        }
           }); 
    	});
		 
		$('#myForm').submit(function(e){ 
		    e.preventDefault();
		     var fa = $(this);  
		      $.ajax({
		        url: ci.baseurl + 'kontak/simpan', //fa.attr('action'),
		        type: 'post' , 
				dataType:'json',
		        data:  fa.serialize(), 
		        success: function(response) {  
		          if(response.errNum ==  0) {  
		            fa[0].reset();    
					message(response.errNum, response.errMsg);    
					$(".list_kontak").show();
					$(".form_kontak").hide();  
					$('.add_heading-elements').show();
					table.ajax.reload();     
		          }  else {
				  	message(response.errNum, response.errMsg);    
				  }
		        }
		     });
		 });
		 
		 $('#myFormX').submit(function(e){ 
		    e.preventDefault();
		     var fa = $(this);  
		      $.ajax({
		        url: ci.baseurl + 'kontak/simpan_upload', //fa.attr('action'),
		        type: 'post' , 
				dataType:'json',
		        data:  fa.serialize(), 
		        success: function(response) {  
				console.log("repsos:", response);
		          if(response.errNum ==  0) {  
		            fa[0].reset();    
					message(response.errNum, response.errMsg);    
					$(".list_kontak").show();
					$(".form_kontak").hide();  
					$('.add_heading-elements').show();
					table.ajax.reload();     
					$(".upload_kontak").hide();  
		          }  else {
				  	message(response.errNum, response.errMsg);    
				  }
		        }
		     });
		 });
});
 
function initDataTable() { 
    //waitingDialog.show('Loading.....');
    $('#tbl_kontak').dataTable().fnDestroy();
    table = $('#tbl_kontak').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [4], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' + 
						   '<li class="text-primary-600"><a href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' + 
						   (ci.sr === "0" ?'<li class="text-primary-600"><a href="#"><i class="icon-trash btnDelete"></i></a></li>' : '') + 
						   '</ul>';
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'kontak/list_kontak', 
            "type": "GET",
            "dataSrc": function (json) {  
                return json.data;
            }
        },
        "columns": [  
			{"data": "nama_instansi", "defaultContent": ""},
			{"data": "nama_kontak", "defaultContent": ""},
			{"data": "no_telp", "defaultContent": ""},
			{"data": "email", "defaultContent": ""} 
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    }); 
}

function uploadFile(file) {
    data = new FormData();
    data.append("file", file);
    url = ci.baseurl + "kontak/upload"; 
    $.ajax({
        data: data,
        type: "POST", 
        url: url,
		dataType:'json',
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) { 
			if (response.errNum === 99) {
				control = $("#fileToUpload");
				control.replaceWith( control.val('').clone( true ) );
	            $(".divMsg").show();
	            $("#message").html(response.errMsg);
				$("#path").val('');
			} else {  
				 $("#path").val(response.errMsg);
				 $("#message").html(response);
			}
        }
    });


}
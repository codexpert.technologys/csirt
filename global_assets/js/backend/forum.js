$(document).ready(
        function () {
            initDataTable();
            $("#div_konten").show();

            $("#btn_add_forum").on('click', function () {
                $('#myForum').trigger("reset");
                $("#div_konten").hide();
                $("#form_forum").show();
                $("#mode").val("add");
            });

            $("#btn_cancel_forum").on('click', function () {
                $("#div_konten").show();
                $("#form_forum").hide();
            });

            $('#tbl_forum tbody').on('click', 'tr', function () {
                selectedRow = table.row(this).data(); 
                $("#nama_forum").val(selectedRow.nama_forum);
                $("#deskripsi").html(selectedRow.deskripsi);
				$("#mode").val("edit");
                $("#id_forum").val(selectedRow.id_forum);
                $(".title_forum").text(selectedRow.nama_forum);
                $(".descr_forum").html(selectedRow.deskripsi);
                $(".create_forum").text(selectedRow.user_name);
                $(".date_forum").text(selectedRow.create_dt);
                $('#txtHapus').text('Data : "' + selectedRow.nama_forum + '" akan di hapus ? ');

                initDataTopik();
            });
            
            $("#tbl_forum").on('click', '.btnViewT', function () {
                
                $("#div_konten").hide();
                $("#div_forum").show();
                $("#div_topik").show();
            });

            $("#tbl_forum").on('click', '.btnDelete', function () {
                $('#modal_default').modal('show');
            });

            $("#tbl_topik tbody").on('click', '.btnDeleteT', function () {
                $("#flagD").val(1);
                $('#modal_default1').modal('show');
            });

//            $("#tbl_forum tbody").on('click', '.btnEdit', function () {
//                $("#div_konten").hide();
//                $("#form_forum").show();
//            });

            $("#btn_add_topik").on('click', function () { 
                $("#id_forumt").val($("#id_forum").val());
                $('#nama_topik').val('');
                $('#deskripsi').val('');
                $("#div_forum").hide();
                $("#div_topik").hide();
                $("#form_topik").show();
            });

            $("#btn_back_topik").on('click', function () {
                $("#div_konten").show();
                $("#div_forum").hide();
                $("#div_topik").hide();
            });
             

            $("#btn_cancel_topik").on('click', function () {
                $("#div_forum").show();
                $("#div_topik").show();
                $("#form_topik").hide();
            });

            $(".btn_balas").on('click', function () {
                $("#form_reply").show();
                $("#div_reply").hide();
            });

            $('#tbl_topik tbody').on('click', 'tr', function () {
                rowT = tablet.row(this).data(); 

                $("#div_header_title").text(rowT.nama_topik);
                $("#div_header_by").text("oleh " + rowT.user_name + ", " + rowT.create_dt);
                $("#div_header_descr").html(rowT.deskripsi);
                $("#id_forum_r").val(rowT.id_forum);
                $("#id_topik_r").val(rowT.id_topik);
                $('#txtHapusT').text('Data : "' + rowT.nama_topik + '" akan di hapus ? ');
                ($("#flagD").val() === "0" ? location.href = ci.baseurl + 'forum/reply/' + rowT.id_topik : "");
                //location.href = ci.baseurl + 'forum/reply/' + selectedRow.id_topik;
                //initDataReply(selectedRow.id_topik); 
            });
            
            $("#tbl_topik tbody").on('click', '.btnViewR', function () {
                $("#flagD").val(0);
            });

            $("#btn_cancel_reply").on('click', function () {
                $("#div_reply").show();
                $("#form_reply").hide();
            });

//            rowForum = table.row(this).data();
            //$('#txtHapus').text('Data : "' + rowForum.nama_lengkap + '" akan di hapus ? ');
            /*
             $('#tbl_forum tbody').on('click', 'tr', function () {
             
             selectedRow = table.row(this).data(); 
             $("#id_forum").val(selectedRow.id_forum);   
             $("#nama_forum").val(selectedRow.nama_forum);   
             $("#nama_instansi").val(selectedRow.nama_instansi);   
             $("#alamat").val(selectedRow.alamat);   
             $("#no_telp").val(selectedRow.no_telp);   
             $("#fax").val(selectedRow.fax);   
             $("#email").val(selectedRow.email);   
             $('#txtHapus').text('Data : "' + selectedRow.nama_instansi  + '" akan di hapus ? ');
             $("#mode").val("edit"); 
             });
             
             $("#tbl_forum").on('click','.btnEdit',function(){   
             $(".list_forum").hide();
             $(".form_forum").show();  
             
             }); 
             $("#tbl_forum").on('click','.btnDelete',function(){    
             $('#modal_default').modal('show');  
             
             });
             
             $(".btnHapus").click(function(){  
             $.ajax({
             type:"post",
             url:ci.baseurl + 'forum/hapus', 
             data:  {id:selectedRow.id_forum} ,   
             success: function(response){  
             message_hapus(response);
             table.ajax.reload(); 
             $('#modal_default').modal('hide'); 
             },
             error: function(XMLHttpRequest, textStatus, errorThrown) { 
             msg_error(XMLHttpRequest.statusText);
             }
             }); 
             });*/

            /*$(".btn_save_forum").click(function(){  
             $.ajax({
             type:"post",
             url:ci.baseurl + 'forum/simpan', 
             data: $("#myForm").serialize()   ,  
             success: function(response){   
             $(".list_forum").show();
             $(".form_forum").hide();   
             $('.add_heading-elements').show();
             message(response);
             table.ajax.reload(); 
             
             },
             error: function(XMLHttpRequest, textStatus, errorThrown) { 
             msg_error(XMLHttpRequest.statusText);
             }
             }); 
             });*/

            $(".btnHapus").click(function () {
                $.ajax({
                    type: "post",
                    url: ci.baseurl + 'forum/hapusf',
                    dataType: "json",
                    data: {id: selectedRow.id_forum},
                    success: function (response) {
                        message_hapus(response.errNum, response.errMsg);
                        table.ajax.reload();
                        $('#modal_default').modal('hide');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });
            
            $(".btnHapusT").click(function () { 
                $.ajax({
                    type: "post",
                    url: ci.baseurl + 'forum/hapust',
                    dataType: "json",
                    data: {id: rowT.id_topik},
                    success: function (response) {
                        message_hapus(response.errNum, response.errMsg);
                        tablet.ajax.reload();
                        $('#modal_default1').modal('hide');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });

            $('#myForum').submit(function (e) {
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'forum/simpanf', //fa.attr('action'),
                    type: 'post',
                    dataType: 'json',
                    data: fa.serialize(),
                    success: function (response) {
                        if (response.errNum == 0) {
                            fa[0].reset();
                            message(response.errNum, response.errMsg);
                            $("#div_konten").show();
                            $("#form_forum").hide();
                            table.ajax.reload();
                        } else {
                            message(response.errNum, response.errMsg);
                        }
                    }
                });
            });

            $('#myTopik').submit(function (e) {
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'forum/simpant',
                    type: 'post',
                    dataType: 'json',
                    data: fa.serialize(),
                    success: function (response) {
                        if (response.errNum == 0) {
                            fa[0].reset();
                            message(response.errNum, response.errMsg);
                            $("#div_forum").show();
                            $("#div_topik").show();
                            $("#form_topik").hide();

                            tablet.ajax.reload();     
                        } else {
                            message(response.errNum, response.errMsg);
                        }
                    }
                });
            });

            $('#myReply').submit(function (e) {
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'forum/simpanr',
                    type: 'post',
                    dataType: 'json',
                    data: fa.serialize(),
                    success: function (response) {
                        console.log("resp", response);
                        if (response.errNum == 0) {
                            fa[0].reset();
                            message(response.errNum, response.errMsg);
                            $("#div_reply").show();
                            $("#form_reply").hide();
                            location.reload();
                        } else {
                            message(response.errNum, response.errMsg);
                        }
                    }
                });
            });
			
			$("#deskripsi").keyup(function(){
			  $("#count").text("Karakter tersisa : " + (500 - $(this).val().length));
			});
			
        });



function initDataTable() {
    //waitingDialog.show('Loading.....');
    $('#tbl_forum').dataTable().fnDestroy();
    table = $('#tbl_forum').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [0], // Column to target
                "mRender": function (data, type, full) {
                    str = '<div class="media-left media-middle">' +
                            '<a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs">' +
                            '    <span class="letter-icon">A</span>' +
                            '</a>' +
                            '</div>' +
                            '<div class="media-body"> ' +
                            '    ' + full.user_name + ' ' +
                            '    <div class="text-muted text-size-mini"><i class="icon-watch2"></i> ' + full.create_dt + '</div>' +
                            '</div>';
                    return str;
                }
            },
            {
                "aTargets": [1], // Column to target
                "mRender": function (data, type, full) {
                    str = ' <div class="media-body">' +
                            //'      <a href="#" class="display-inline-block text-default text-semibold letter-icon-title link_forum">' + full.nama_forum + '</a>' +
                            '      ' + full.nama_forum + ' ' +
                            '      <span class="display-block text-muted text-size-mini">' + full.deskripsi + '</span>' +
                            ' </div>   ';
                    return str;
                }
            },
            {
                "aTargets": [2], // Column to target
                "mRender": function (data, type, full) {
                    str = '<span class="text-semibold">' + full.jml_topik + ' Topics</span><small class="display-block text-size-mini no-margin">' + full.jml_posts + ' Posts</small>';
                    return str;
                }
            },
            {
                "aTargets": [3], // Column to target
                "mRender": function (data, type, full) {
                    return '<div class="media-body">' +
                            //'     <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">' + full.last_post + '</a>' +
                            '     ' + full.last_post + ' ' +
                            '     <div class="text-muted text-size-mini"><i class="icon-watch2"></i> ' + full.last_date + '</div>' +
                            ' </div>';
                }
            },
            {
                "aTargets": [4], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' +
                            '<li class="text-primary-600"><a title="Lihat Topik" href="#"><i class="icon-search4 btnViewT"></i></a></li>' +
                            (ci.sr === "0" ? '<li class="text-primary-600"><a title="Hapus" href="#"><i class="icon-trash btnDelete"></i></a></li>' : "") +
                            '</ul>';

                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'forum/list_forum',
            "type": "GET",
            "dataSrc": function (json) { // console.log("json:", json);
                return json.data;
            }
        },
        "columns": [
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    });
}

function initDataTopik() {
    console.log("ksesini", selectedRow.id_forum);
    $('#tbl_topik').dataTable().fnDestroy();
    tablet = $('#tbl_topik').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [0], // Column to target
                "mRender": function (data, type, full) {
                    str = ' <a href="#" class="text-default display-inline-block">' +
                            '      <span class="text-semibold">' + full.nama_topik + '</span>' +
                            '      <span class="display-block text-muted text-size-mini">oleh : ' + full.user_name + ', ' + full.create_dt + '</span>' +
                            '  </a>';
                    return str;
                }
            },
            {
                "aTargets": [1], // Column to target
                "mRender": function (data, type, full) {
                    str = '<a href="#" class="text-default display-inline-block"><span class="text-semibold">' + full.jml_reply + ' replies</span></a>';
                    return str;
                }
            },
            {
                "aTargets": [2], // Column to target
                "mRender": function (data, type, full) {
                    return '<div class="media-body">' +
                            '     <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">' + full.last_post + '</a>' +
                            '     <div class="text-muted text-size-mini"><i class="icon-watch2"></i> ' + full.last_date + '</div>' +
                            '</div>';
                }
            },
            {
                "aTargets": [3], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' +
                            '<li class="text-primary-600"><a title="Lihat Detail" href="#"><i class="icon-search4 btnViewR"></i></a></li>' +
                            (ci.sr === "0" ? '<li class="text-primary-600"><a title="Hapus" href="#"><i class="icon-trash btnDeleteT"></i></a></li>' :"") +
                            '</ul>';

                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'forum/list_topik',
            "type": "POST",
            data: {id_topik: selectedRow.id_forum},
            "dataSrc": function (json) {
//                console.log("json:", json);
                return json.data;
            }
        },
        "columns": [
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    });
}


function initDataReply(id) {
    $.ajax({
        type: "post",
        url: ci.baseurl + 'forum/list_reply',
        dataType: 'json',
        data: {id: id},
        success: function (response) {
            console.log(response);
            if (response.length !== 0) {
                $.each(response, function (index, file) {
//                    console.log("file:", file);
                    str +=
                            '<div class="panel panel-white">' +
                            '	<div class="panel-heading">' +
                            '		<span class="badge bg-grey-700 pull-right">387</span> ' +
                            '		<span class="text-semibold">Topik 1 testing</span>' +
                            '        <span class="display-block text-muted text-size-mini">oleh : Administrator , ' + file.create_dt + '</span> ' +
                            '	</div>' +
                            '	<div class="panel-body">' +
                            '		' + file.deskripsi +
                            '	</div>' +
                            '</div>';
                });
                $("#div_detil").html(str);
            } else {
                console.log("kosong");
            }


        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            msg_error(XMLHttpRequest.statusText);
        }
    });
}

function nl2br (str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}
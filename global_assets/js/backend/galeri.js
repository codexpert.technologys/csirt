$(document).ready(
        function () {
            initDataTable();

            $('#tbl_galeri tbody').on('click', 'tr', function () {

                selectedRow = table.row(this).data();
//                console.log("selRow:", selectedRow);
                $("#id_galeri").val(selectedRow.id_galeri);
                $("#nama_galeri").val(selectedRow.nama);
                $("#nama_file").val(selectedRow.nama_file);
                $("#path_file").val(selectedRow.path_file);
                $("#size_file").val(selectedRow.size_file);
                $("#tipe_file").val(selectedRow.tipe_file);
                $("#lbl_uf").text(selectedRow.nama_file);
                $("a.uf").attr("href", selectedRow.path_file + selectedRow.nama_file);
                (selectedRow.status === "1" ? $("#status").prop("checked", true) : $("#status").prop("checked", false));
                (selectedRow.status === "1" ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
                $("#id_galeri").prop('readonly', true);
                $('#txtHapus').text('Data : "' + selectedRow.nama + '" akan di hapus ? ');
                $("#mode").val("edit");
				
				control = $("#fileToUpload");
				control.replaceWith( control.val('').clone( true ) );
            });

            $("#tbl_galeri").on('click', '.btnEdit', function () {
                $(".list_galeri").hide();
                $(".form_galeri").show();

            });

            $("#btn_add_galeri").on('click', function () {
                $('#myForm').trigger("reset");
                $('#fileToUpload').val('');
                $('.filename').text('');
                $(".list_galeri").hide();
                $(".form_galeri").show();
				$("#lbl_uf").text('');
				$("#txtStatus").text('UnPublish');
                $("#mode").val("add");
            });

            $("#status").on('click', function () {
                ($("#status").prop("checked") === true ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
                ($("#status").prop("checked") === true ? $("#status").val("1") : $("#status").val("0"));
            });

            $(".btn_cancel").on('click', function () {
                $(".list_galeri").show();
                $(".form_galeri").hide();
            });


            $("#tbl_galeri").on('click', '.btnDelete', function () {
                $('#modal_default').modal('show');

            });

            $(".btnHapus").click(function () {
                $.ajax({
                    type: "post",
                    url: ci.baseurl + 'galeri/hapus',
                    dataType: 'json',
                    data: {id: selectedRow.id_galeri, nama_file: selectedRow.nama_file},
                    success: function (response) {
                        message_hapus(response.errNum, response.errMsg);
                        table.ajax.reload();
                        $('#modal_default').modal('hide');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });

            $("#fileToUpload").on('change', function () {
				file = $('#fileToUpload')[0].files[0];
				url = 'galeri/saveGambar';
				ext = 'jpg|jpeg|png';  
                uploadFile(file, url, ext);
            });

            $('#myForm').submit(function (e) {
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'galeri/simpan', //fa.attr('action'),
                    type: 'post',
                    data: fa.serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.errNum == 0) {
                            fa[0].reset();
                            message(response.errNum, response.errMsg);
                            $(".list_galeri").show();
                            $(".form_galeri").hide();
                            table.ajax.reload();
                        } else {
                            message(response.errNum, response.errMsg);
                        }
                    }
                });

            });

        });

function initDataTable() {
    //waitingDialog.show('Loading.....');
    $('#tbl_galeri').dataTable().fnDestroy();
    table = $('#tbl_galeri').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [2], // Column to target
                "mRender": function (data, type, full) {
                    str = '<ul class="list-condensed list-unstyled no-margin">' +
                            '<li><span class="text-semibold">Size:</span> ' + full.size_file + ' Kb</li>' +
                            '<li><span class="text-semibold">Format:</span> .' + full.tipe_file + '</li>' +
                            '</ul>';
                    //str = "KK";
                    return str;
                }
            },
            {
                "aTargets": [3], // Column to target
                "mRender": function (data, type, full) {
                    return (full.status == '1' ? '<span class="label bg-blue">Publish</span>' : '<span class="label bg-danger">UnPublish</span>');
                }
            },
            {
                "aTargets": [4], // Column to target
                "mRender": function (data, type, full) {
                    str = '<a href="' + ci.baseurl + full.path_file + full.nama_file + '" data-popup="lightbox">' +
                            '<img src="' + ci.baseurl + full.path_file + full.nama_file + '" alt="" class="img-rounded img-preview">' +
                            '</a>';
                    return str;
                }
            },
            {
                "aTargets": [5], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' +
                            '<li class="text-primary-600"><a href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' +
                            (ci.sr === "0" ? '<li class="text-primary-600"><a href="#"><i class="icon-trash btnDelete"></i></a></li>' : '') +
                            '</ul>';
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'galeri/list_galeri',
            "type": "GET",
            "dataSrc": function (json) {
//                console.log("json", json);
                return json.data;
            }
        },
        "columns": [
            {"data": "nama", "defaultContent": ""},
            {"data": "create_dt", "defaultContent": ""}
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    });
}
 
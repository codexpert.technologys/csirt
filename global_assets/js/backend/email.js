$(document).ready(
    function () { 
		initDataTable();
		
		$('#tbl_email tbody').on('click', 'tr', function () {
		     
        	 selectedRow = table.row(this).data(); 
        	 $("#id").val(selectedRow.id);   
			 $("#from_email").val(selectedRow.from_email);   
			 $("#from_name").val(selectedRow.from_name);   
			 $("#to_email").val(selectedRow.to_email);    ;   
			 $('#txtHapus').text('Data : "' + selectedRow.nama_instansi  + '" akan di hapus ? ');
			 $("#mode").val("edit"); 
    	});
		
		$("#tbl_email").on('click','.btnEdit',function(){   
	         $(".list_email").hide();
			 $(".form_email").show();  
			 
		});
		
		$("#btn_add_email").on('click',function(){  
			$('#myForm').trigger("reset");
	         $(".list_email").hide();
			 $(".form_email").show();  
			 $("#email_id").prop('readonly', false); 
			 $("#mode").val("add"); 
		});
		 
		$(".btn_cancel").on('click',function(){  
	         $(".list_email").show();
			 $(".form_email").hide();   
		});
		   
		
		$("#tbl_email").on('click','.btnDelete',function(){    
			 $('#modal_default').modal('show');  
			
    	});
		
		/*
		$(".btnHapus").click(function(){  
			$.ajax({
                        type:"post",
                        url:ci.baseurl + 'email/hapus', 
                        data:  {id:selectedRow.id_email} ,  
						dataType: 'json', 
                        success: function(response){  
							message_hapus(response.errNum, response.errMsg);   
							table.ajax.reload(); 
							$('#modal_default').modal('hide'); 
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
							 msg_error(XMLHttpRequest.statusText);
                        }
           }); 
    	});
		*/
		/*$(".btn_save_email").click(function(){  
			$.ajax({
                        type:"post",
                        url:ci.baseurl + 'email/simpan', 
                        data: $("#myForm").serialize()   ,  
                        success: function(response){   
							$(".list_email").show();
							$(".form_email").hide();   
							$('.add_heading-elements').show();
							message(response);
							table.ajax.reload(); 
							
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
							 msg_error(XMLHttpRequest.statusText);
                        }
           }); 
    	});*/
		
		$('#myForm').submit(function(e){ 
		    e.preventDefault();
		     var fa = $(this);  
		      $.ajax({
		        url: ci.baseurl + 'email/simpan', //fa.attr('action'),
		        type: 'post' , 
				dataType:'json',
		        data:  fa.serialize(), 
		        success: function(response) {  
		          if(response.errNum ==  0) {  
		            fa[0].reset();    
					message(response.errNum, response.errMsg);    
					$(".list_email").show();
					$(".form_email").hide();  
					$('.add_heading-elements').show();
					table.ajax.reload();     
		          }  else {
				  	message(response.errNum, response.errMsg);    
				  }
		        }
		     });
		
		 });
		 
});
 
function initDataTable() { 
    //waitingDialog.show('Loading.....');
    $('#tbl_email').dataTable().fnDestroy();
    table = $('#tbl_email').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [3], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' + 
						   '<li class="text-primary-600"><a href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' //+ 
						   //(ci.sr === "0" ?'<li class="text-primary-600"><a href="#"><i class="icon-trash btnDelete"></i></a></li>' : '') + 
						   '</ul>';
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'email/list_email', 
            "type": "GET",
            "dataSrc": function (json) {   
				if (json.data.length != 0) {
					$("#rowAdd").hide(); 
				} else {
					$("#rowAdd").show();
				}
                return json.data;
            }
        },
        "columns": [  
			{"data": "from_email", "defaultContent": ""},
			{"data": "from_name", "defaultContent": ""},
			{"data": "to_email", "defaultContent": ""} 
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    }); 
}
function uploadFile(file, url, ext) {
    data = new FormData();
    data.append("file", file);
    url = ci.baseurl + url; 
    $.ajax({
        data: data,
        type: "POST",
        dataType: 'json',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) { console.log("xxx", response);
            if (response.data !== undefined) {
                $("#path_file").val(response.path_file);
                $("#nama_file").val(response.data.file_name);
                $("#size_file").val(response.data.file_size);
                $("#tipe_file").val(response.data.file_type);
                $(".divMsg").hide();
				$("#lbl_uf").text(response.data.file_name);
            } else {
				control = $("#fileToUpload");
				control.replaceWith( control.val('').clone( true ) );
				$(".divMsg").show();
				$("#modal_theme_primary").modal("hide");  
				msg_error(response + '\n File Type Allowed : ' + ext + ' Only');
				$("#path_file").val('');
                $("#nama_file").val('');
                $("#size_file").val('');
                $("#tipe_file").val(''); 			
				$("#lbl_uf").text('');	 
            } 
        },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
				$("#modal_theme_primary").modal("hide");
                msg_error(XMLHttpRequest.responseText + '\n Maximum File Upload : 25M'); 
            }
    });
}
$(document).ready(
    function () { 
		Noty.overrideDefaults({
                    theme: 'limitless',
                    layout: 'topRight',
                    type: 'alert',
                    timeout: 1000
                });
				 
		$("#fileToUpload").on('change', function () {
        	//uploadFile($('#fileToUpload')[0].files[0]);
			file = $('#fileToUpload')[0].files[0];
				url = ci.baseurl + "user/saveSk"
				ext = "PDF";  
                uploadFile(file, url, ext);
        });
		
		jQuery("#myForm").validationEngine({
        
        custom_error_messages : {
            '#username' : { 'required': { 'message': "Username Wajib Diisi" },
				'custom[onlyLetterNumber]': {
                    'message': "Hanya boleh Huruf dan Angka"
                }  }, 
			'#password' : { 'required': { 'message': "Password Wajib Diisi" } }, 
			'#nama' : { 'required': { 'message': "Nama Wajib Diisi" } }, 
			'#email' : { 'required': { 'message': "Email Wajib Diisi" },
				'custom[email]': {
                    'message': "Email tidak valid"
                } }, 
			'#fileToUpload' : { 'required': { 'message': "SK Penetapan Wajib Diisi" } }, 
			'#no_telp' : { 'required': { 'message': "No Telepon Wajib Diisi" },
				'custom[onlyNumberSp]': {
                    'message': "Hanya Boleh Diisi Angka"
                } }, 
			'#nip' : { 'required': { 'message': "NIP Wajib Diisi" },
				'custom[onlyNumberSp]': {
                    'message': "Hanya Boleh Diisi Angka"
                } }, 
			'#nik' : { 'required': { 'message': "NIK Wajib Diisi" },
				'custom[onlyNumberSp]': {
                    'message': "Hanya Boleh Diisi Wajib Diisi"
                } }, 
			'#asal_instansi' : { 'required': { 'message': "Asal Instansi Wajib Diisi" }}, 
			'#jabatan' : { 'required': { 'message': "Jabatan Wajib Diisi" } }
			
        },
        onValidationComplete: function(form, status){
            if(status==true){
              var fa = $('#myForm');  
		      $.ajax({
		        url: ci.baseurl + 'user/simpan', //fa.attr('action'),
		        type: 'post' , 
		        data:  fa.serialize(), 
				dataType: 'json',
		        success: function(response) {   console.log("response", response);
		          if(response.errNum == 0) {  
		            fa[0].reset();    
					message(response.errNum, response.errMsg);    
					
					
		          }  else {
				  	message(response.errNum, response.errMsg);    
				  }
		        },
		            error: function (XMLHttpRequest, textStatus, errorThrown, response) {  
		                msg_error(errorThrown); 
		            }
		     });
            }else{  
                //waitingDialog.hide();
            }
        }
    });
			
		//$('#myForm').submit(function(e){  
		//    e.preventDefault();
		     
		
		// });
		 
});
  

function uploadFile(file, url, ext) {
    data = new FormData();
    data.append("file", file);
    url = url;
    $.ajax({
        data: data,
        type: "POST",
        dataType: 'json',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) { console.log("xxx", response);
            if (response.data !== undefined) {
                $("#path_file").val(response.path_file);
                $("#nama_file").val(response.data.file_name);
                $("#size_file").val(response.data.file_size);
                $("#tipe_file").val(response.data.file_type);
                $(".divMsg").hide();
				$("#lbl_uf").text(response.data.file_name);
            } else {
				$(".divMsg").show();
				$("#modal_theme_primary").modal("hide");  
				msg_error(response + '\n File Type Allowed : ' + ext + ' Only');
				$("#path_file").val('');
                $("#nama_file").val('');
                $("#size_file").val('');
                $("#tipe_file").val(''); 
				control = $("#fileToUpload");
				control.replaceWith( control.val('').clone( true ) );
            } 
        },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
				$("#modal_theme_primary").modal("hide");
                msg_error(XMLHttpRequest.responseText); 
            }
    });
}

function message(num, msg) {
                if (num == 0) {
                    new Noty({
                        layout: 'center',
                        text: 'Data berhasil disimpan.',
                        type: 'success'
                    }).show();
					
					setTimeout(function () {
			            location.href = ci.baseurl + "login";
			        }, 5000);
					
                } else {
                    new Noty({
                        layout: 'center',
                        text: 'Data gagal disimpan. <br> \n' + msg,
                        type: 'error'
                    }).show();
                }
            }

function msg_error(txt) {
                new Noty({
                    layout: 'center',
                    text: txt,
                    type: 'error',
					timeout: 5000
                }).show();
            }			
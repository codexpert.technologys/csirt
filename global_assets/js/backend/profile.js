$(document).ready(
        function () {
            initDataTable();
            $('#tbl_user tbody').on('click', 'tr', function () {

                selectedRow = table.row(this).data();
                $("#username").val(selectedRow.user_username);
                $("#nama").val(selectedRow.user_name);
                $("#role").val(selectedRow.user_role);
                $("#nip").val(selectedRow.nip);
                $("#nik").val(selectedRow.nik);
                $("#no_telp").val(selectedRow.no_telp);
                $("#asal_instansi").val(selectedRow.asal_instansi);
                $("#jabatan").val(selectedRow.jabatan);
                $("#email").val(selectedRow.email);
                $("#lbl_sk").text(selectedRow.nama_file);
                $("a.sk").attr("href", selectedRow.path_file + selectedRow.nama_file);
                //console.log("selectedRow", selectedRow);
                (selectedRow.aktif === "1" ? $("#status").prop("checked", true) : $("#status").prop("checked", false));
                (selectedRow.aktif === "1" ? $("#txtStatus").text("Ya") : $("#txtStatus").text("Tidak"));
                $("#username").prop('readonly', true);
                $('#txtHapus').text('Data : "' + selectedRow.user_name + '" akan di hapus ? ');
                $("#mode").val("edit");
            });
  

            $("#fileToUpload").on('change', function () {
                uploadFile($('#fileToUpload')[0].files[0]);
            });

            $('#myForm').submit(function (e) {
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'profile/simpan', //fa.attr('action'),
                    type: 'post',
                    data: fa.serialize(),
                    dataType: 'json',
                    success: function (response) {
                            message(response.errNum, response.errMsg);
							initDataTable();
                    }
                });

            });

        });

function initDataTable() {
    $.ajax({
                    type: "post",
                    url: ci.baseurl + 'profile/get',
                    dataType: 'json',
                    success: function (response) { 
						console.log("resppp:", response);
                        $("#username").val(response.user_username);
		                $("#nama").val(response.user_name);
		                $("#role").val(response.user_role);
		                $("#nip").val(response.nip);
		                $("#nik").val(response.nik);
		                $("#no_telp").val(response.no_telp);
		                $("#asal_instansi").val(response.asal_instansi);
		                $("#jabatan").val(response.jabatan);
		                $("#email").val(response.email);
		                $("#lbl_sk").text(response.nama_file);
		                $("a.sk").attr("href", response.path_file + response.nama_file);
						$("#nama_file").val(response.nama_file);
		                $("#path_file").val(response.path_file);
		                $("#size_file").val(response.size_file);
		                $("#tipe_file").val(response.tipe_file);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
}

function uploadFile(file) {
    data = new FormData();
    data.append("file", file);
    url = "user/saveSk";
    $.ajax({
        data: data,
        type: "POST",
        dataType: 'json',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            $("#path_file").val(response.path_file);
            $("#nama_file").val(response.nama_file);
            $("#size_file").val(response.size_file);
            $("#tipe_file").val(response.tipe_file);
        }
    });
}

function set_form(bool) {
	$("#username").prop('readonly', bool);
    $("#nama").prop('readonly', bool);
    $("#email").prop('readonly', bool);
    $("#nip").prop('readonly', bool);
    $("#nik").prop('readonly', bool);
    $("#no_telp").prop('readonly', bool);
    $("#asal_instansi").prop('readonly', bool);
    $("#jabatan").prop('readonly', bool);
}
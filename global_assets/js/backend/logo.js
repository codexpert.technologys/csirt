$(document).ready(
        function () {
            initDataTable();

            $('#tbl_logo tbody').on('click', 'tr', function () {

                selectedRow = table.row(this).data();
//                console.log("selRow:", selectedRow);
                $("#id_logo").val(selectedRow.id_logo);
				$("#title").val(selectedRow.title);
                $("#nama_file").val(selectedRow.nama_file);
                $("#path_file").val(selectedRow.path_file);
                $("#size_file").val(selectedRow.size_file);
                $("#tipe_file").val(selectedRow.tipe_file);
                $("#lbl_uf").text(selectedRow.nama_file);
                $('#fileToUpload').val('');
                $("a.uf").attr("href", selectedRow.path_file + selectedRow.nama_file);
                (selectedRow.status === "1" ? $("#status").prop("checked", true) : $("#status").prop("checked", false));
                (selectedRow.status === "1" ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
                $("#id_logo").prop('readonly', true);
                $('#txtHapus').text('Data : "' + selectedRow.nama_file + '" akan di hapus ? ');
                $("#mode").val("edit");
            });

            $("#tbl_logo").on('click', '.btnEdit', function () {
                $(".list_logo").hide();
                $(".form_logo").show();

            });

            $("#btn_add_logo").on('click', function () {
                $('#myForm').trigger("reset");
                $('#fileToUpload').val('');
                $('.filename').text('');
                $(".list_logo").hide();
                $(".form_logo").show();
                $("#lbl_uf").text('');
                $("#txtStatus").text('Unpublish');
                $("#message").html("<font color='#000'>jpg|jpeg|png only - dimensions : 64 x 64 </font>");
                $("#mode").val("add");
            });

            $("#status").on('click', function () {
                ($("#status").prop("checked") === true ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
                ($("#status").prop("checked") === true ? $("#status").val("1") : $("#status").val("0"));
            });

            $(".btn_cancel").on('click', function () {
                $(".list_logo").show();
                $(".form_logo").hide();
            });


            $("#tbl_logo").on('click', '.btnDelete', function () {
                $('#modal_default').modal('show');

            });

            $(".btnHapus").click(function () {
                $.ajax({
                    type: "post",
                    url: ci.baseurl + 'logo/hapus',
                    dataType: 'json',
                    data: {id: selectedRow.id_logo, nama_file: selectedRow.nama_file},
                    success: function (response) {
                        message_hapus(response.errNum, response.errMsg);
                        table.ajax.reload();
                        $('#modal_default').modal('hide');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });

            $("#fileToUpload").on('change', function () {
				url = "logo/saveGambar";
				file = $('#fileToUpload')[0].files[0]; 
				ext = "jpg|jpeg|png";  
                uploadFile(file, url, ext); 
            });

            $('#myForm').submit(function (e) {
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'logo/simpan', //fa.attr('action'),
                    type: 'post',
                    data: fa.serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.errNum == 0) {
                            fa[0].reset();
                            message(response.errNum, response.errMsg);
                            $(".list_logo").show();
                            $(".form_logo").hide();
                            table.ajax.reload();
                        } else {
                            message(response.errNum, response.errMsg);
                        }
                    }
                });

            });

        });

function initDataTable() {
    //waitingDialog.show('Loading.....');
    $('#tbl_logo').dataTable().fnDestroy();
    table = $('#tbl_logo').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [1], // Column to target
                "mRender": function (data, type, full) {
                    str = '<ul class="list-condensed list-unstyled no-margin">' +
                            '<li><span class="text-semibold">Size:</span> ' + full.size_file + ' Kb</li>' +
                            '<li><span class="text-semibold">Format:</span> .' + full.tipe_file + '</li>' +
                            '</ul>';
                    //str = "KK";
                    return str;
                }
            },
            {
                "aTargets": [2], // Column to target
                "mRender": function (data, type, full) {
                    return (full.status == '1' ? '<span class="label bg-blue">Publish</span>' : '<span class="label bg-danger">UnPublish</span>');
                }
            },
            {
                "aTargets": [3], // Column to target
                "mRender": function (data, type, full) {
                    str = '<a href="' + ci.baseurl + full.path_file + full.nama_file + '" data-popup="lightbox">' +
                            '<img src="' + ci.baseurl + full.path_file + full.nama_file + '" alt="" class="img-rounded img-preview">' +
                            '</a>';
                    return str;
                }
            },
            {
                "aTargets": [4], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' +
                            '<li class="text-primary-600"><a href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' +
                            (ci.sr === "0" ? '<li class="text-primary-600"><a href="#"><i class="icon-trash btnDelete"></i></a></li>' : '') +
                            '</ul>';
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'logo/list_logo',
            "type": "GET",
            "dataSrc": function (json) {
                return json.data;
            }
        },
        "columns": [
            {"data": "title", "defaultContent": ""}
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    });
}
 
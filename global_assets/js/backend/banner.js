$(document).ready(
        function () {
            initDataTable();

            $('#tbl_banner tbody').on('click', 'tr', function () {

                selectedRow = table.row(this).data();
//                console.log("selRow:", selectedRow);
                $("#id_banner").val(selectedRow.id_banner);
                $("#nama_banner").val(selectedRow.nama);
				$("#keterangan").val(selectedRow.keterangan);
                $("#nama_file").val(selectedRow.nama_file);
                $("#path_file").val(selectedRow.path_file);
                $("#size_file").val(selectedRow.size_file);
                $("#tipe_file").val(selectedRow.tipe_file);
                $("#lbl_uf").text(selectedRow.nama_file);
                $('#fileToUpload').val('');
                $("a.uf").attr("href", selectedRow.path_file + selectedRow.nama_file);
                (selectedRow.status === "1" ? $("#status").prop("checked", true) : $("#status").prop("checked", false));
                (selectedRow.status === "1" ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
                $("#id_banner").prop('readonly', true);
                $('#txtHapus').text('Data : "' + selectedRow.nama + '" akan di hapus ? ');
                $("#mode").val("edit");
            });

            $("#tbl_banner").on('click', '.btnEdit', function () {
                $(".list_banner").hide();
                $(".form_banner").show();

            });

            $("#btn_add_banner").on('click', function () {
                $('#myForm').trigger("reset");
                $('#fileToUpload').val('');
                $('.filename').text('');
                $(".list_banner").hide();
                $(".form_banner").show();
                $("#lbl_uf").text('');
                $("#txtStatus").text('Unpublish');
                $("#message").html("<font color='#000'>jpg|jpeg|png only - dimensions : 1052 x 292 </font>");
                $("#mode").val("add");
            });

            $("#status").on('click', function () {
                ($("#status").prop("checked") === true ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
                ($("#status").prop("checked") === true ? $("#status").val("1") : $("#status").val("0"));
            });

            $(".btn_cancel").on('click', function () {
                $(".list_banner").show();
                $(".form_banner").hide();
            });


            $("#tbl_banner").on('click', '.btnDelete', function () {
                $('#modal_default').modal('show');

            });

            $(".btnHapus").click(function () {
                $.ajax({
                    type: "post",
                    url: ci.baseurl + 'banner/hapus',
                    dataType: 'json',
                    data: {id: selectedRow.id_banner, nama_file: selectedRow.nama_file},
                    success: function (response) {
                        message_hapus(response.errNum, response.errMsg);
                        table.ajax.reload();
                        $('#modal_default').modal('hide');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });

            $("#fileToUpload").on('change', function () {
                url = "banner/saveGambar";
				file = $('#fileToUpload')[0].files[0]; 
				ext = "jpg|jpeg|png";  
                uploadFile(file, url, ext); 
            });

            $('#myForm').submit(function (e) {
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'banner/simpan', //fa.attr('action'),
                    type: 'post',
                    data: fa.serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.errNum == 0) {
                            fa[0].reset();
                            message(response.errNum, response.errMsg);
                            $(".list_banner").show();
                            $(".form_banner").hide();
                            table.ajax.reload();
                        } else {
                            message(response.errNum, response.errMsg);
                        }
                    }
                });

            });

        });

function initDataTable() {
    //waitingDialog.show('Loading.....');
    $('#tbl_banner').dataTable().fnDestroy();
    table = $('#tbl_banner').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [2], // Column to target
                "mRender": function (data, type, full) {
                    str = '<ul class="list-condensed list-unstyled no-margin">' +
                            '<li><span class="text-semibold">Size:</span> ' + full.size_file + ' Kb</li>' +
                            '<li><span class="text-semibold">Format:</span> .' + full.tipe_file + '</li>' +
                            '</ul>';
                    //str = "KK";
                    return str;
                }
            },
            {
                "aTargets": [3], // Column to target
                "mRender": function (data, type, full) {
                    return (full.status == '1' ? '<span class="label bg-blue">Publish</span>' : '<span class="label bg-danger">UnPublish</span>');
                }
            },
            {
                "aTargets": [4], // Column to target
                "mRender": function (data, type, full) {
                    str = '<a href="' + ci.baseurl + full.path_file + full.nama_file + '" data-popup="lightbox">' +
                            '<img src="' + ci.baseurl + full.path_file + full.nama_file + '" alt="" class="img-rounded img-preview">' +
                            '</a>';
                    return str;
                }
            },
            {
                "aTargets": [5], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' +
                            '<li class="text-primary-600"><a href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' +
                            (ci.sr === "0" ? '<li class="text-primary-600"><a href="#"><i class="icon-trash btnDelete"></i></a></li>' : '') +
                            '</ul>';
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'banner/list_banner',
            "type": "GET",
            "dataSrc": function (json) { 
                return json.data;
            }
        },
        "columns": [
            {"data": "nama", "defaultContent": ""},
            {"data": "create_dt", "defaultContent": ""}
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    });
}

 
$(document).ready(
    function () { 
		initDataTable();
		$('#tbl_survey tbody').on('click', 'tr', function () {
		     
        	 selectedRow = table.row(this).data();
			 console.log("selRow:", selectedRow);
        	 $("#id_survey").val(selectedRow.id_survey); 
			 $("#nama_survey").val(selectedRow.nama_survey);  
			 $("#nama_file").val(selectedRow.nama_file); 
			 $("#path_file").val(selectedRow.path_file); 
			 $("#size_file").val(selectedRow.size_file); 
			 $("#tipe_file").val(selectedRow.tipe_file);
			 (selectedRow.status == "1" ? $("#status").prop("checked", true): $("#status").prop("checked", false));
			 (selectedRow.status == "1" ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
			 $("#id_survey").prop('readonly', true); 
			 $('#txtHapus').text('Data : "' + selectedRow.nama_survey + '" akan di hapus ? ');
			 $("#mode").val("edit"); 
    	});
		
		$("#tbl_survey").on('click','.btnEdit',function(){   
	         $(".list_survey").hide();
			 $(".form_survey").show();  
			 
		});
		
		$("#btn_add_survey").on('click',function(){  
			$('#myForm').trigger("reset");
	         $(".list_survey").hide();
			 $(".form_survey").show();  
			 $("#id_survey").prop('readonly', false); 
			 $("#mode").val("add"); 
		});
		
		$("#status").on('click',function(){  
			  ($("#status").prop("checked") === true ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish")); 
			  ($("#status").prop("checked") === true ? $("#status").val("1") : $("#status").val("0")); 
		});
		
		$(".btn_cancel").on('click',function(){  
	         $(".list_survey").show();
			 $(".form_survey").hide();   
		});
		   
		
		$("#tbl_survey").on('click','.btnDelete',function(){    
			 $('#modal_default').modal('show');  
			
    	});
		
		$(".btnHapus").click(function(){  
			$.ajax({
                        type:"post",
                        url:ci.baseurl + 'survey/hapus', 
                        data:  {id:selectedRow.id_survey, nama_file:selectedRow.nama_file} ,  
						dataType: 'json',
                        success: function(response){   
							message_hapus(response.errNum, response.errMsg);    
							table.ajax.reload(); 
							$('#modal_default').modal('hide'); 
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
							 msg_error(XMLHttpRequest.statusText);
                        }
           }); 
    	});
		 
});

function initDataTable() { 
    //waitingDialog.show('Loading.....');
    $('#tbl_survey').dataTable().fnDestroy();
    table = $('#tbl_survey').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [2], // Column to target
                "mRender": function (data, type, full) {
                    return full.size_file + ' KB';
                }
            },
			{
                "aTargets": [3], // Column to target
                "mRender": function (data, type, full) {
                    return full.hit;
                }
            },
            {
                "aTargets": [4], // Column to target
                "mRender": function (data, type, full) {
                    return (full.status=='1' ? '<span class="label bg-blue">Publish</span>': '<span class="label bg-danger">UnPublish</span>');
                }
            },
            {
                "aTargets": [5], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' + 
						   '<li class="text-primary-600"><a href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' + 
						   '<li class="text-primary-600"><a href="#"><i class="icon-trash btnDelete"></i></a></li>' + 
						   '</ul>';
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'survey/list_survey', 
            "type": "GET",
            "dataSrc": function (json) { 
                return json.data;
            }
        },
        "columns": [ 
			{"data": "nama_survey", "defaultContent": ""},
			{"data": "create_dt", "defaultContent": ""} 
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    }); 
}
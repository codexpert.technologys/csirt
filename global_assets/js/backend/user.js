$(document).ready(
        function () {
            initDataTable();
            $('#tbl_user tbody').on('click', 'tr', function () {

                selectedRow = table.row(this).data();
                $("#username").val(selectedRow.user_username);
                $("#nama").val(selectedRow.user_name);
                $("#role").val(selectedRow.user_role);
                $("#nip").val(selectedRow.nip);
                $("#nik").val(selectedRow.nik);
                $("#no_telp").val(selectedRow.no_telp);
                $("#asal_instansi").val(selectedRow.asal_instansi);
                $("#jabatan").val(selectedRow.jabatan);
                $("#email").val(selectedRow.email);
                $("#lbl_sk").text(selectedRow.nama_file);
                $("a.sk").attr("href", selectedRow.path_file + selectedRow.nama_file);
                console.log("selectedRow", selectedRow);
                (selectedRow.aktif === "1" ? $("#status").prop("checked", true) : $("#status").prop("checked", false));
                (selectedRow.aktif === "1" ? $("#txtStatus").text("Ya") : $("#txtStatus").text("Tidak"));
                $("#username").prop('readonly', true);
                $('#txtHapus').text('Data : "' + selectedRow.user_name + '" akan di hapus ? ');
                $("#mode").val("edit");
            });

            $("#tbl_user").on('click', '.btnEdit', function () {
                $(".list_user").hide();
                $(".form_user").show();
				(ci.sr === "0" ? set_form(false) : set_form(true));
            });

            $("#btn_add_user").on('click', function () {
                $('#myForm').trigger("reset");
                $(".list_user").hide();
                $(".form_user").show();
                set_form(false);
                $("#status").prop("checked", false);
                $("#txtStatus").text("Tidak");
                $("#mode").val("add");
            });

            $("#status").on('click', function () {
                ($("#status").prop("checked") === true ? $("#txtStatus").text("Ya") : $("#txtStatus").text("Tidak"));
                ($("#status").prop("checked") === true ? $("#status").val("1") : $("#status").val("0"));
            });

            $(".btn_cancel").on('click', function () {
                $(".list_user").show();
                $(".form_user").hide();
            });


            $("#tbl_user").on('click', '.btnDelete', function () {
                $('#modal_default').modal('show');

            });

            $(".btnHapus").click(function () {
                $.ajax({
                    type: "post",
                    url: ci.baseurl + 'user/hapus',
                    data: {id: selectedRow.user_username},
                    dataType: 'json',
                    success: function (response) {
//                        console.log("response:", response);
                        message_hapus(response.errNum, response.errMsg);
                        table.ajax.reload();
                        $('#modal_default').modal('hide');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });

            $("#fileToUpload").on('change', function () {
                uploadFile($('#fileToUpload')[0].files[0]);
            });

            $('#myForm').submit(function (e) {
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'user/simpan', //fa.attr('action'),
                    type: 'post',
                    data: fa.serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.errNum == 0) {
                            fa[0].reset();
                            message(response.errNum, response.errMsg);
                            $(".list_user").show();
                            $(".form_user").hide();
                            table.ajax.reload();
                        } else {
                            message(response.errNum, response.errMsg);
                        }
                    }
                });

            });

        });

function initDataTable() {
    //waitingDialog.show('Loading.....');
    $('#tbl_user').dataTable().fnDestroy();
    table = $('#tbl_user').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [2], // Column to target
                "mRender": function (data, type, full) {
                    if (full.user_role == '1') {
                        dt = 'Admin';
                    } else if (full.user_role == '2') {
                        dt = 'User';
                    }
                    return dt;
                }
            },
            {
                "aTargets": [3], // Column to target
                "mRender": function (data, type, full) {
                    return (full.aktif == '1' ? '<span class="label bg-success">Aktif</span>' : '<span class="label bg-danger">Tidak</span>');
                }
            },
            {
                "aTargets": [4], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' +
                            '<li class="text-primary-600"><a href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' +
                            (ci.sr === "0" ? '<li class="text-primary-600"><a href="#"><i class="icon-trash btnDelete"></i></a></li>' : '') +
                            '</ul>';
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'user/list_user',
            "type": "GET",
            "dataSrc": function (json) {
                return json.data;
            }
        },
        "columns": [
            {"data": "user_username", "defaultContent": ""},
            {"data": "user_name", "defaultContent": ""}

        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    });
}

function uploadFile(file) {
    data = new FormData();
    data.append("file", file);
    url = "user/saveSk";
    $.ajax({
        data: data,
        type: "POST",
        dataType: 'json',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            $("#path_file").val(response.path_file);
            $("#nama_file").val(response.nama_file);
            $("#size_file").val(response.size_file);
            $("#tipe_file").val(response.tipe_file);
        }
    });
}

function set_form(bool) {console.log("kesini");
	//$("#username").prop('readonly', bool);
    $("#nama").prop('readonly', bool);
    $("#email").prop('readonly', bool);
    $("#nip").prop('readonly', bool);
    $("#nik").prop('readonly', bool);
    $("#no_telp").prop('readonly', bool);
    $("#asal_instansi").prop('readonly', bool);
    $("#jabatan").prop('readonly', bool);
}
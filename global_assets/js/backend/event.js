$(document).ready(
        function () {
            tempMateri = [];
            arrMateri = [];
            initDataTable();
            $(".err_materi").hide();
            $('#tbl_event tbody').on('click', 'tr', function () {
                tempMateri = [];
                arrMateri = [];
                selectedRow = table.row(this).data();
                $("#id_event").val(selectedRow.id_event);
                $("#nama_event").val(selectedRow.nama_event);
                $("#tanggal_awal").val(selectedRow.tanggal_awal);
                $("#tanggal_akhir").val(selectedRow.tanggal_akhir);
                $("#tempat").val(selectedRow.tempat);
                (selectedRow.status == "1" ? $("#status").prop("checked", true) : $("#status").prop("checked", false));
                (selectedRow.status == "1" ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
                $("#id_event").prop('readonly', true);
                $('#txtHapus').text('Data : "' + selectedRow.nama_event + '" akan di hapus ? ');
                $('.add_heading-elements').hide();
                $("#mode").val("edit");

                //get detil 
                $.ajax({
                    type: "post",
                    url: ci.baseurl + 'event/materi',
                    data: {id: selectedRow.id_event},
                    dataType: 'json',
                    success: function (response) {
                        $('#tbl_materi tbody').empty();
                        $.each(response.data, function (index, file) {
                            var strRow =
                                    '<tr id="' + file.id_materi + '">' +
                                    '<td>' + file.nama_materi + '</td>' +
                                    '<td>' + file.nama_file + '</td>' +
                                    '<td align="center">' + file.size_file + ' KB</td>' +
                                    '<td align="center">' + '<a href="javascript:void(0);" title="Hapus Materi" class="widget-icon widget-icon-dark" onclick="del_temp_arr(' + file.id_materi + ')";><span class="icon-trash"></span></a>' + '</td>' +
                                    '</tr>';
                            $('#tbl_materi tbody').append(strRow);
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });

            $("#tbl_event").on('click', '.btnEdit', function () {
                $(".err_materi").show();
                $(".list_event").hide();
                $(".form_event").show();

            });

            $("#btn_add_event").on('click', function () {
                $('#myForm').trigger("reset");
                $(".list_event").hide();
                $(".form_event").show();
                $("#id_event").prop('readonly', false);
                $("#mode").val("add");
                $('.add_heading-elements').hide();
                $('#tbl_materi tbody').empty();

            });

            $("#status").on('click', function () {
                ($("#status").prop("checked") === true ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
                ($("#status").prop("checked") === true ? $("#status").val("1") : $("#status").val("0"));
            });

            $(".btn_cancel").on('click', function () {
                $(".list_event").show();
                $(".form_event").hide();
                $('.add_heading-elements').show();
            });

            $(".btn_add_materi").on('click', function () {
                $('#myFormU').trigger("reset");
                $(".err_materi").hide();
                $(".filename").text("");
				$("#message").html("pdf Only");
            });


            $("#tbl_event").on('click', '.btnDelete', function () {
                $('#modal_default').modal('show');
				$('.add_heading-elements').show();
            });

            $(".btnHapus").click(function () {
                $.ajax({
                    type: "post",
                    url: ci.baseurl + 'event/hapus',
                    data: {id: selectedRow.id_event, nama_file: selectedRow.nama_file},
                    dataType: 'json',
                    success: function (response) {
                        message_hapus(response.errNum, response.errMsg);
                        table.ajax.reload();
                        $('#modal_default').modal('hide');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });

            $(".btn_save_event").click(function () {
                $.ajax({
                    type: "post",
                    url: ci.baseurl + 'event/simpan',
                    dataType: 'json',
                    data: {id_event: $("#id_event").val(),
                        nama_event: $("#nama_event").val(),
                        tanggal_awal: $("#tanggal_awal").val(),
                        tanggal_akhir: $("#tanggal_akhir").val(),
                        tempat: $("#tempat").val(),
                        status: $("#status").val(),
                        mode: $("#mode").val(),
                        materi: arrMateri
                    },
                    success: function (response) {
                        $(".list_event").show();
                        $(".form_event").hide();
                        $('.add_heading-elements').show();
                        message(response.errNum, response.errMsg);
						arrMateri=[];
                        table.ajax.reload();

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });

            $("#fileToUpload").on('change', function () {
                file = $('#fileToUpload')[0].files[0];
				url = "event/upload";
				ext = "PDF";  
                uploadFile(file, url, ext);
            });

            $("#btn_save_materi").on('click', function () {
                nama_materi = $("#nama_materi").val();
                if (nama_materi != "") {
                    z = $('#fileToUpload')[0].files[0];
                    
                    str = z.name;
                    ext = str.substring(str.length - 3, str.length);
					file_name = $("#nama_file").val();
                    file_size = $("#size_file").val();
                    file_type = $("#tipe_file").val();
					
					if (ext !== "pdf") {
						file_name = "";
                    	file_size = "0";
                    	file_type = "";
					}

                    tempMateri = {
                        arr_materi: nama_materi,
                        arr_name: file_name,
                        arr_size: file_size,
                        arr_type: file_type
                    };
                    //console.log("tempMateri:", tempMateri);
                    arrMateri.push(tempMateri);
                    $('#modal_theme_primary').modal('toggle');
                    //uploadFile($('#fileToUpload')[0].files[0]);
                    $("#myFormU").trigger("reset");

                    randomNumber = getRandomArbitrary();
                    var strRow =
                            '<tr id="N-' + randomNumber + '">' +
                            '<td>' + nama_materi + '</td>' +
                            '<td>' + file_name + '</td>' +
                            '<td align="center">' + file_size + ' KB</td>' +
                            '<td align="center">' + '<a href="javascript:void(0);" title="Hapus Materi" class="widget-icon widget-icon-dark" onclick="del_temp_arr(\'N-' + randomNumber + '\')";><span class="icon-trash"></span></a>' + '</td>' +
                            '</tr>';
                    $('#tbl_materi tbody').append(strRow);
                } else {
                    $(".err_materi").show();
                    $(".err_materi").text("Isi nama materi !");
                }
            });

        });
 
function initDataTable() {
    //waitingDialog.show('Loading.....');
    $('#tbl_event').dataTable().fnDestroy();
    table = $('#tbl_event').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [4], // Column to target
                "mRender": function (data, type, full) {
                    return (full.status == '1' ? '<span class="label bg-blue">Publish</span>' : '<span class="label bg-danger">UnPublish</span>');
                }
            },
            {
                "aTargets": [5], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' +
                            '<li class="text-primary-600"><a href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' +
                            (ci.sr === "0" ? '<li class="text-primary-600"><a href="#"><i class="icon-trash btnDelete"></i></a></li>' : '') +
                            '</ul>';
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'event/list_event',
            "type": "GET",
            "dataSrc": function (json) {
               // console.log("json", json);
                return json.data;
            }
        },
        "columns": [
            {"data": "nama_event", "defaultContent": ""},
            {"data": "tanggal_awal", "defaultContent": ""},
            {"data": "tanggal_akhir", "defaultContent": ""},
            {"data": "tempat", "defaultContent": ""}
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    });
}

function del_temp_arr(r) {
   // console.log("r nya :", r);
    $target = $('#' + r);
    var tempDokMat = $target.find('td').toArray();
    var namaMateri = $(tempDokMat[0]).html();
    var namaFile = $(tempDokMat[1]).html();
    arrMateri = deleteObjFromList(arrMateri, namaMateri, 'arr_materi');
    $target.remove();

    //del file 
    $.ajax({
        type: "post",
        url: ci.baseurl + 'event/hapus_file',
        data: {id: r, nama_file: namaFile},
        dataType: 'json',
        success: function (response) {console.log("respo:", response);
            message_hapus(response.errNum, response.errMsg);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            msg_error(XMLHttpRequest.statusText);
        }
    });
}

function deleteObjFromList(listObj, rowId, paramToCompare) {

    try {
        $.each(listObj, function (index, value) {
            var idObj1 = value[paramToCompare];
            if (idObj1 == rowId) {
                listObj.splice(index, 1);
                return listObj;
            }
        });
    } catch (err) {
        //console.log('error karena data sudah dihapus');
    }

    return listObj;
}

function getRandomArbitrary() {
    return parseInt(Math.random() * (100 - 1) + 1);
}
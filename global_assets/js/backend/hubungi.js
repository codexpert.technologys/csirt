$(document).ready(
    function () { 
		initDataTable();
		
		$('#tbl_hubungi tbody').on('click', 'tr', function () {
		     
        	 selectedRow = table.row(this).data(); 
        	 $("#id_hubungi").val(selectedRow.hubungi_id);   
			 $("#nama_lokasi").val(selectedRow.nama_lokasi);
			 $("#alamat_lokasi").val(selectedRow.alamat_lokasi);
			 $("#telepon").val(selectedRow.telepon);
			 $("#email").val(selectedRow.email);
			 $("#url_map").val(selectedRow.url_map);
			 $("#keterangan").val(selectedRow.keterangan);
			 var htmlEntities = selectedRow.url_map;
			 var htmlDecode =$.parseHTML(htmlEntities)[0]['wholeText']; 
			 $("#urlMap").html(htmlDecode);
			 $('#txtHapus').text('Data : "' + selectedRow.hubungi  + '" akan di hapus ? ');
			 $("#mode").val("edit"); 
    	});
		
		$("#tbl_hubungi").on('click','.btnEdit',function(){   
	         $(".list_hubungi").hide();
			 $(".form_hubungi").show();  
			 
		});
		
		$("#btn_add_hubungi").on('click',function(){  
			$('#myForm').trigger("reset");
	         $(".list_hubungi").hide();
			 $(".form_hubungi").show();  
			 $("#hubungi_id").prop('readonly', false); 
			 $("#mode").val("add"); 
		});
		 
		$(".btn_cancel").on('click',function(){  
	         $(".list_hubungi").show();
			 $(".form_hubungi").hide();   
		});	   
		
		$("#tbl_hubungi").on('click','.btnDelete',function(){    
			 $('#modal_default').modal('show');  
    	});
		
		$(".btnHapus").click(function(){  
			$.ajax({
                        type:"post",
                        url:ci.baseurl + 'hubungi/hapus', 
						dataType:'json',
                        data:  {id:selectedRow.hubungi_id} ,   
                        success: function(response){  
							message_hapus(response.errNum, response.errMsg);
							table.ajax.reload(); 
							$('#modal_default').modal('hide'); 
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
							 msg_error(XMLHttpRequest.statusText);
                        }
           }); 
    	});
		 
		
		$('#myForm').submit(function(e){ 
		    e.preventDefault();
		     var fa = $(this);  
		      $.ajax({
		        url: ci.baseurl + 'hubungi/simpan', //fa.attr('action'),
		        type: 'post' , 
				dataType:'json',
		        data:  fa.serialize(), 
		        success: function(response) {  
		          if(response.errNum ==  0) {  
		            fa[0].reset();    
					message(response.errNum, response.errMsg);    
					$(".list_hubungi").show();
					$(".form_hubungi").hide();  
					$('.add_heading-elements').show();
					table.ajax.reload();     
		          }  else {
				  	message(response.errNum, response.errMsg);    
				  }
		        }
		     });
		
		 });
		 
});
 
function initDataTable() { 
    //waitingDialog.show('Loading.....');
    $('#tbl_hubungi').dataTable().fnDestroy();
    table = $('#tbl_hubungi').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [2], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' + 
						   '<li class="text-primary-600"><a href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' + 
						   '<li class="text-primary-600"><a href="#"><i class="icon-trash btnDelete"></i></a></li>' + 
						   '</ul>';
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'hubungi/list_hubungi', 
            "type": "GET",
            "dataSrc": function (json) {  
				(json.data.length > 0 ? $("#btn_add_hubungi").hide() : $("#btn_add_hubungi").show()); 
                return json.data;
            }
        },
        "columns": [  
			{"data": "nama_lokasi", "defaultContent": ""},
			{"data": "alamat_lokasi", "defaultContent": ""} 
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    }); 
}

$(document).ready(
        function () { 
            summernote()

            $(".btn_cancel").on('click', function () { 
                $('#editordata').summernote('destroy');
            });
  
            $('#myForm').submit(function (e) {
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'rfc2350/simpan',  
                    type: 'post',
                    data: fa.serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.errNum == 0) {
                            message(response.errNum, response.errMsg);
                        } else {
                            message(response.errNum, response.errMsg);
                        }
                    }
                });
            });
			
			$('#myForm1').submit(function (e) {
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'rfc2350/simpan_file',  
                    type: 'post',
                    data: fa.serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.errNum == 0) {
                            message(response.errNum, response.errMsg);
                        } else {
                            message(response.errNum, response.errMsg);
                        }
                    }
                });
            });
			
			$("#btn_add_rfc").on('click', function () {
                $('#myForm1').trigger("reset");
                $(".form_rfc").hide();
                $(".list_rfc").show();
                $(".add_heading-elements").hide(); 
                $("#mode").val("edit");
            });
			
			$(".btn_cancel_rfc").on('click', function () {
                $('#myForm1').trigger("reset");
                $(".form_rfc").show();
                $(".list_rfc").hide();
                $(".add_heading-elements").show(); 
            });
			
			$("#fileToUpload1").on('change', function () { 
                file = $('#fileToUpload1')[0].files[0];
				url = "rfc2350/uploadRfc"; 
				ext = "PDF";  
                uploadFile1(file, url, ext); 
            });
			
			$("#fileToUpload2").on('change', function () { 
                file = $('#fileToUpload2')[0].files[0];
				url = "rfc2350/uploadRfc"; 
				ext = "PDF";  
                uploadFile2(file, url, ext); 
            });

        });
		
function uploadFile1(file, url, ext) {
    data = new FormData();
    data.append("file", file);
    url = ci.baseurl + url; 
    $.ajax({
        data: data,
        type: "POST",
        dataType: 'json',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) { console.log("xxx", response);
            if (response.data !== undefined) {
                $("#path_file_1").val(response.path_file);
                $("#nama_file_1").val(response.data.file_name);
                $("#size_file_1").val(response.data.file_size);
                $("#tipe_file_1").val(response.data.file_type);
                $(".divMsg").hide();
				$("#lbl_uf_id").text(response.data.file_name);
				$("a.uf_id").attr("href", response.path_file + response.data.file_name);
				console.log('ksinin id');
            } else {
				$(".divMsg").show(); 
				msg_error(response + '\n File Type Allowed : ' + ext + ' Only');
				$("#path_file_1").val('');
                $("#nama_file_1").val('');
                $("#size_file_1").val('');
                $("#tipe_file_1").val(''); 
				$("#lbl_uf_1").text('');
				$("a.uf_1").attr("href", '#');
            } 
        },
            error: function (XMLHttpRequest, textStatus, errorThrown) { 
                msg_error(XMLHttpRequest.responseText + '\n Maximum File Upload : 25M'); 
            }
    });
}	

function uploadFile2(file, url, ext) {
    data = new FormData();
    data.append("file", file);
    url = ci.baseurl + url; 
    $.ajax({
        data: data,
        type: "POST",
        dataType: 'json',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) { console.log("xxx", response);
            if (response.data !== undefined) {
                $("#path_file_2").val(response.path_file);
                $("#nama_file_2").val(response.data.file_name);
                $("#size_file_2").val(response.data.file_size);
                $("#tipe_file_2").val(response.data.file_type);
                $(".divMsg").hide();
				$("#lbl_uf_2").text(response.data.file_name);
				$("a.uf_2").attr("href", response.path_file + response.data.file_name);
				console.log('ksinin en');
            } else {
				$(".divMsg").show(); 
				msg_error(response + '\n File Type Allowed : ' + ext + ' Only');
				$("#path_file_2").val('');
                $("#nama_file_2").val('');
                $("#size_file_2").val('');
                $("#tipe_file_2").val(''); 
				$("#lbl_uf_2").text('');
				$("a.uf_2").attr("href", '#');
            } 
        },
            error: function (XMLHttpRequest, textStatus, errorThrown) { 
                msg_error(XMLHttpRequest.responseText + '\n Maximum File Upload : 25M'); 
            }
    });
}		
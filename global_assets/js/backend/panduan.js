$(document).ready(
        function () {
            initDataTable();
            $('#tbl_panduan tbody').on('click', 'tr', function () {

                selectedRow = table.row(this).data();
                //console.log("selRow:", selectedRow);
                $("#id_panduan").val(selectedRow.id_panduan);
                $("#nama_panduan").val(selectedRow.nama_panduan);
                $("#nama_file").val(selectedRow.nama_file);
                $("#path_file").val(selectedRow.path_file);
                $("#size_file").val(selectedRow.size_file);
                $("#tipe_file").val(selectedRow.tipe_file);
				$("#lbl_uf").text(selectedRow.nama_file);
                $("a.uf").attr("href", selectedRow.path_file + selectedRow.nama_file);
                (selectedRow.status === "1" ? $("#status").prop("checked", true) : $("#status").prop("checked", false));
                (selectedRow.status === "1" ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
				$(".divMsg").show('');
                $("#id_panduan").prop('readonly', true);
                $('#txtHapus').text('Data : "' + selectedRow.nama_panduan + '" akan di hapus ? ');
                $("#mode").val("edit");
								
			    control = $("#fileToUpload");
				control.replaceWith( control.val('').clone( true ) );
            });

            $("#tbl_panduan").on('click', '.btnEdit', function () {
                $(".list_panduan").hide();
                $(".form_panduan").show();

            });

            $("#btn_add_panduan").on('click', function () {
                $('#myForm').trigger("reset");
                $(".list_panduan").hide();
                $(".form_panduan").show();
                $("#id_panduan").prop('readonly', false);
                $("#mode").val("add");
            });

            $("#status").on('click', function () {
                ($("#status").prop("checked") === true ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
                ($("#status").prop("checked") === true ? $("#status").val("1") : $("#status").val("0"));
            });

            $(".btn_cancel").on('click', function () {
                $(".list_panduan").show();
                $(".form_panduan").hide();
            });


            $("#tbl_panduan").on('click', '.btnDelete', function () {
                $('#modal_default').modal('show');

            });

            $(".btnHapus").click(function () {
                $.ajax({
                    type: "post",
                    url: ci.baseurl + 'panduan/hapus',
                    data: {id: selectedRow.id_panduan, nama_file: selectedRow.nama_file},
                    dataType: 'json',
                    success: function (response) {
                        message_hapus(response.errNum, response.errMsg);
                        table.ajax.reload();
                        $('#modal_default').modal('hide');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });
			
			$("#fileToUpload").on('change', function () { 
                file = $('#fileToUpload')[0].files[0];
				url = "panduan/upload"; 
				ext = "PDF";  
                uploadFile(file, url, ext);
            });
			
			$('#myForm').submit(function (e) {
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'panduan/simpan', //fa.attr('action'),
                    type: 'post',
                    data: fa.serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.errNum == 0) {
                            fa[0].reset();
                            message(response.errNum, response.errMsg);
                            $(".list_panduan").show();
                            $(".form_panduan").hide();
                            table.ajax.reload();
                        } else {
                            message(response.errNum, response.errMsg);
                        }
                    }
                });

            });

        });

function initDataTable() {
    //waitingDialog.show('Loading.....');
    $('#tbl_panduan').dataTable().fnDestroy();
    table = $('#tbl_panduan').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [2], // Column to target
                "mRender": function (data, type, full) {
                    return full.size_file + ' KB';
                }
            },
            {
                "aTargets": [3], // Column to target
                "mRender": function (data, type, full) {
                    return full.hit;
                }
            },
            {
                "aTargets": [4], // Column to target
                "mRender": function (data, type, full) {
                    return (full.status == '1' ? '<span class="label bg-blue">Publish</span>' : '<span class="label bg-danger">UnPublish</span>');
                }
            },
            {
                "aTargets": [5], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' +
                            '<li class="text-primary-600"><a href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' +
                            (ci.sr === "0" ? '<li class="text-primary-600"><a href="#"><i class="icon-trash btnDelete"></i></a></li>' : '') +
                            '</ul>';
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'panduan/list_panduan',
            "type": "GET",
            "dataSrc": function (json) {
                return json.data;
            }
        },
        "columns": [
            {"data": "nama_panduan", "defaultContent": ""},
            {"data": "create_dt", "defaultContent": ""}
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    });
}
 
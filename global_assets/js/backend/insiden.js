$(document).ready(
        function () {
            initDataTable();

            $('#tbl_insiden tbody').on('click', 'tr', function () {

                selectedRow = table.row(this).data();
//                console.log('xxx', selectedRow);
                var ti = selectedRow.tipe_insiden;
				if (ti.indexOf(",") >= 0) {

	                var arr_ti = ti.split(",");
	                for (var i = 0; i < arr_ti.length; i++) {
	                    $("#ti [value=" + arr_ti[i] + "]").prop("checked", true);
	                }
				}
                $("#id_insiden").val(selectedRow.id_insiden);
                $("#nama_lengkap").val(selectedRow.nama_lengkap);
                $("#telp_organisasi").val(selectedRow.telp_organisasi);
                $("#organisasi").val(selectedRow.organisasi);
                $("#handphone").val(selectedRow.handphone);
                $("#email").val(selectedRow.email);
                $("#tgl_insiden").val(selectedRow.tgl_insiden);
                $("#jam_insiden").val(selectedRow.jam_insiden);
                $("input[name='tipe_laporan'][value='" + selectedRow.tipe_laporan + "']").prop('checked', true);
                $("#deskripsi_insiden").val(selectedRow.deskripsi_insiden);
                var links = $("<a download>");
                links.attr("href", selectedRow.path_file + selectedRow.nama_file);
                links.attr("title", selectedRow.nama_file);
                links.text(selectedRow.nama_file);
                $("#links").html(links);
                $("input[name='dampak_insiden'][value='" + selectedRow.dampak_insiden + "']").prop('checked', true);
                $("#respon_pdk").val(selectedRow.respon_pdk);
                $("#respon_pjg").val(selectedRow.respon_pjg);
                $("#respon_bak").val(selectedRow.respon_bak);
                $("#laporan_lain").val(selectedRow.laporan_lain);
                $("#aset_kritis").val(selectedRow.aset_kritis);
                $("#dampak_aset").val(selectedRow.dampak_aset);
                $("#jumlah_dampak").val(selectedRow.jumlah_dampak);
                $("#dampak_ict").val(selectedRow.dampak_ict);
                $("#ip_penyerang").val(selectedRow.ip_penyerang);
                $("#port_diserang").val(selectedRow.port_diserang);
                var ts = selectedRow.tipe_serangan;
				if (ts.indexOf(",") >= 0) {
	                var arr_ts = ts.split(",");
	                for (var k = 0; k < arr_ts.length; k++) {
	                    $("#ts [value=" + arr_ts[k] + "]").prop("checked", true);
	                }
				}	
                $("input[name='lap_analisis'][value='" + selectedRow.lap_analisis + "']").prop('checked', true);
                $("input[name='lap_forensik'][value='" + selectedRow.lap_forensik + "']").prop('checked', true);
                $("input[name='lap_audit'][value='" + selectedRow.lap_audit + "']").prop('checked', true);
                $("input[name='lap_traffic'][value='" + selectedRow.lap_traffic + "']").prop('checked', true);
                $("#nama_perangkat").val(selectedRow.nama_perangkat);
                $("#lokasi_perangkat").val(selectedRow.lokasi_perangkat);
                $("#sistem_operasi").val(selectedRow.sistem_operasi);
                $("#last_update_os").val(selectedRow.last_update_os);
                $("#ip_address").val(selectedRow.ip_address);
                $("#mac_address").val(selectedRow.mac_address);
                $("#dns_entry").val(selectedRow.dns_entry);
                $("#domain").val(selectedRow.domain);
                $("input[name='konek_jaringan'][value='" + selectedRow.konek_jaringan + "']").prop('checked', true);
                $("input[name='konek_modem'][value='" + selectedRow.konek_modem + "']").prop('checked', true);
                $("input[name='pengamanan_fisik'][value='" + selectedRow.pengamanan_fisik + "']").prop('checked', true);
                $("input[name='pengamanan_logik'][value='" + selectedRow.pengamanan_logik + "']").prop('checked', true);
                $("input[name='perangkat_putus'][value='" + selectedRow.perangkat_putus + "']").prop('checked', true);
                $("#status_insiden").val(selectedRow.status_insiden);
                $("#manajemen_krisis").val(selectedRow.manajemen_krisis);
                $('#txtHapus').text('Data : "' + selectedRow.nama_lengkap + '" akan di hapus ? ');
                $('#txtKirim').text('Data ini akan dikirim ? ');
                $("#mode").val("edit");
                (selectedRow.status === "S" ? $(".btn_save_insiden").hide() : $(".btn_save_insiden").show());
                //($("#flagPdf").val() === "1" ? generatePdf(selectedRow) : console.log("bkn"));
            });

            $("#tbl_insiden tbody").on('click', '.btnEdit', function () {
                $(".list_insiden").hide();
                $(".form_insiden").show();
                $("#flagPdf").val("0"); 
            });

            $("#tbl_insiden").on('click', '.btnKirim', function () {
                $('#modal_default1').modal('show');
                $("#flagPdf").val("0");
            });

            $("#btn_add_insiden").on('click', function () {
                $('#myForm').trigger("reset");
                $(".add_heading-elements").hide();
                $(".list_insiden").hide();
                $(".form_insiden").show();
                $("#mode").val("add");
            });

            $("#tbl_insiden").on('click', '.btnDelete', function () {
                $('#modal_default').modal('show');
                $("#flagPdf").val("0");
            });

            $(".btn_cancel").on('click', function () {
                $(".list_insiden").show();
                $(".form_insiden").hide();
            });

            /*$(".btn_lanjut").on('click',function(){ 
             $('.nav-tabs > .active').next('li').find('a').trigger('click'); 
             $("#aset_kritis").focus();
             });
             */
            $(".btnHapus").click(function () {
                $.ajax({
                    type: "post",
                    url: ci.baseurl + 'insiden/hapus',
                    dataType: "json",
                    data: {id: selectedRow.id_insiden},
                    success: function (response) {
                        message_hapus(response.errNum, response.errMsg);
                        table.ajax.reload();
                        $('#modal_default').modal('hide');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });

            $(".btnSend").click(function () {
                $.ajax({
                    type: "post",
                    url: ci.baseurl + 'insiden/send',
                    dataType: "json",
                    data: {id: selectedRow.id_insiden},
                    success: function (response) {
                        message(response.errNum, response.errMsg);
                        table.ajax.reload();
                        $('#modal_default1').modal('hide');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });

            $("#fileToUpload").on('change', function () {
                //uploadFilex($('#fileToUpload')[0].files[0]);
				file = $('#fileToUpload')[0].files[0];
				url = "insiden/upload";
				ext = "PDF";  
                uploadFile(file, url, ext);
            });

            $('#myForm').submit(function (e) {
                console.log("rep");
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'insiden/simpan', //fa.attr('action'),
                    type: 'post',
                    dataType: 'json',
                    data: fa.serialize(),
                    success: function (response) {
//                        console.log("rep", response);
                        $("#temp").html(response);
                        if (response.errNum == 0) {
                            fa[0].reset();
                            message(response.errNum, response.errMsg);
                            $(".list_insiden").show();
                            $(".form_insiden").hide();
                            $('.add_heading-elements').show();
                            table.ajax.reload();
                        } else {
                            message(response.errNum, response.errMsg);
                        }
                    }
                });

            });
 
            $("#tbl_insiden tbody").on('click', '.btnPdf', function () {
                $("#flagPdf").val("1"); 
				$('#modal_default2').modal('show'); 
            });
        });

function initDataTable() {
    //waitingDialog.show('Loading.....');
    $('#tbl_insiden').dataTable().fnDestroy();
    table = $('#tbl_insiden').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [3], // Column to target
                "mRender": function (data, type, full) {
                    return (full.status == "D" ? "Draft" : "Sent");

                }
            },
            {
                "aTargets": [4], // Column to target
                "mRender": function (data, type, full) {
                    if (full.status === "D") {
                        return '<ul class="icons-list">' +
                                '<li class="text-primary-600"><a title="Ubah" href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' +
                                '<li class="text-primary-600"><a title="Hapus" href="#"><i class="icon-trash btnDelete"></i></a></li>' +
                                '<li class="text-primary-600"><a title="Kirim" href="#"><i class="icon-envelope btnKirim"></i></a></li>' +
                                '<li class="text-primary-600"><a title="Download PDF" href="#"><i class="icon-file-pdf btnPdf"></i></a></li>' +
                                '</ul>';
                    } else {
                        return '<ul class="icons-list">' +
                                '<li class="text-primary-600"><a title="Lihat" href="#"><i class="icon-zoomin3 btnEdit"></i></a></li>' +
                                '<li class="text-primary-600"><a title="Download PDF" href="#"><i class="icon-file-pdf btnPdf"></i></a></li>' +
                                '</ul>';
                    }
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'insiden/list_insiden',
            "type": "GET",
            "dataSrc": function (json) {
                return json.data;
            }
        },
        "columns": [
            {"data": "nama_lengkap", "defaultContent": ""},
            {"data": "organisasi", "defaultContent": ""},
            {"data": "create_dt", "defaultContent": ""}
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    });
}

function uploadFilex(file) {
    data = new FormData();
    data.append("file", file);
    url = ci.baseurl + "insiden/upload";
    $.ajax({
        data: data,
        type: "POST",
        dataType: 'json',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            //console.log("===>", response);		
            $("#file_name").val(response.file_name);
            $(".divMsg").show();
            $("#message").html(response);
        }
    });


}

function generatePdf(data) {
    $.ajax({
        type: "post",
        url: ci.baseurl + 'insiden/get_insiden',
        dataType: "json",
        data: {id: data.id_insiden},
        success: function (response) {
            var strins = "";
            var strser = "";
            $.each(response.di, function (index, val) {
                strins += val.deskripsi + "<br>";
            });
            $.each(response.ds, function (index, val) {
                strser += val.deskripsi + "<br>";
            });

            var pdf = new jsPDF('p', 'pt', 'letter');
            pdf.setFont("helvetica");
            pdf.setFontSize(10); 

            var str = 'A. INFORMASI UMUM<br>' +
                    '<p>1. IDENTITAS PELAPOR <br>' +
                    'Nama Lengkap : ' + data.nama_lengkap + '<br>' +
                    'Telp. Organisasi : ' + data.telp_organisasi + '<br>' +
                    'Organisasi : ' + data.organisasi + '<br>' +
                    'Email : ' + data.email + '<br>' +
                    'Handphone : ' + data.handphone + '</p>' +
                    '<p>2. TIPE LAPORAN <br>' +
                    'Tipe : ' + (data.tipe_laporan === "AW" ? "Awal" : (data.tipe_laporan === "TL" ? "Tindak Lanjut" : "Akhir")) + '</p>' +
                    '<p>3. WAKTU TERJADINYA INSIDEN <br>' +
                    'Tanggal : ' + data.tgl_insiden + '<br>' +
                    'Jam : ' + data.jam_insiden + '</p>' +
                    '<p>4. TIPE INSIDEN </br>' +
                    '' + strins + '</p>' +
                    '<p>5. DESKRIPSI INSIDEN <br>' +
                    'Deskripsi : ' + data.deskripsi_insiden + '</p>' +
                    '<p>6. DAMPAK INSIDEN <br>' +
                    'Dampak : ' + (data.dampak_insiden === "P" ? "Jaringan Publik" : (data.dampak_insiden === "I" ? "Jaringan Internal" : "Lainnya")) + '</p>' +
                    '<p>7. TINDAKAN PENANGGULANGAN INSIDEN <br>' +
                    'a. Respon Cepat/Awal(Jangka Pendek) : ' + data.respon_pdk + '<br>' +
                    'b. Jangka Panjang : ' + data.respon_pjg + '<br>' +
                    'c. Apakah Rencana BackUp System sukses diimplementasikan : ' + data.respon_bak + '</p>' +
                    '<p>8. APAKAH ORGANISASI LAIN DILAPORKAN? JIKA IYA, SEBUTKAN   <br>' +
                    '' + data.laporan_lain + '</p>' +
                    '<p>B. INFORMASI KHUSUS</p>' +
                    '<p>9. ASET KRITIS YANG TERKENA DAMPAK  <br>' +
                    '' + data.aset_kritis + '<br>' +
                    '<p>10. DAMPAK INSIDEN TERHADAP ASET   <br>' +
                    '' + data.dampak_aset + '<br>' +
                    '<p>11. JUMLAH PENGGUNA YANG TERKENA DAMPAK <br>' +
                    '' + data.jumlah_dampak + '<br>' +
                    '<p>12. DAMPAK TERHADAP ICT <br>' +
                    '' + data.dampak_ict + '<br>' +
                    '<p>13. PROFIL PENYERANG <br>' +
                    'IP Penyerang :' + data.ip_penyerang + '<br>' +
                    'Port Diserang :' + data.port_diserang + '</p>' +
                    '<p>14. TIPE SERANGAN <br>' +
                    '' + strser + '</p>' +
                    '<p>15. ANALISIS <br>' +
                    'a. Laporan Analisis Log : ' + (data.lap_analisis === "A" ? "Ada" : (data.lap_analisis === "T" ? "Tidak Ada" : "Sedang Proses")) + '<br>' +
                    'b. Laporan Forensik : ' + (data.lap_forensik === "A" ? "Ada" : (data.lap_forensik === "T" ? "Tidak Ada" : "Sedang Proses")) + '<br>' +
                    'c. Laporan Audit : ' + (data.lap_audit === "A" ? "Ada" : (data.lap_audit === "T" ? "Tidak Ada" : "Sedang Proses")) + '<br>' +
                    'd. Laporan Lalu Lintas Jaringan : ' + (data.lap_traffic === "A" ? "Ada" : (data.lap_traffic === "T" ? "Tidak Ada" : "Sedang Proses")) + '</p>' +
                    '<p>16. RINCIAN <br>' +
                    'a. Nama dan Versi Perangkat :' + data.nama_perangkat + '<br>' +
                    'b. Lokasi Perangkat :' + data.lokasi_perangkat + '<br>' +
                    'c. Sistem Operasi :' + data.sistem_operasi + '<br>' +
                    'd. Terakhir Update Firmware :' + data.last_update_os + '<br>' +
                    'e. IP Address :' + data.ip_address + '<br>' +
                    'f. MAC Address :' + data.mac_address + '<br>' +
                    'g. DNS Entry :' + data.dns_entry + '<br>' +
                    'h. Domain/Workgroup :' + data.domain + '<br>' +
                    'i. Perangkat terhubung ke jaringan ? ' + (data.konek_jaringan === "Y" ? "Ya" : "Tidak" )+ '<br>' +
                    'j. Perangkat terhubung ke modem ? ' + (data.konek_modem === "Y" ? "Ya" : "Tidak" ) + '<br>' +
                    'k. Adakah pengamanan fisik terhadap perangkat? ' + (data.pengamanan_fisik === "Y" ? "Ya" : "Tidak" ) + '<br>' +
                    'l. Adakah pengamanan logik terhadap perangkat ? ' + (data.pengamanan_logik === "Y" ? "Ya" : "Tidak" ) + '<br>' +
                    'm. Apakah perangkat sudah diputus dari jaringan ?' + (data.perangkat_putus === "Y" ? "Ya" : "Tidak" ) + '</p>' +
                    '<p>16. STATUS INSIDEN<br>' +
                    '' + data.status_insiden + '</p>' +
                    '<p>17. APAKAH SUDAH PERNAH DITAWARKAN SISTEM MANAJEMEN KRISIS?<br>' +
                    '' + data.manajemen_krisis + '<br>' +
                    '';

            $('#customers').html(str);
            source = $('#customers')[0];
            specialElementHandlers = {
                // element with id of "bypass" - jQuery style selector
                '#bypassme': function (element, renderer) {
                    // true = "handled elsewhere, bypass text extraction"
                    return true;
                }
            };
            margins = {
                top: 60,
                bottom: 60,
                left: 60,
                width: 800
            };
            
            pdf.fromHTML(
                    source, // HTML string or DOM elem ref.
                    margins.left, // x coord
                    margins.top, {// y coord
                        'width': margins.width, // max width of content on PDF
                        'elementHandlers': specialElementHandlers
                    },
            function (dispose) { 
                pdf.save('Test.pdf');
            }, margins);

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            msg_error(XMLHttpRequest.statusText);
        }
    });
 
}
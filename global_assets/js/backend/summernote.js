function summernote() {
    $('#editordata').summernote({
        height: 500,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['table', ['table']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['picture', 'link', 'hr']],
        ],
        //set callback image tuk upload ke serverside
        callbacks: {
            onImageUpload: function (files) {
                uploadFile(files[0], 1);
            }
        }
    });
}
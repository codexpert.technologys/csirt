
$(document).ready(
        function () {
            $('#tbl_berita').DataTable();
            //isChange();
            initDataTable();
            //$("#tbl_berita").on('click','.btnEdit',function(){ 
            $('#tbl_berita tbody').on('click', 'tr', function () {
                //var currentRow=$(this).closest("tr");   
                selectedRow = table.row(this).data();
                console.log("sel:", selectedRow);
                $(".list_berita").hide();
                $(".form_berita").show();
                $(".add_heading-elements").hide();
                $("#berita_id").val(selectedRow.berita_id);
                $("#kategori_id").val(selectedRow.kategori_id);
                $("#judul_video").val(selectedRow.judul_video);
				$("#path_file").val(selectedRow.path_file);
                $("#short_desc").val(selectedRow.short_desc);
                $("#editordata").val(decodeHtml(selectedRow.long_desc));
                $("#title").val(selectedRow.title);
                $("#publish_dt").val(selectedRow.publish_dt);
                (selectedRow.status == "1" ? $("#status").prop("checked", true) : $("#status").prop("checked", false));
                (selectedRow.status == "1" ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
                $("#berita_id").prop('readonly', true);
                $("#mode").val("edit");
                summernote();
            });

            $("#btn_add_berita").on('click', function () {
                $('#myForm').trigger("reset");
                $(".list_berita").hide();
                $(".form_berita").show();
                $(".add_heading-elements").hide();
                $("#berita_id").prop('readonly', false);
                $("#mode").val("add");
                $('#editordata').val('');
                summernote();
            });

            $(".btn_cancel").on('click', function () {
                $(".list_berita").show();
                $(".form_berita").hide();
                $(".add_heading-elements").show();
                $('#editordata').summernote('destroy');
            });

            $("#status").on('click', function () {
                ($("#status").prop("checked") === true ? $("#txtStatus").text("Publish") : $("#txtStatus").text("UnPublish"));
                ($("#status").prop("checked") === true ? $("#status").val("1") : $("#status").val("0"));
            });


            $("#tbl_berita").on('click', '.btnDelete', function () {
                var currentRow = $(this).closest("tr");
                var berita_id = currentRow.find("td:eq(0)").text();
                $('#txtHapus').text('Berita : "' + currentRow.find("td:eq(1)").text() + '" akan di hapus ? ');
                $('#modal_default').modal('show');
                $('#berita_id').val(berita_id);
            });

            $(".btnHapus").click(function () {
                $(".list_berita").show();
                $(".form_berita").hide();
                $(".add_heading-elements").show();
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: ci.baseurl + 'berita/hapus_berita',
                    data: {id: $('#berita_id').val()},
                    success: function (response) {
                        console.log("resdel:", response);
                        message_hapus(response.errNum, response.errMsg);
                        table.ajax.reload();
                        $('#modal_default').modal('hide');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        msg_error(XMLHttpRequest.statusText);
                    }
                });
            });

            $("#fileToUpload").on('change', function () {
                uploadFileBerita($('#fileToUpload')[0].files[0], 2);
                console.log("kesini");
            });



            $('#myFormC').submit(function (e) {
                e.preventDefault();
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'user/save_change', // fa.attr('action'),
                    type: 'post',
                    data: fa.serialize(),
                    success: function (response) {
                        if (response === "true") {
                            message(response);
                            $('#modal_change').modal('hide');
                        } else {
                            message(response);
                        }
                    }
                });

            });

            $('#myForm').submit(function (e) {
                e.preventDefault();
				var aHTML = $('#editordata').summernote('code');
				$('#editordata').val(aHTML);
                var fa = $(this);
                $.ajax({
                    url: ci.baseurl + 'berita/data_berita', //fa.attr('action'),
                    type: 'post',
                    data: fa.serialize(),
                    dataType: 'json',
                    success: function (response) {
                        console.log("resp:", response);
                        if (response.errNum == 0) {
                            fa[0].reset();
                            message(response.errNum, response.errMsg);
                            $(".list_berita").show();
                            $(".form_berita").hide();
                            table.ajax.reload();
                        } else {
                            message(response.errNum, response.errMsg);
                        }
                    }
                });

            });

        });

function summernote() {
    $('#editordata').summernote({
        height: 200,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['table', ['table']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['picture', 'link', 'hr']],
        ],
        //set callback image tuk upload ke serverside
        callbacks: {
            onImageUpload: function (files) {
                uploadFileBerita(files[0], 1);
            }
        }
    });
}


function uploadFileBerita(file, i) {
    data = new FormData();
    data.append("file", file);
    url = (i === 1 ? ci.baseurl + "berita/saveGambar" : ci.baseurl + "berita/saveVideo");
    $.ajax({
        data: data,
        type: "POST",
        dataType: 'json',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (url) {
            console.log("urrl:", url);
            if (url.errNum > 0)
            {
                message(url.errNum, url.errMsg);
            } else {
                (i === 1 ? $('#editordata').summernote("insertImage", url) : $("#nama_file").val(url.fileName));
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            msg_error(XMLHttpRequest.statusText);
        }
    });
}

function initDataTable() {
    //waitingDialog.show('Loading.....');
    $('#tbl_berita').dataTable().fnDestroy();
    table = $('#tbl_berita').DataTable({
	    //"order":[[3], "desc"],
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [2], // Column to target
                "mRender": function (data, type, full) {
                    if (full.kategori_id === '1') {
                        kategori = "Berita"
                    } else if (full.kategori_id === '1') {
                        kategori = "Info Penting"
                    } else {
                        kategori = "Keamanan"
                    }
                    return kategori;
                }
            },
            {
                "aTargets": [3], // Column to target
                "mRender": function (data, type, full) {
                    return full.publish_dt;
                }
            },
            {
                "aTargets": [4], // Column to target
                "mRender": function (data, type, full) {
                    return (full.status == '1' ? '<span class="label bg-blue">Publish</span>' : '<span class="label bg-danger">UnPublish</span>');
                }
            },
            {
                "aTargets": [5], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' +
                            '<li class="text-primary-600"><a href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' +
                            (ci.sr === "0" ? '<li class="text-primary-600"><a href="#"><i class="icon-trash btnDelete"></i></a></li>' : "") +
                            '</ul>';
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'berita/list_berita',
            "type": "GET",
            "dataSrc": function (json) {console.log("json:", json);
                return json.data;
            }
        },
        "columns": [
            {"data": "berita_id", "defaultContent": "", "sClass": "hide"},
            {"data": "title", "defaultContent": ""}
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    });

}

function isChange() {
    $.ajax({
        type: "post",
        url: ci.baseurl + 'user/is_change',
        dataType: "json",
        success: function (response) {
            console.log("response", response);
            if (response.is_change == "0") {
                $('#modal_change').modal('show');
            } else {
                $('#modal_change').modal('hide');
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            msg_error(XMLHttpRequest.statusText);
        }
    });
}


function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}
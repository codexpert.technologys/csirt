$(document).ready(
    function () { 
		initDataTable();
		
		$('#tbl_kategori tbody').on('click', 'tr', function () {
		     
        	 selectedRow = table.row(this).data(); 
        	 $("#kategori_id").val(selectedRow.kategori_id);   
			 $("#kategori").val(selectedRow.kategori);   
			 $('#txtHapus').text('Data : "' + selectedRow.kategori  + '" akan di hapus ? ');
			 $("#mode").val("edit"); 
    	});
		
		$("#tbl_kategori").on('click','.btnEdit',function(){   
	         $(".list_kategori").hide();
			 $(".form_kategori").show();  
			 
		});
		
		$("#btn_add_kategori").on('click',function(){  
			$('#myForm').trigger("reset");
	         $(".list_kategori").hide();
			 $(".form_kategori").show();  
			 $("#kategori_id").prop('readonly', false); 
			 $("#mode").val("add"); 
		});
		 
		$(".btn_cancel").on('click',function(){  
	         $(".list_kategori").show();
			 $(".form_kategori").hide();   
		});
		   
		
		$("#tbl_kategori").on('click','.btnDelete',function(){    
			 $('#modal_default').modal('show');  
			
    	});
		
		$(".btnHapus").click(function(){  
			$.ajax({
                        type:"post",
                        url:ci.baseurl + 'kategori/hapus', 
						dataType:'json',
                        data:  {id:selectedRow.kategori_id} ,   
                        success: function(response){  
							message_hapus(response.errNum, response.errMsg);
							table.ajax.reload(); 
							$('#modal_default').modal('hide'); 
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
							 msg_error(XMLHttpRequest.statusText);
                        }
           }); 
    	});
		
		/*$(".btn_save_kategori").click(function(){  
			$.ajax({
                        type:"post",
                        url:ci.baseurl + 'kategori/simpan', 
                        data: $("#myForm").serialize()   ,  
                        success: function(response){   
							$(".list_kategori").show();
							$(".form_kategori").hide();   
							$('.add_heading-elements').show();
							message(response);
							table.ajax.reload(); 
							
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
							 msg_error(XMLHttpRequest.statusText);
                        }
           }); 
    	});*/
		
		$('#myForm').submit(function(e){ 
		    e.preventDefault();
		     var fa = $(this);  
		      $.ajax({
		        url: ci.baseurl + 'kategori/simpan', //fa.attr('action'),
		        type: 'post' , 
				dataType:'json',
		        data:  fa.serialize(), 
		        success: function(response) {  
		          if(response.errNum ==  0) {  
		            fa[0].reset();    
					message(response.errNum, response.errMsg);    
					$(".list_kategori").show();
					$(".form_kategori").hide();  
					$('.add_heading-elements').show();
					table.ajax.reload();     
		          }  else {
				  	message(response.errNum, response.errMsg);    
				  }
		        }
		     });
		
		 });
		 
});
 
function initDataTable() { 
    //waitingDialog.show('Loading.....');
    $('#tbl_kategori').dataTable().fnDestroy();
    table = $('#tbl_kategori').DataTable({
        "serverSide": false,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            },
            {
                "aTargets": [1], // Column to target
                "mRender": function (data, type, full) {
                    return '<ul class="icons-list">' + 
						   '<li class="text-primary-600"><a href="#"><i class="icon-pencil7 btnEdit"></i></a></li>' + 
						   '<li class="text-primary-600"><a href="#"><i class="icon-trash btnDelete"></i></a></li>' + 
						   '</ul>';
                }
            }
        ],
        "ajax": {
            "url": ci.baseurl + 'kategori/list_kategori', 
            "type": "GET",
            "dataSrc": function (json) {  
                return json.data;
            }
        },
        "columns": [  
			{"data": "kategori", "defaultContent": ""} 
        ],
        "drawCallback": function (settings) {
            //$('th').removeClass('sorting_asc');
        }
    }); 
}